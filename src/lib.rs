pub mod condition;
pub mod consts;
pub mod debuginfo;
pub mod error;
pub mod functions;
pub mod globals;
pub mod graph;
pub mod graph_algo;
pub mod graph_node;
pub mod graphgen;
pub mod graphviz;
pub mod id_tracker;
pub mod instructions;
pub mod lowering;
pub mod name_util;
pub mod operand;
pub mod typecheck;
pub mod uniquevec;
pub mod util;

#[cfg(test)]
mod testutil;

use std::path::PathBuf;

use llvm_ir::Module;

use error::LoadError;
use graph::Graph;
use graphgen::{BlockGraph, PhiStrategy, ReturnValue};
use id_tracker::IdTracker;
use lowering::do_lowering;

use std::sync::Mutex;

// The function loading process is split in two in order to be able to inspect
// the resulting graph before lowering
pub struct UnloweredFunction {
    pub name: String,
    pub graph: Graph,
    pub return_operand: operand::Operand,
    pub debug_mutex: Mutex<debuginfo::DebugInfo>,
    pub globals: globals::GlobalTable,
    pub next_id: Mutex<IdTracker>,
    // Mostly used for debug purposes
    pub block_graph: BlockGraph,
}

pub struct Function {
    pub name: String,
    pub return_value: ReturnValue,
    pub graph: Graph,
    pub debug: debuginfo::DebugInfo,
    pub globals: globals::GlobalTable,
    pub next_id: IdTracker,
    pub lowering_diffs: Vec<Vec<graph::GraphDiff>>,
}

/// Loads llvm bitcode and converts it into a graph. The returned values are
/// (function name, id of return node, graph)
pub fn load_function_from_module(
    input_file: PathBuf,
    phi_strategy: PhiStrategy,
) -> Result<UnloweredFunction, LoadError> {
    let debug_mutex = Mutex::new(debuginfo::DebugInfo::new());

    let module = Module::from_bc_path(input_file.clone()).expect(&format!(
        "Failed to read module at {}",
        input_file.to_string_lossy()
    ));

    if module.functions.len() > 1 {
        Err(LoadError::MultipleFunctions)?;
    }

    let function = module.functions.first().ok_or(LoadError::NoFunctions)?;

    let block_graph = BlockGraph::new(&function.basic_blocks);

    let next_id = Mutex::new(IdTracker::new(10000));
    let (converted, return_operand) = {
        let mut debug = debug_mutex.lock().unwrap();
        graphgen::function_to_graph(
            &function,
            phi_strategy,
            &mut next_id.lock().unwrap(),
            &mut debug,
        )
        .map_err(LoadError::ConversionFailure)?
    };

    let globals = globals::GlobalTable::from(&module.global_vars);

    Ok(UnloweredFunction {
        name: function.name.clone(),
        graph: converted,
        return_operand,
        globals,
        next_id,
        debug_mutex,
        block_graph,
    })
}

pub fn lower_function(input: UnloweredFunction) -> Result<Function, LoadError> {
    let UnloweredFunction {
        name,
        return_operand,
        graph,
        next_id,
        globals,
        debug_mutex,
        ..
    } = input;

    // Do lowering
    let (lowered, mut lowering_diffs) = do_lowering(
        graph,
        &[
            &|graph| lowering::lower_loads(graph, &globals),
            &lowering::input_bounds::lower_inputs,
            &|graph| {
                lowering::structs::lower_struct_stores(
                    graph,
                    &mut next_id.lock().unwrap(),
                    &mut debug_mutex.lock().unwrap(),
                )
                .map_err(|e| e.into())
            },
            &|graph| lowering::structs::lower_struct_insertvalue(graph).map_err(|e| e.into()),
            &|graph| Ok(lowering::interp::lower_interps(&graph, &globals)?),
        ],
    )
    .map_err(LoadError::Lowering)?;

    let return_value =
        graphgen::return_operands(&return_operand, &lowered).map_err(LoadError::ReturnValue)?;

    let pre_clean = lowered.clone();
    let clean = match &return_value {
        ReturnValue::Single(ret) => lowered.clean_unreachable(ret),
        ReturnValue::Struct { struct_node, .. } => lowered.clean_unreachable(struct_node),
    };

    lowering_diffs.push(clean.diff(&pre_clean));

    Ok(Function {
        name,
        return_value,
        graph: clean,
        debug: debug_mutex.into_inner().unwrap(),
        globals,
        next_id: next_id.into_inner().unwrap(),
        lowering_diffs,
    })
}
