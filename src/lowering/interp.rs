/**
  Functions used for lowering nodes, i.e. converting one or more nodes
  into new nodes that are better suited for further operations
*/
use thiserror::Error;

use crate::globals::GlobalTable;
use crate::graph::Graph;
use crate::graph_node::{Kind, Node, UnloweredInterp1, UnloweredInterp2};
use crate::operand::Operand;

use super::util::get_global_double_array;

#[derive(Debug, Clone, PartialEq, Error)]
pub enum Error {
    #[error("Global lookup failed")]
    GlobalLookupError(#[from] super::util::GlobalLookupError),
}

fn lower_interp1_args(
    args: &UnloweredInterp1,
    globals: &GlobalTable,
) -> Result<(Vec<f64>, Vec<f64>, Operand), Error> {
    let x = get_global_double_array(&args.x.0, globals)?;
    let y = get_global_double_array(&args.y.0, globals)?;

    Ok((x, y, args.x_point.clone()))
}

fn lower_interp2_args(
    args: &UnloweredInterp2,
    globals: &GlobalTable,
) -> Result<(Vec<f64>, Vec<f64>, Vec<f64>, Operand, Operand), Error> {
    let x = get_global_double_array(&args.x.0, globals)?;
    let y = get_global_double_array(&args.y.0, globals)?;
    let z = get_global_double_array(&args.z.0, globals)?;

    Ok((x, y, z, args.x_point.clone(), args.y_point.clone()))
}

/**
  Converts `load(getelementpointer(<lut pointer>))` into look up instructions
*/
pub fn lower_interps(graph: &Graph, globals: &GlobalTable) -> Result<Graph, Error> {
    let mut new_graph = Graph::empty();
    for (name, node) in &graph.nodes {
        match node.kind {
            Kind::UnloweredInterp1(ref args) => {
                let (x, y, x_point) = lower_interp1_args(args, globals)?;
                new_graph.add_node(Node::new(name.clone(), Kind::Interp1 { x, y, x_point }));
            }
            Kind::UnloweredInterp1Valid(ref args) => {
                let (x, y, x_point) = lower_interp1_args(args, globals)?;
                new_graph.add_node(Node::new(
                    name.clone(),
                    Kind::Interp1Valid { x, y, x_point },
                ));
            }
            Kind::UnloweredInterp2(ref args) => {
                let (x, y, z, x_point, y_point) = lower_interp2_args(args, globals)?;
                new_graph.add_node(Node::new(
                    name.clone(),
                    Kind::Interp2 {
                        x,
                        y,
                        z,
                        x_point,
                        y_point,
                    },
                ));
            }
            Kind::UnloweredInterp2Valid(ref args) => {
                let (x, y, z, x_point, y_point) = lower_interp2_args(args, globals)?;
                new_graph.add_node(Node::new(
                    name.clone(),
                    Kind::Interp2Valid {
                        x,
                        y,
                        z,
                        x_point,
                        y_point,
                    },
                ));
            }
            _ => {
                new_graph.add_node(node.clone());
            }
        }
    }
    Ok(new_graph)
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashMap;

    use crate::globals::Global;
    use crate::graph_node::{node, GlobalIdent};
    use crate::operand::ident;

    #[test]
    fn interp1_lowering_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::UnloweredInterp1(UnloweredInterp1 {
                        x: GlobalIdent("x".into()),
                        y: GlobalIdent("y".into()),
                        x_point: ident("a".into()),
                    }),
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::Interp1 {
                        x: vec![0., 1., 3., 5.],
                        y: vec![3., 4., 6., 8.],
                        x_point: ident("a".into()),
                    },
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let strings = GlobalTable {
            content: vec![
                ("x".into(), Global::DoubleArray(vec![0., 1., 3., 5.])),
                ("y".into(), Global::DoubleArray(vec![3., 4., 6., 8.])),
            ]
            .into_iter()
            .collect(),
        };

        assert_eq!(lower_interps(&graph, &strings), Ok(expected),);
    }

    #[test]
    fn interp1_valid_lowering_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::UnloweredInterp1Valid(UnloweredInterp1 {
                        x: GlobalIdent("x".into()),
                        y: GlobalIdent("y".into()),
                        x_point: ident("a".into()),
                    }),
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::Interp1Valid {
                        x: vec![0., 1., 3., 5.],
                        y: vec![3., 4., 6., 8.],
                        x_point: ident("a".into()),
                    },
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let strings = GlobalTable {
            content: vec![
                ("x".into(), Global::DoubleArray(vec![0., 1., 3., 5.])),
                ("y".into(), Global::DoubleArray(vec![3., 4., 6., 8.])),
            ]
            .into_iter()
            .collect(),
        };

        assert_eq!(lower_interps(&graph, &strings), Ok(expected),);
    }

    #[test]
    fn interp2_lowering_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::UnloweredInterp2(UnloweredInterp2 {
                        x: GlobalIdent("x".into()),
                        y: GlobalIdent("y".into()),
                        z: GlobalIdent("z".into()),
                        x_point: ident("a".into()),
                        y_point: ident("b".into()),
                    }),
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::Interp2 {
                        x: vec![0., 1., 3., 5.],
                        y: vec![3., 4., 6., 8.],
                        z: vec![4., 5., 7., 9.],
                        x_point: ident("a".into()),
                        y_point: ident("b".into()),
                    },
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let strings = GlobalTable {
            content: vec![
                ("x".into(), Global::DoubleArray(vec![0., 1., 3., 5.])),
                ("y".into(), Global::DoubleArray(vec![3., 4., 6., 8.])),
                ("z".into(), Global::DoubleArray(vec![4., 5., 7., 9.])),
            ]
            .into_iter()
            .collect(),
        };

        assert_eq!(lower_interps(&graph, &strings), Ok(expected),);
    }

    #[test]
    fn interp2_valid_lowering_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::UnloweredInterp2Valid(UnloweredInterp2 {
                        x: GlobalIdent("x".into()),
                        y: GlobalIdent("y".into()),
                        z: GlobalIdent("z".into()),
                        x_point: ident("a".into()),
                        y_point: ident("b".into()),
                    }),
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "interp",
                    Kind::Interp2Valid {
                        x: vec![0., 1., 3., 5.],
                        y: vec![3., 4., 6., 8.],
                        z: vec![4., 5., 7., 9.],
                        x_point: ident("a".into()),
                        y_point: ident("b".into()),
                    },
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let strings = GlobalTable {
            content: vec![
                ("x".into(), Global::DoubleArray(vec![0., 1., 3., 5.])),
                ("y".into(), Global::DoubleArray(vec![3., 4., 6., 8.])),
                ("z".into(), Global::DoubleArray(vec![4., 5., 7., 9.])),
            ]
            .into_iter()
            .collect(),
        };

        assert_eq!(lower_interps(&graph, &strings), Ok(expected),);
    }
}
