use std::collections::HashMap;

use llvm_ir::DebugLoc as LLDebugLoc;

use crate::graph_node::Node;
use crate::operand::Identifier;
use serde_derive::{Deserialize, Serialize};

// Needed since DebugLoc does not impl `Serialize` and `Deserialize`
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct FileLocation {
    pub line: u32,
    pub col: Option<u32>,
    pub filename: String,
    pub directory: Option<String>,
}
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DebugLink {
    pub parent: Identifier,
    pub description: String,
}

/// Debug info which is matched on but not useful in general
#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum SpecificInfo {
    /// This node is part of the condition for phi node `.0` being reached from `.0`
    PhiCondition(Identifier, Identifier),
    /// This node is part of the exit condition for block `.0`
    ExitCondition(Identifier),
    /// This node is part of the edge mask from basic block .0 to .1
    EdgeMask(Identifier, Identifier),
    /// This node is part of the entry mask for .0
    EntryMask(Identifier),
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum IdentDebugInfo {
    /// An identifier which has a precise location in the original source file
    InFile(Option<FileLocation>),
    /// A node that was created to help another node, usually in a lowering step.
    /// Provides a description of what the link means
    Link(DebugLink),
    Alias(Identifier),
    Specific(SpecificInfo),
    /// A field with arbitrary extra information about the node
    Extra(String),
}
impl IdentDebugInfo {
    pub fn link(parent: &Identifier, description: &str) -> Self {
        IdentDebugInfo::Link(DebugLink {
            parent: parent.clone(),
            description: description.to_string(),
        })
    }
}
impl From<&Option<LLDebugLoc>> for IdentDebugInfo {
    fn from(other: &Option<LLDebugLoc>) -> Self {
        IdentDebugInfo::InFile(other.clone().map(|other| FileLocation {
            line: other.line,
            col: other.col,
            filename: other.filename,
            directory: other.directory,
        }))
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct DebugInfo {
    pub ident_map: HashMap<Identifier, Vec<IdentDebugInfo>>,
}

impl DebugInfo {
    pub fn new() -> Self {
        Self {
            ident_map: HashMap::new(),
        }
    }

    pub fn insert(&mut self, ident: &Identifier, info: &Option<LLDebugLoc>) {
        self.ident_map
            .entry(ident.clone())
            .or_insert(vec![])
            .push(info.into());
    }

    pub fn add_link(&mut self, from: &Identifier, target: &Identifier, description: &str) {
        self.ident_map
            .entry(from.clone())
            .or_insert(vec![])
            .push(IdentDebugInfo::Link(DebugLink {
                parent: target.clone(),
                description: description.to_string(),
            }));
    }

    pub fn add_alias(&mut self, from: &Identifier, target: &Identifier) {
        self.ident_map
            .entry(from.clone())
            .or_insert(vec![])
            .push(IdentDebugInfo::Alias(target.clone()));
    }

    pub fn add_note(&mut self, about: &Identifier, note: &str) {
        self.ident_map
            .entry(about.clone())
            .or_insert(vec![])
            .push(IdentDebugInfo::Extra(note.to_string()))
    }

    pub fn query(&self, query: &Identifier) -> Option<Vec<IdentDebugInfo>> {
        self.ident_map.get(&query).cloned()
    }

    pub fn add_info(&mut self, ident: Identifier, info: Vec<IdentDebugInfo>) {
        self.ident_map
            .entry(ident)
            .or_default()
            .append(&mut info.clone());
    }
}

pub struct WithPurpose<T> {
    pub node: Node,
    pub purpose: T,
}
impl Node {
    pub fn purpose<T>(self, purpose: T) -> WithPurpose<T> {
        WithPurpose {
            node: self,
            purpose,
        }
    }
}
