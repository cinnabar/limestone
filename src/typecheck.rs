use crate::consts::Const;
use crate::error::{Error, InternalError, Result};
use crate::graph::Graph;
use crate::graph_node::Kind;
use crate::operand::Operand;

#[derive(Clone, Debug, PartialEq, Copy)]
pub enum Type {
    Frac,
    Int,
    Bool,
    Struct,
    NonSynth,
}

// TODO: This is going to be pretty slow if it becomse more recursive, for example
// if the add operator gets overloaded. In that case, using an OperandMap<Type> and
// doing a cached traversal of the graph is most likely a better option
pub fn get_type(op: &Operand, graph: &Graph) -> Result<Type> {
    match op {
        Operand::Ident(id) => {
            let node = graph
                .nodes
                .get(id)
                .ok_or(Error::Internal(InternalError::NoSuchOperand(op.clone())))
                .map_err(|e| Error::TypeCheckError(op.clone(), Box::new(e)))?;

            match &node.kind {
                Kind::Nop(op) => get_type(&op, graph),
                Kind::Input(_, _) => Ok(Type::Frac),
                Kind::GreaterThan(_, _) => Ok(Type::Bool),
                Kind::GreaterThanOrEq(_, _) => Ok(Type::Bool),
                Kind::LessThan(_, _) => Ok(Type::Bool),
                Kind::LessThanOrEq(_, _) => Ok(Type::Bool),
                Kind::Equal(_, _) => Ok(Type::Bool),
                Kind::NotEqual(_, _) => Ok(Type::Bool),
                Kind::Add(_, _) => Ok(Type::Frac),
                Kind::Sub(_, _) => Ok(Type::Frac),
                Kind::Div(_, _) => Ok(Type::Frac),
                Kind::Mul(_, _) => Ok(Type::Frac),
                Kind::USub(_) => Ok(Type::Frac),
                Kind::Truncate(_) => Ok(Type::Int),
                Kind::Sqrt(_) => Ok(Type::Frac),
                Kind::Abs(_) => Ok(Type::Frac),
                Kind::Max(_, _) => Ok(Type::Frac),
                Kind::Min(_, _) => Ok(Type::Frac),
                Kind::Not(_) => Ok(Type::Bool),
                Kind::And(_, _) => Ok(Type::Bool),
                Kind::Or(_, _) => Ok(Type::Bool),
                Kind::Select(operands) => {
                    if operands.is_empty() {
                        Err(Error::Internal(InternalError::EmptySelectStatement))
                    } else {
                        operands
                            .iter()
                            // Check the type of each branch
                            .map(|op| get_type(&op.1, graph))
                            .collect::<Result<Vec<_>>>()
                            .and_then(|types| {
                                // Ensure that the types are consistent. We know that
                                // the operands list wasn't empty, so skip and index are
                                // safe
                                if types.iter().skip(1).all(|t| *t == types[0]) {
                                    Ok(types[0])
                                } else {
                                    Err(Error::AmbiguousSelectType(types))
                                }
                            })
                            .map_err(|e| Error::TypeCheckError(op.clone(), Box::new(e)))
                    }
                }
                Kind::IfElse(_, on_true, on_false) => {
                    let true_type = get_type(&on_true, graph)?;
                    let false_type = get_type(&on_false, graph)?;
                    if true_type != false_type {
                        Err(Error::AmbiguousIfType(true_type, false_type))
                    } else {
                        Ok(true_type)
                    }
                }
                Kind::Lookup { .. } => Ok(Type::Frac),
                Kind::Interp1 { .. } => Ok(Type::Frac),
                Kind::Interp1Valid { .. } => Ok(Type::Bool),
                Kind::Interp2 { .. } => Ok(Type::Frac),
                Kind::Interp2Valid { .. } => Ok(Type::Bool),
                Kind::StructAllocation(_) => Ok(Type::Struct),
                Kind::StructWithMembers(_, _) => Ok(Type::Struct),
                Kind::GetElementPtr { .. } => Ok(Type::NonSynth),
                Kind::Load(_) => Ok(Type::NonSynth),
                Kind::Store { .. } => Ok(Type::NonSynth),
                Kind::InsertValue { .. } => Ok(Type::NonSynth),
                Kind::UnloweredInterp1(_) => Ok(Type::NonSynth),
                Kind::UnloweredInterp1Valid(_) => Ok(Type::NonSynth),
                Kind::UnloweredInterp2(_) => Ok(Type::NonSynth),
                Kind::UnloweredInterp2Valid(_) => Ok(Type::NonSynth),
                Kind::InputBound(_, _, _) => Ok(Type::NonSynth),
                Kind::UnboundedInput => Ok(Type::NonSynth),
                Kind::OpaqueBarrier(_) => Ok(Type::Frac),
            }
        }
        Operand::Const(Const::BoolConst(_)) => Ok(Type::Bool),
        Operand::Const(Const::DoubleConst(_)) => Ok(Type::Frac),
        Operand::Const(Const::IntConst(_)) => Ok(Type::Frac),
        Operand::Const(Const::ArrayReference(_)) => Ok(Type::NonSynth),
        Operand::Const(Const::GetElementPtr { .. }) => Ok(Type::NonSynth),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::graph_node::node;
    use crate::operand::ident;

    #[test]
    fn select_typecheck_works_on_fracs() {
        let graph = Graph::from_tuples(vec![
            node("subnode", Kind::Add(ident(0.into()), ident(1.into()))),
            node(
                "selectFrac",
                Kind::Select(
                    vec![
                        (vec![].into(), ident("subnode".into())),
                        (vec![].into(), Operand::Const(Const::DoubleConst(1.))),
                    ]
                    .into(),
                ),
            ),
        ]);

        let result = get_type(&ident("selectFrac".into()), &graph);
        assert_eq!(result, Ok(Type::Frac));
    }
    #[test]
    fn select_typecheck_works_on_bools() {
        let graph = Graph::from_tuples(vec![node(
            "selectBool",
            Kind::Select(
                vec![
                    (vec![].into(), Operand::Const(Const::BoolConst(true))),
                    (vec![].into(), Operand::Const(Const::BoolConst(false))),
                ]
                .into(),
            ),
        )]);

        let result = get_type(&ident("selectBool".into()), &graph);
        assert_eq!(result, Ok(Type::Bool));
    }

    #[test]
    fn select_typecheck_fails_on_ambiguity() {
        let graph = Graph::from_tuples(vec![node(
            "select",
            Kind::Select(
                vec![
                    (vec![].into(), Operand::Const(Const::BoolConst(true))),
                    (vec![].into(), Operand::Const(Const::DoubleConst(1.))),
                ]
                .into(),
            ),
        )]);

        let result = get_type(&ident("select".into()), &graph);
        let expected = Err(Error::TypeCheckError(
            ident("select".into()),
            Box::new(Error::AmbiguousSelectType(vec![Type::Bool, Type::Frac])),
        ));
        assert_eq!(result, expected);
    }
}
