use std::collections::{HashMap, HashSet};

use thiserror::Error;

use crate::debuginfo::DebugInfo;
use crate::graph::Graph;
use crate::graph_node::{Kind, Node};
use crate::id_tracker::IdTracker;
use crate::operand::{ident, Identifier, Operand};
use crate::uniquevec::UniqueVec;

use super::util::get_get_element_ptr;
use crate::util::resolve_operand;

#[derive(Debug, Clone, PartialEq, Error)]
pub enum Error {
    #[error("Expected load of identifier, found: {0:?}")]
    NonIdentLoad(Operand),
    #[error("Attempting to load non-struct {0:?}")]
    NonStructLoad(Operand),

    #[error("Expected to find a GetElementPtr")]
    NoGetElementPtr(#[from] super::util::GetElementPtrError),
    #[error("Struct store to non-identifier {0:?}")]
    StoreToNonIdent(Operand),
    #[error("Attempting to store to {0:?} which is not a struct")]
    NonStructStore(Identifier),
    #[error("GetElementPtr ({0:?}) must have 1 index, found {1}")]
    IncorrectIndexAmount(Identifier, usize),
    #[error("Expected first index to be 0 but found {1}. Is this an array lookup? {0:?}")]
    NonZeroFirstIndex(Identifier, u64),
    #[error("Struct store address must be constant")]
    NonConstIndex,
    #[error("Struct store address must be integer")]
    NonIntIndex,
    #[error("Multiple stores to index {1} of struct {0}")]
    MultipleStores(String, u64),

    #[error("There is an allocation of struct {0} called {1:?}, but nothing writes to it")]
    NoMembersForAllocation(String, Identifier),
}

pub type Result<T> = std::result::Result<T, Error>;

/// Lowers the pattern load(struct_alloc) to Nop(struct_alloc)
pub fn lower_struct_load(
    (name, load_operand): (&Identifier, &Operand),
    graph: &Graph,
) -> Result<Node> {
    let load_target = resolve_operand(load_operand, graph);
    match load_target {
        Operand::Ident(ref target) => {
            let load_node = &graph.nodes[&target];

            if let Node {
                kind: Kind::StructAllocation(_),
                ..
            } = load_node
            {
                Ok(Node::new(name.clone(), Kind::Nop(load_target.clone())))
            } else {
                Err(Error::NonStructLoad(load_operand.clone()))
            }
        }
        other => Err(Error::NonIdentLoad(other.clone()))?,
    }
}

enum Purpose {
    MemberSelect,
}
type WithPurpose = crate::debuginfo::WithPurpose<Purpose>;

/// Keeps track of assignments to struct members.
struct Struct {
    // Values are on the form (value, condition)
    pub members: HashMap<u64, Vec<(Operand, Option<Operand>)>>,
}

impl Struct {
    pub fn new() -> Self {
        Self {
            members: HashMap::new(),
        }
    }

    /// Add an assignment of `value` to the `member` of the struct which happens
    /// assuming `Condition` holds.
    /// (value, condition)
    pub fn add_member_assignment(&mut self, member: u64, value: (Operand, Option<Operand>)) {
        self.members.entry(member).or_insert(vec![]).push(value)
    }

    /// Returns the nodes which the struct "aliases". If a member only has one assignment,
    /// that operand is returned, if not, a new `Select` node is created, and its value
    /// is returned
    pub fn get_members(&self, id_tracker: &mut IdTracker) -> (Vec<Operand>, Vec<WithPurpose>) {
        // TODO: Check the amount of members here
        // Members need to be sorted
        let mut members_tuples = self.members.iter().collect::<Vec<_>>();
        members_tuples.sort_by_key(|(i, _)| *i);

        let mut members = vec![];
        let mut new_nodes = vec![];
        for (_, values) in members_tuples {
            // If there is only one member assignment, this is a normal alias
            if values.len() == 1 {
                members.push(values[0].0.clone())
            } else {
                let mut select_options = UniqueVec::new();
                for (value, condition) in values {
                    let cond_op = if let Some(condition) = condition {
                        vec![condition.clone()].into()
                    } else {
                        UniqueVec::new()
                    };
                    select_options.push((cond_op, value.clone()))
                }
                let new_id = id_tracker.next_no_purpose();
                new_nodes.push(
                    Node::new(new_id, Kind::Select(select_options)).purpose(Purpose::MemberSelect),
                );
                members.push(ident(Identifier::Id(new_id)));
            }
        }

        (members, new_nodes)
    }
}

/// Resolves stores to struct members which take the form
///
/// store(getelementptr(struct instance, offset))
///
/// These instructions are removed, and a mapping between struct members is created
pub fn lower_struct_stores(
    graph: Graph,
    id_tracker: &mut IdTracker,
    debug: &mut DebugInfo,
) -> Result<Graph> {
    // Mapping of operands to struct members.
    let mut struct_members = HashMap::new();

    let mut first_result = Graph::empty();
    for (name, Node { kind, .. }) in &graph.nodes {
        match kind {
            Kind::Store {
                addr,
                value,
                condition,
            } => {
                // The store has to point to an addres generated by a get element ptr
                // instruction
                let elementptr = get_get_element_ptr(&graph, &addr)?;

                // That address has to resolve to a struct pointer
                let struct_address = elementptr
                    .address
                    .expect_ident()
                    .ok_or(Error::StoreToNonIdent(elementptr.address.clone()))?;

                // Get the actual struct, erroring if it doesn't exist
                let struct_name = assume_struct_allocation(&graph.nodes[&struct_address])
                    .ok_or(Error::NonStructStore(struct_address.clone()))?;

                // Find the index of the store
                if elementptr.indices.len() != 2 {
                    Err(Error::IncorrectIndexAmount(
                        addr.clone(),
                        elementptr.indices.len(),
                    ))?
                }

                // The first index must be 0 for now
                let first_index = elementptr.indices[0]
                    .expect_const()
                    .ok_or(Error::NonConstIndex)?
                    .expect_int()
                    .ok_or(Error::NonIntIndex)?;

                if first_index != 0 {
                    Err(Error::NonZeroFirstIndex(addr.clone(), first_index))?;
                }

                // Get the index of the member to write to
                let index = elementptr.indices[1]
                    .expect_const()
                    .ok_or(Error::NonConstIndex)?
                    .expect_int()
                    .ok_or(Error::NonIntIndex)?;

                // If this is the first time we encounter this struct, create it
                let target_struct = struct_members.entry(struct_name).or_insert(Struct::new());

                target_struct.add_member_assignment(index, (value.clone(), condition.clone()));
            }
            _ => {
                first_result.add_node(Node::new(name.clone(), kind.clone()));
            }
        }
    }

    // Make a second pass that replaces struct allocations with their defintions and
    // members
    let mut result = Graph::empty();
    for (node_name, Node { kind, .. }) in &first_result.nodes {
        match kind {
            Kind::StructAllocation(struct_name) => {
                let struct_ob = struct_members.get(struct_name.as_str()).ok_or(
                    Error::NoMembersForAllocation(struct_name.clone(), node_name.clone()),
                )?;

                let (members, new_nodes) = struct_ob.get_members(id_tracker);

                for node in new_nodes {
                    let description = match node.purpose {
                        Purpose::MemberSelect => "Select node added to select member conditionally",
                    };
                    result
                        .add_node(node.node)
                        .debug_link(debug, &node_name, description);
                }
                result.add_node(Node::new(
                    node_name.clone(),
                    Kind::StructWithMembers(struct_name.clone(), members),
                ));
            }
            _ => {
                result.add_node(Node::new(node_name.clone(), kind.clone()));
            }
        }
    }

    // Ensure that the structs have stores to each of their members, and add corresponding
    // nodes
    Ok(result)
}

fn assume_struct_allocation(node: &Node) -> Option<&str> {
    if let Node {
        kind: Kind::StructAllocation(name),
        ..
    } = node
    {
        Some(name)
    } else {
        None
    }
}

pub fn lower_struct_insertvalue(graph: Graph) -> Result<Graph> {
    // We are looking for a chain of insertvalue operations with the first one being
    // `undef`
    // Contains insertvalue nodes mapped from their name
    let mut relevant_nodes = HashMap::new();
    // Map of nodes referenced as the `previous` value of an insertvalue. Nodes
    // in the relevant_nodes list but not in this are fully constructed structs.
    let mut referened = HashSet::new();

    let mut result = Graph::empty();
    for (name, node) in &graph.nodes {
        match &node.kind {
            Kind::InsertValue {
                previous,
                value,
                index,
            } => {
                if let Some(prev) = previous {
                    referened.insert(prev);
                };
                relevant_nodes.insert(name, (previous, value, index));
            }
            _ => {
                result.add_node(node.clone());
            }
        }
    }

    // Create structs from the recovered elements
    for (name, _) in &relevant_nodes {
        if !referened.contains(name) {
            let mut members = HashMap::new();
            let mut curr = Some((*name).clone());
            while let Some(next) = curr {
                let (next_node, value, index) = relevant_nodes[&next];
                members.insert(index, value);

                curr = next_node.clone()
            }
            // Found a bottom level initialisation, build the full struct
            let mut members = members.into_iter().collect::<Vec<_>>();

            members.sort_by_key(|(idx, _)| *idx);

            // We hope/assume that the construction is *complete*, i.e. the indices are
            // consecutive and start at 0. Check that assumption just to be sure
            assert!(
                members
                    .iter()
                    .enumerate()
                    .all(|(i, (idx, _))| i as u32 == **idx),
                "Missing some members in the struct insertvalue operation. {:?}",
                members.iter().map(|(idx, _)| idx).collect::<Vec<_>>()
            );

            let members = members.into_iter().map(|(_, val)| val).cloned().collect();
            result.add_node(Node::new(
                (*name).clone(),
                Kind::StructWithMembers((*name).into(), members),
            ));
        }
    }

    Ok(result)
}

#[cfg(test)]
mod test {
    use super::*;

    use std::collections::HashMap;

    use crate::consts::Const;
    use crate::graph::Graph;
    use crate::graph_node::{node, Kind};
    use crate::operand::ident;

    #[test]
    fn struct_load_lowering_works() {
        let graph = Graph {
            nodes: vec![
                node("dummy", Kind::Nop(ident("a".into()))),
                node("load", Kind::Load(ident("nop".into()))),
                node("nop", Kind::Nop(ident("struct".into()))),
                node("struct", Kind::StructAllocation("Struct".to_string())),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let result = lower_struct_load((&"load".into(), &ident("nop".into())), &graph);

        assert_eq!(
            result,
            Ok(Node::new("load", Kind::Nop(ident("struct".into()))))
        );
    }

    #[test]
    fn store_lowering_works() {
        let graph = Graph {
            nodes: vec![
                node("struct", Kind::StructAllocation("Struct".to_string())),
                node(
                    "addr1",
                    Kind::GetElementPtr {
                        address: ident("struct".into()),
                        indices: vec![
                            Operand::Const(Const::IntConst(0)),
                            Operand::Const(Const::IntConst(0)),
                        ],
                    },
                ),
                node(
                    "store1",
                    Kind::Store {
                        addr: "addr1".into(),
                        value: ident("value1".into()),
                        condition: None,
                    },
                ),
                node(
                    "addr2",
                    Kind::GetElementPtr {
                        address: ident("struct".into()),
                        indices: vec![
                            Operand::Const(Const::IntConst(0)),
                            Operand::Const(Const::IntConst(1)),
                        ],
                    },
                ),
                node(
                    "store2",
                    Kind::Store {
                        addr: "addr2".into(),
                        value: ident("value2".into()),
                        condition: None,
                    },
                ),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph {
            nodes: vec![
                node(
                    "struct",
                    Kind::StructWithMembers(
                        "Struct".to_string(),
                        vec![ident("value1".into()), ident("value2".into())],
                    ),
                ),
                node(
                    "addr1",
                    Kind::GetElementPtr {
                        address: ident("struct".into()),
                        indices: vec![
                            Operand::Const(Const::IntConst(0)),
                            Operand::Const(Const::IntConst(0)),
                        ],
                    },
                ),
                node(
                    "addr2",
                    Kind::GetElementPtr {
                        address: ident("struct".into()),
                        indices: vec![
                            Operand::Const(Const::IntConst(0)),
                            Operand::Const(Const::IntConst(1)),
                        ],
                    },
                ),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let mut debug = DebugInfo::new();
        let mut id_tracker = IdTracker::new(100);
        let result = lower_struct_stores(graph, &mut id_tracker, &mut debug);

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn insertvalue_lowering_works() {
        let graph = Graph {
            nodes: vec![
                node(
                    "second",
                    Kind::InsertValue {
                        previous: Some("first".into()),
                        value: ident("value2".into()),
                        index: 1,
                    },
                ),
                node(
                    "first",
                    Kind::InsertValue {
                        previous: None,
                        value: ident("value1".into()),
                        index: 0,
                    },
                ),
                node(
                    "third",
                    Kind::InsertValue {
                        previous: Some("second".into()),
                        value: ident("value3".into()),
                        index: 2,
                    },
                ),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph {
            nodes: vec![node(
                "third",
                Kind::StructWithMembers(
                    "$third".to_string(),
                    vec![
                        ident("value1".into()),
                        ident("value2".into()),
                        ident("value3".into()),
                    ],
                ),
            )]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let result = lower_struct_insertvalue(graph);

        assert_eq!(result, Ok(expected));
    }
}
