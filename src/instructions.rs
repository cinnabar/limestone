use crate::error::{Error, Result};
use crate::graph_node::{Kind, Node};
use crate::id_tracker::IdTracker;
use crate::operand::{Identifier, Operand};

use llvm_ir::Operand as LlvmOperand;
use llvm_ir::{instruction, FPPredicate};
use llvm_ir::{Constant, IntPredicate};

macro_rules! binop {
    ($kind:ident, $instr:expr) => {
        Ok(Node::new(
            $instr.dest.clone(),
            Kind::$kind(
                Operand::from_llvm(&$instr.operand0)?,
                Operand::from_llvm(&$instr.operand1)?,
            ),
        ))
    };
}

// Basic binops
pub fn add(ins: &instruction::Add) -> Result<Node> {
    binop!(Add, ins)
}
pub fn sub(ins: &instruction::Sub) -> Result<Node> {
    binop!(Sub, ins)
}
pub fn fadd(ins: &instruction::FAdd) -> Result<Node> {
    binop!(Add, ins)
}
pub fn fsub(ins: &instruction::FSub) -> Result<Node> {
    binop!(Sub, ins)
}
pub fn fdiv(ins: &instruction::FDiv) -> Result<Node> {
    binop!(Div, ins)
}
pub fn fmul(ins: &instruction::FMul) -> Result<Node> {
    binop!(Mul, ins)
}
pub fn fneg(ins: &instruction::FNeg) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::USub(Operand::from_llvm(&ins.operand)?),
    ))
}

pub fn sext(ins: &instruction::SExt) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::Nop(Operand::from_llvm(&ins.operand)?),
    ))
}
pub fn zext(ins: &instruction::ZExt) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::Nop(Operand::from_llvm(&ins.operand)?),
    ))
}
pub fn uitofp(ins: &instruction::UIToFP) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::Nop(Operand::from_llvm(&ins.operand)?),
    ))
}
pub fn sitofp(ins: &instruction::SIToFP) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::Nop(Operand::from_llvm(&ins.operand)?),
    ))
}

pub fn fcmp(ins: &instruction::FCmp) -> Result<Node> {
    match ins {
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::OGT,
            ..
        } => binop!(GreaterThan, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::OLT,
            ..
        } => binop!(LessThan, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::OGE,
            ..
        } => binop!(GreaterThanOrEq, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::UGE,
            ..
        } => binop!(GreaterThanOrEq, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::OLE,
            ..
        } => binop!(LessThanOrEq, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::ULE,
            ..
        } => binop!(LessThanOrEq, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::OEQ,
            ..
        } => binop!(Equal, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::ULT,
            ..
        } => binop!(LessThan, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::UGT,
            ..
        } => binop!(GreaterThan, ins),
        ins
        @ instruction::FCmp {
            predicate: FPPredicate::UNE,
            ..
        } => binop!(NotEqual, ins),
        instruction::FCmp {
            predicate: other_predicate,
            ..
        } => Err(Error::UnhandledFPPredicate(*other_predicate))?,
    }
}
pub fn icmp(ins: &instruction::ICmp) -> Result<Node> {
    log::warn!(
        "Using icmp which will be casted to fixed point numbers. Correctness is not guaranteed"
    );
    match ins {
        ins
        @ instruction::ICmp {
            predicate: IntPredicate::EQ,
            ..
        } => {
            log::error!("Integer eq instructions are done with fixpoint");
            binop!(Equal, ins)
        }
        ins
        @ instruction::ICmp {
            predicate: IntPredicate::NE,
            ..
        } => {
            log::error!("Integer neq instructions are done with fixpoint");
            binop!(NotEqual, ins)
        }
        instruction::ICmp {
            predicate: other_predicate,
            ..
        } => Err(Error::UnhandledIntPredicate(*other_predicate))?,
    }
}

pub fn or(ins: &instruction::Or) -> Result<Node> {
    binop!(Or, ins)
}
pub fn and(ins: &instruction::And) -> Result<Node> {
    binop!(And, ins)
}

pub fn select(ins: &instruction::Select) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::IfElse(
            Operand::from_llvm(&ins.condition)?,
            Operand::from_llvm(&ins.true_value)?,
            Operand::from_llvm(&ins.false_value)?,
        ),
    ))
}

pub fn get_element_ptr(ins: &instruction::GetElementPtr) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::GetElementPtr {
            address: Operand::from_llvm(&ins.address)?,
            indices: ins
                .indices
                .iter()
                .map(Operand::from_llvm)
                .collect::<Result<Vec<_>>>()?,
        },
    ))
}

pub fn alloca(ins: &instruction::Alloca) -> Result<Node> {
    match ins.allocated_type.as_ref() {
        llvm_ir::Type::NamedStructType { name, .. } => Ok(Node::new(
            ins.dest.clone(),
            Kind::StructAllocation(name.clone()),
        )),
        t => Err(Error::UnsupportedAllocation(t.clone())),
    }
}

pub fn store(
    ins: &instruction::Store,
    condition: Option<Operand>,
    id_tracker: &mut IdTracker,
) -> Result<Node> {
    let addr = match &ins.address {
        LlvmOperand::LocalOperand { name, .. } => Identifier::from(name),
        _ => Err(Error::NonAliasingStore)?,
    };

    let value = Operand::from_llvm(&ins.value)?;

    let new_node_name = Identifier::Id(id_tracker.next_no_purpose());

    Ok(Node::new(
        new_node_name,
        Kind::Store {
            addr,
            value,
            condition,
        },
    ))
}

pub fn load(ins: &instruction::Load) -> Result<Node> {
    match ins.address.as_constant() {
        Some(Constant::GetElementPtr(_inner)) => {
            unimplemented!["Constant loads *should* be constant folded away by llvm"]
        }
        _ => Ok(Node::new(
            ins.dest.clone(),
            Kind::Load(Operand::from_llvm(&ins.address)?),
        )),
    }
}

pub fn bitcast(ins: &instruction::BitCast) -> Result<Node> {
    Ok(Node::new(
        ins.dest.clone(),
        Kind::Nop(Operand::from_llvm(&ins.operand)?),
    ))
}

pub fn insert_value(ins: &instruction::InsertValue) -> Result<Node> {
    let instruction::InsertValue {
        aggregate,
        element,
        indices,
        dest,
        debugloc: _,
    } = ins;

    let previous = match aggregate.as_constant() {
        Some(Constant::Undef(_)) => None,
        _ => match aggregate {
            LlvmOperand::LocalOperand { name, ty: _ } => Some(name.into()),
            other => return Err(Error::InvalidInsertValueAggregate(other.clone())),
        },
    };

    let index = match indices.as_slice() {
        [index] => *index,
        _ => return Err(Error::InvalidInsertValueIndices(indices.clone())),
    };

    Ok(Node::new(
        dest.clone(),
        Kind::InsertValue {
            previous,
            value: Operand::from_llvm(&element)?,
            index,
        },
    ))
}

// This function is declared here because graphgen uses a macro that assumes that all
// instruction handlers are defined in instructions.rs. To define this in `functions`
// we would have to export every function there which also wouldn't be super pretty.
pub fn call(ins: &instruction::Call) -> Result<Option<Node>> {
    crate::functions::call(ins)
}

// Intrinsic functions

#[cfg(test)]
mod instruction_tests {
    use super::*;

    use crate::consts::Const;
    use crate::graph_node::{Kind, Node};
    use crate::operand::{ident, Operand};
    use crate::testutil::*;

    use llvm_ir::instruction::{self, BitCast, GetElementPtr, InsertValue, ZExt};
    use llvm_ir::types::FPType;
    use llvm_ir::Operand as LlvmOperand;
    use llvm_ir::{types::Types, Constant, ConstantRef};

    // use instruction::BitCast;
    // use std::sync::{Arc, RwLock};

    macro_rules! test_binop {
        ($instruction:ident, $handler:ident => $kind:ident) => {
            let types = Types::blank_for_testing();
            let instruction = instruction::$instruction {
                operand0: f32var("x", &types),
                operand1: f32var("y", &types),
                dest: "z".into(),
                debugloc: None,
            };

            assert_eq!(
                $handler(&instruction),
                Ok(Node {
                    name: "z".into(),
                    kind: Kind::$kind(ident("x".into()), ident("y".into()))
                }),
            );
        };
    }

    #[test]
    fn add_works() {
        test_binop!(Add, add => Add);
        test_binop!(FAdd, fadd => Add);
    }
    #[test]
    fn sub_works() {
        test_binop!(Sub, sub => Sub);
        test_binop!(FSub, fsub => Sub);
    }
    #[test]
    fn div_works() {
        test_binop!(FDiv, fdiv => Div);
    }
    #[test]
    fn mul_works() {
        test_binop!(FMul, fmul => Mul);
    }

    #[test]
    fn and_works() {
        test_binop!(And, and => And);
    }
    #[test]
    fn or_works() {
        test_binop!(Or, or => Or);
    }

    #[test]
    fn fneg_works() {
        let types = Types::blank_for_testing();
        let instruction = instruction::FNeg {
            operand: f32var("x", &types),
            dest: "y".into(),
            debugloc: None,
        };

        assert_eq!(
            fneg(&instruction),
            Ok(Node {
                name: "y".into(),
                kind: Kind::USub(ident("x".into()))
            }),
        );
    }

    #[test]
    fn fcmp_works() {
        let types = Types::blank_for_testing();

        macro_rules! test_fcmp {
            ($predicate:ident => $kind:ident) => {
                let instruction = instruction::FCmp {
                    predicate: FPPredicate::$predicate,
                    operand0: f32var("x", &types),
                    operand1: f32var("y", &types),
                    dest: "z".into(),
                    debugloc: None,
                };

                assert_eq!(
                    fcmp(&instruction),
                    Ok(Node {
                        name: "z".into(),
                        kind: Kind::$kind(ident("x".into()), ident("y".into()))
                    }),
                );
            };
        }

        test_fcmp!(OGT => GreaterThan);
        test_fcmp!(OLT => LessThan);
        test_fcmp!(OGE => GreaterThanOrEq);
        test_fcmp!(OLE => LessThanOrEq);
        test_fcmp!(ULT => LessThan);
        test_fcmp!(UGT => GreaterThan);
        test_fcmp!(OEQ => Equal);
    }

    #[test]
    fn get_element_ptr_works() {
        let types = Types::blank_for_testing();
        let instruction = GetElementPtr {
            address: i32var("x", &types),
            indices: vec![LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::Int { bits: 64, value: 0 },
            ))],
            dest: "arrayidx".into(),
            in_bounds: true,
            debugloc: None,
        };

        let expected = Node::new(
            "arrayidx",
            Kind::GetElementPtr {
                address: ident("x".into()),
                indices: vec![Operand::Const(Const::IntConst(0))],
            },
        );

        assert_eq!(expected, get_element_ptr(&instruction).unwrap());
    }

    #[test]
    fn sext_produces_nop() {
        let types = Types::blank_for_testing();
        let ins = instruction::SExt {
            operand: LlvmOperand::LocalOperand {
                name: "in".into(),
                ty: types.int(32),
            },
            to_type: types.int(64),
            dest: "out".into(),
            debugloc: None,
        };

        let expected = Node::new("out", Kind::Nop(ident("in".into())));

        assert_eq!(sext(&ins), Ok(expected));
    }

    #[test]
    fn uitofp_works() {
        let types = Types::blank_for_testing();
        let ins = instruction::UIToFP {
            operand: LlvmOperand::LocalOperand {
                name: "nx".into(),
                ty: types.int(64),
            },
            to_type: types.fp(FPType::Double),
            dest: "conv".into(),
            debugloc: None,
        };

        let expected = Node::new("conv", Kind::Nop(ident("nx".into())));

        assert_eq!(uitofp(&ins), Ok(expected));
    }

    #[test]
    fn select_works() {
        let types = Types::blank_for_testing();
        let instruction = instruction::Select {
            condition: f32var("c", &types),
            true_value: f32var("x", &types),
            false_value: f32var("y", &types),
            dest: "dest".into(),
            debugloc: None,
        };

        let expected = Node::new(
            "dest",
            Kind::IfElse(ident("c".into()), ident("x".into()), ident("y".into())),
        );

        assert_eq!(select(&instruction), Ok(expected));
    }

    #[test]
    fn struct_allocation_instruction_works() {
        let types = Types::blank_for_testing();

        let instruction = instruction::Alloca {
            allocated_type: types.named_struct("struct.Result"),
            num_elements: LlvmOperand::ConstantOperand(ConstantRef::new(Constant::Int {
                bits: 32,
                value: 1,
            })),
            dest: "retval".into(),
            alignment: 8,
            debugloc: None,
        };

        let expected = Node::new("retval", Kind::StructAllocation("struct.Result".into()));

        let result = alloca(&instruction);

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn store_instructions_are_handled() {
        let types = Types::blank_for_testing();
        let instruction = instruction::Store {
            address: LlvmOperand::LocalOperand {
                name: "a1".into(),
                ty: types.pointer_to(types.fp(FPType::Double)),
            },
            value: LlvmOperand::LocalOperand {
                name: "a".into(),
                ty: types.fp(FPType::Double),
            },
            volatile: false,
            atomicity: None,
            alignment: 8,
            debugloc: None,
        };

        let expected = Node::new(
            100,
            Kind::Store {
                addr: "a1".into(),
                value: ident("a".into()),
                condition: None,
            },
        );

        let result = store(&instruction, None, &mut IdTracker::new(100));

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn store_instructions_with_conditions_are_handled() {
        let types = Types::blank_for_testing();
        let instruction = instruction::Store {
            address: LlvmOperand::LocalOperand {
                name: "a1".into(),
                ty: types.pointer_to(types.fp(FPType::Double)),
            },
            value: LlvmOperand::LocalOperand {
                name: "a".into(),
                ty: types.fp(FPType::Double),
            },
            volatile: false,
            atomicity: None,
            alignment: 8,
            debugloc: None,
        };

        let expected = Node::new(
            100,
            Kind::Store {
                addr: "a1".into(),
                value: ident("a".into()),
                condition: Some(ident(1.into())),
            },
        );

        let result = store(
            &instruction,
            Some(ident(1.into())),
            &mut IdTracker::new(100),
        );

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn bitcast_is_a_no_op() {
        let types = Types::blank_for_testing();
        let instruction = BitCast {
            operand: LlvmOperand::LocalOperand {
                name: "retval".into(),
                ty: types.named_struct("struct.Result"),
            },
            to_type: types.pointer_to(types.struct_of(
                vec![types.fp(FPType::Double), types.fp(FPType::Double)],
                false,
            )),
            dest: 0.into(),
            debugloc: None,
        };

        let result = bitcast(&instruction);

        let expected = Node::new(Identifier::Id(0), Kind::Nop(ident("retval".into())));

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn zext_is_a_nop() {
        let types = Types::blank_for_testing();
        let instruction = ZExt {
            operand: LlvmOperand::LocalOperand {
                name: "retval".into(),
                ty: types.named_struct("struct.Result"),
            },
            to_type: types.int(32),
            dest: 0.into(),
            debugloc: None,
        };

        let result = zext(&instruction);

        let expected = Node::new(Identifier::Id(0), Kind::Nop(ident("retval".into())));

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn insertvalue_works() {
        let types = Types::blank_for_testing();
        let instruction = InsertValue {
            aggregate: LlvmOperand::ConstantOperand(ConstantRef::new(llvm_ir::Constant::Undef(
                types.named_struct("struct.Result"),
            ))),
            element: LlvmOperand::LocalOperand {
                name: "x1".into(),
                ty: types.fp(FPType::Double),
            },
            indices: vec![0],
            dest: 0.into(),
            debugloc: None,
        };

        let result = insert_value(&instruction);

        let expected = Node::new(
            Identifier::Id(0),
            Kind::InsertValue {
                previous: None,
                value: Operand::Ident("x1".into()),
                index: 0,
            },
        );

        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn insertvalue_works_with_previous_struct() {
        let types = Types::blank_for_testing();
        let instruction = InsertValue {
            aggregate: LlvmOperand::LocalOperand {
                name: "prev".into(),
                ty: types.fp(FPType::Double),
            },
            element: LlvmOperand::LocalOperand {
                name: "x1".into(),
                ty: types.fp(FPType::Double),
            },
            indices: vec![0],
            dest: 0.into(),
            debugloc: None,
        };

        let result = insert_value(&instruction);

        let expected = Node::new(
            Identifier::Id(0),
            Kind::InsertValue {
                previous: Some("prev".into()),
                value: Operand::Ident("x1".into()),
                index: 0,
            },
        );

        assert_eq!(result, Ok(expected));
    }
}
