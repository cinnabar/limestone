use std::collections::HashMap;
use std::convert::TryFrom;

use llvm_ir::Name;
use llvm_ir::Operand as LlvmOperand;

use serde_derive::{Deserialize, Serialize};

use crate::consts::Const;
use crate::error::{Error, Result};

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[serde(into = "String", try_from = "TaggedIdentifier")]
pub enum Identifier {
    Str(String),
    Id(usize),
}

impl From<Name> for Identifier {
    fn from(other: Name) -> Self {
        match other {
            Name::Name(s) => Identifier::Str(s.to_string()),
            Name::Number(i) => Identifier::Id(i),
        }
    }
}

impl From<&Name> for Identifier {
    fn from(other: &Name) -> Self {
        other.clone().into()
    }
}

impl From<usize> for Identifier {
    fn from(id: usize) -> Self {
        Identifier::Id(id)
    }
}
impl From<&str> for Identifier {
    fn from(id: &str) -> Self {
        Identifier::Str(id.to_string())
    }
}

impl From<Identifier> for String {
    fn from(other: Identifier) -> Self {
        (&other).into()
    }
}
impl From<&Identifier> for String {
    fn from(other: &Identifier) -> Self {
        match other {
            Identifier::Id(id) => format!("%{}", id),
            Identifier::Str(val) => format!("${}", val),
        }
    }
}

// For serialisation purposes, we need a wrapper structs around the string formed by
// Identifier::into<String>().
#[derive(Deserialize)]
#[serde(transparent)]
struct TaggedIdentifier(String);

impl TryFrom<TaggedIdentifier> for Identifier {
    type Error = &'static str;

    fn try_from(value: TaggedIdentifier) -> std::result::Result<Self, Self::Error> {
        match value
            .0
            .chars()
            .nth(0)
            .ok_or("Zero length tagged identifiers are not allowed")?
        {
            '%' => {
                let as_num = (value.0[1..])
                    .parse::<usize>()
                    .map_err(|_| "Failed to parse number")?;
                Ok(Identifier::from(as_num))
            }
            '$' => Ok(Identifier::from(&value.0[1..])),
            _ => Err("Not a valid prefix"),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum Operand {
    Const(Const),
    Ident(Identifier),
}

impl Operand {
    pub fn from_llvm(operand: &LlvmOperand) -> Result<Operand> {
        match operand {
            LlvmOperand::LocalOperand { name, .. } => Ok(Operand::Ident(name.into())),
            LlvmOperand::ConstantOperand(constant) => Const::try_from(constant)
                .map(Operand::Const)
                .map_err(Error::ConstConversionError),
            a @ LlvmOperand::MetadataOperand => Err(Error::UnnamedOperand(a.clone())),
        }
    }

    pub fn from_llvm_filter_undef(operand: &LlvmOperand) -> Result<Option<Operand>> {
        let is_undef = operand
            .as_constant()
            .and_then(|c| {
                if let llvm_ir::Constant::Undef(_) = c {
                    Some(true)
                } else {
                    Some(false)
                }
            })
            .unwrap_or(false);

        if is_undef {
            Ok(None)
        } else {
            Ok(Some(Self::from_llvm(operand)?))
        }
    }

    /// Returns Some(Identifier) if the operand is an identifier, otherwise None
    pub fn expect_ident(&self) -> Option<&Identifier> {
        match self {
            Operand::Ident(x) => Some(x),
            _ => None,
        }
    }

    pub fn expect_const(&self) -> Option<&Const> {
        match self {
            Operand::Const(x) => Some(x),
            _ => None,
        }
    }
}

impl From<Identifier> for Operand {
    fn from(other: Identifier) -> Self {
        Operand::Ident(other)
    }
}

impl From<&Identifier> for Operand {
    fn from(other: &Identifier) -> Self {
        Operand::from(other.clone())
    }
}

/// Shorthand constructor for Operand::Ident
pub fn ident(inner: Identifier) -> Operand {
    Operand::Ident(inner)
}

/// Key value store for operands. Identifiers are stored and looked up using
/// a HashMap, constants using a vector with PartialEq for equality.
///
/// NOTE: This map becomes invalid after optimisation, as some nodes will be replaced
#[derive(Debug)]
pub struct OperandMap<T> {
    idents: HashMap<Identifier, T>,
    consts: Vec<(Operand, T)>,
}

impl<T> OperandMap<T> {
    pub fn new() -> Self {
        OperandMap {
            idents: HashMap::new(),
            consts: vec![],
        }
    }

    /// Insert a new value into the map. Panics if a key with the same value was already
    /// present
    pub fn insert(&mut self, key: Operand, value: T) {
        if let Operand::Ident(key) = key {
            if let Some(_) = self.idents.insert(key, value) {
                panic!("Inserting a value with a key that is already present")
            }
        } else {
            let already_present = self.consts.iter().any(|(k, _)| k == &key);
            if !already_present {
                self.consts.push((key, value))
            } else {
                panic!("Inserting a value with a key that is already present")
            }
        }
    }

    pub fn get(&self, key: &Operand) -> Option<&T> {
        if let Operand::Ident(key) = key {
            self.idents.get(key)
        } else {
            self.consts
                .iter()
                .find_map(|(k, v)| if k == key { Some(v) } else { None })
        }
    }

    pub fn get_mut(&mut self, key: &Operand) -> Option<&mut T> {
        if let Operand::Ident(key) = key {
            self.idents.get_mut(key)
        } else {
            self.consts
                .iter_mut()
                .find_map(|(k, v)| if k == key { Some(v) } else { None })
        }
    }

    // Returns a vector of all the values present in the operand map in unspecified order
    pub fn values(self) -> Vec<T> {
        let mut result: Vec<T> = self.idents.into_iter().map(|(_, v)| v).collect();
        result.append(&mut self.consts.into_iter().map(|(_, v)| v).collect());
        result
    }

    pub fn values_ref<'a>(&'a self) -> Vec<&'a T> {
        let mut result: Vec<&T> = self.idents.iter().map(|(_, v)| v).collect();
        result.append(&mut self.consts.iter().map(|(_, v)| v).collect());
        result
    }
}

impl<T: PartialEq> PartialEq for OperandMap<T> {
    fn eq(&self, other: &Self) -> bool {
        if other.idents != self.idents {
            false
        } else if self.consts.len() != other.consts.len() {
            false
        } else {
            // Compare the vectors. This is quite slow for now, but easy
            // to implement
            for (k, v) in &self.consts {
                if other.get(&k) != Some(&v) {
                    return false;
                }
            }
            true
        }
    }
}

#[cfg(test)]
mod kvs_tests {
    use super::*;

    #[test]
    fn ident_insertion_works() {
        let mut kvs = OperandMap::new();

        kvs.insert(ident("a".into()), 1);
        kvs.insert(ident("b".into()), 2);

        let expected: HashMap<_, _> = vec![("a".into(), 1), ("b".into(), 2)].into_iter().collect();

        assert_eq!(Some(&1), kvs.get(&ident("a".into())));

        assert_eq!(expected, kvs.idents);
    }
    #[test]
    #[should_panic]
    fn multiple_ident_insertions_panics() {
        let mut kvs = OperandMap::new();

        kvs.insert(ident("a".into()), 1);
        kvs.insert(ident("a".into()), 2);
    }

    #[test]
    fn const_insertion_works() {
        let mut kvs = OperandMap::new();

        kvs.insert(Operand::Const(Const::IntConst(5)), 1);
        kvs.insert(Operand::Const(Const::DoubleConst(3.2)), 2);

        let expected = vec![
            (Operand::Const(Const::IntConst(5)), 1),
            (Operand::Const(Const::DoubleConst(3.2)), 2),
        ];

        assert_eq!(Some(&1), kvs.get(&Operand::Const(Const::IntConst(5))));

        assert_eq!(expected, kvs.consts);
    }
    #[test]
    #[should_panic]
    fn multiple_const_insertions_panics() {
        let mut kvs = OperandMap::new();

        kvs.insert(Operand::Const(Const::IntConst(5)), 1);
        kvs.insert(Operand::Const(Const::IntConst(5)), 2);
    }

    #[test]
    fn comparison_works() {
        let mut kvs1 = OperandMap::new();
        kvs1.insert(Operand::Const(Const::IntConst(5)), 1);
        kvs1.insert(ident("a".into()), 1);

        // Same values
        let mut kvs2 = OperandMap::new();
        kvs2.insert(Operand::Const(Const::IntConst(5)), 1);
        kvs2.insert(ident("a".into()), 1);

        // Difference in contained ident value
        let mut kvs3 = OperandMap::new();
        kvs3.insert(Operand::Const(Const::IntConst(5)), 1);
        kvs3.insert(ident("a".into()), 2);

        // Difference in contained const value
        let mut kvs4 = OperandMap::new();
        kvs4.insert(Operand::Const(Const::IntConst(5)), 2);
        kvs4.insert(ident("a".into()), 1);

        // More constants
        let mut kvs5 = OperandMap::new();
        kvs5.insert(Operand::Const(Const::IntConst(5)), 2);
        kvs5.insert(ident("a".into()), 1);
        kvs5.insert(Operand::Const(Const::IntConst(6)), 2);

        // Fewer constants
        let mut kvs5 = OperandMap::new();
        kvs5.insert(ident("a".into()), 1);

        assert!(kvs2 == kvs1);
        assert!(kvs3 != kvs1);
        assert!(kvs4 != kvs1);
        assert!(kvs5 != kvs1);
    }
}
