use crate::graph::Graph;
use crate::graph_node::{Kind, Node};
use crate::id_tracker::IdTracker;
use crate::operand::{ident, Operand};

/// Traverses the graph starting at `Operand`, and returns the first operand
/// which is not a Nop.
pub fn resolve_operand(start: &Operand, graph: &Graph) -> Operand {
    match start {
        Operand::Ident(ident) => match &graph.nodes[ident] {
            Node {
                kind: Kind::Nop(ref inner),
                ..
            } => resolve_operand(inner, graph),
            _ => start.clone(),
        },
        other => other.clone(),
    }
}

pub fn invert_operand(
    operand: Operand,
    id_tracker: &mut IdTracker,
    motivation: &str,
) -> (Operand, Node) {
    // Create a node inverting the condition
    // and return that
    let new_id = id_tracker.next_linking_operand(&operand, motivation);
    let new_node = Node {
        name: new_id.into(),
        kind: Kind::Not(operand),
    };
    (ident(new_id.into()), new_node)
}

/// Generic tests for the lowering module
#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashMap;

    use crate::graph::Graph;
    use crate::graph_node::{node, Kind};
    use crate::operand::ident;

    #[test]
    fn nop_resolution_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("a", Kind::Nop(ident("b".into()))),
                node("b", Kind::Nop(ident("c".into()))),
                node("c", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let result = resolve_operand(&ident("a".into()), &graph);

        assert_eq!(result, ident("c".into()));
    }
}
