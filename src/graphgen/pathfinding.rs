use std::collections::HashMap;

use llvm_ir::Name;
use petgraph::graph::NodeIndex;

use crate::graph_algo::has_paths_not_via;

use super::block_graph::{is_reachable, BlockGraph, Dom};

/// A step on a path leading to a specific node.
#[derive(Clone, PartialEq, Eq, Hash)]
pub enum PathStep<T> {
    /// No matter what, we will end up in Name
    Guaranteed(T),
    /// We need to make a choice in `from` to end up drectly in `to`
    // NOTE: The from information is kind of redundant, it could be infered from
    // the previous step of a path. But keeping it here makes it easier to use the
    // structure later
    Selection { from: T, to: T },
}

impl<T> PathStep<T> {
    pub fn map<U>(self, f: impl Fn(T) -> U) -> PathStep<U> {
        match self {
            PathStep::Guaranteed(i) => PathStep::Guaranteed(f(i)),
            PathStep::Selection { from, to } => PathStep::Selection {
                from: f(from),
                to: f(to),
            },
        }
    }
}

impl<T: std::fmt::Debug> std::fmt::Debug for PathStep<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PathStep::Guaranteed(inner) => {
                write!(f, "G({:?})", inner)
            }
            PathStep::Selection { from, to } => {
                write!(f, "S({:?}->{:?})", from, to)
            }
        }
    }
}

// #[allow(non_upper_case_globals)]
// pub const find_significant_paths: &dyn Fn(
//     NodeIndex,
//     NodeIndex,
//     NodeIndex,
//     &BlockGraph,
//     &Dom,
// ) -> Vec<Vec<PathStep<Name>>> = &smart_find_significant_paths;

/// Computes the paths that lead from start to `final_goal` via `target`
///
/// `target` must be a direct parent of `final_goal`, panics otherwise
///
/// A select node is always added for the final step between `target` and `final_goal`, even if it could be guaranteed, for example in a normal branch
pub fn smart_find_significant_paths(
    start: NodeIndex,
    target: NodeIndex,
    final_goal: NodeIndex,
    graph: &BlockGraph,
    doms: &Dom,
) -> Vec<Vec<PathStep<Name>>> {
    fn inner(
        start: NodeIndex,
        target: NodeIndex,
        final_goal: NodeIndex,
        graph: &BlockGraph,
        doms: &Dom,
        cache: &mut HashMap<NodeIndex, Vec<Vec<PathStep<NodeIndex>>>>,
    ) -> Vec<Vec<PathStep<NodeIndex>>> {
        // Check if we already went down this route
        if let Some(cached) = cache.get(&target) {
            return cached.clone();
        }

        // Destination has been reached
        if target == start {
            return vec![vec![PathStep::Guaranteed(start)]];
        } else {
            let dom = if let Some(dom) = doms.immediate_dominator(target) {
                dom
            } else {
                let target_name = graph.name_from_index(&target);

                std::fs::write("__block_graph.gv", graph.dot_graph()).unwrap();
                panic!(
                    "No immediate dominator of {:?}. Dumped __block_raph.gv for debugging",
                    target_name
                )
            };

            let paths_not_via = has_paths_not_via(dom, final_goal, target, graph.inner.clone());
            if paths_not_via {
                let mut result = vec![];
                for parent in graph.rev.neighbors(target) {
                    let rest = inner(start, parent, final_goal, graph, doms, cache);
                    // Are there are any edges going out of parent which lead to the final
                    // goal but which do not go through the current node.
                    let is_guaranteed = !graph
                        .inner
                        .neighbors(parent)
                        .any(|node| node != target && is_reachable(node, final_goal, graph));

                    let step = if is_guaranteed {
                        PathStep::Guaranteed(target)
                    } else {
                        PathStep::Selection {
                            from: parent,
                            to: target,
                        }
                    };
                    for mut path in rest {
                        path.push(step.clone());
                        result.push(path);
                    }
                }
                result
            } else {
                let step = PathStep::Guaranteed(target);

                let mut result = vec![];
                let rest = inner(start, dom, final_goal, graph, doms, cache);
                for mut path in rest {
                    path.push(step.clone());
                    result.push(path)
                }
                result
            }
        }
    }

    assert!(
        graph
            .inner
            .neighbors(target)
            .any(|child| child == final_goal),
        "Calling find_significant_paths with final_goal which is not a child of target"
    );

    // Check if there are paths out of `target` that reach the `final_goal` but which
    // are not direct paths. If so, a selection still has to be made in order to reach
    // the goal.
    //
    // This is nessecary in the following graph:
    //
    // 1
    // |\
    // | 3
    // |/|
    // 2 |
    // |/
    // 4
    //
    // when finding significant paths via 3.

    let mut final_step = PathStep::Guaranteed(final_goal);
    for child in graph.inner.neighbors(target) {
        if child != final_goal && is_reachable(child, final_goal, &graph) {
            final_step = PathStep::Selection {
                from: target,
                to: final_goal,
            };
            break;
        }
    }

    // Call the actual function and return a path with block names instead
    // of node indices
    inner(start, target, final_goal, graph, doms, &mut HashMap::new())
        .into_iter()
        .map(|mut path| {
            path.push(final_step.clone());
            path.into_iter()
                .map(|node| node.map(|index| graph.name_from_index(&index).clone()))
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

/// A version of [find_significant_paths] which naively traverses
pub fn naive_find_significant_paths(
    start: NodeIndex,
    target: NodeIndex,
    final_goal: NodeIndex,
    graph: &BlockGraph,
    doms: &Dom,
) -> Vec<Vec<PathStep<Name>>> {
    fn inner(
        start: NodeIndex,
        target: NodeIndex,
        final_goal: NodeIndex,
        graph: &BlockGraph,
        doms: &Dom,
        cache: &mut HashMap<NodeIndex, Vec<Vec<PathStep<NodeIndex>>>>,
    ) -> Vec<Vec<PathStep<NodeIndex>>> {
        // Check if we already went down this route
        if let Some(cached) = cache.get(&target) {
            return cached.clone();
        }

        // Destination has been reached
        if target == start {
            vec![vec![PathStep::Guaranteed(start)]]
        } else {
            let mut result = vec![];
            for parent in graph.rev.neighbors(target) {
                let rest = inner(start, parent, final_goal, graph, doms, cache);

                let step = PathStep::Selection {
                    from: parent,
                    to: target,
                };
                for mut path in rest {
                    path.push(step.clone());
                    result.push(path);
                }
            }
            result
        }
    }

    assert!(
        graph
            .inner
            .neighbors(target)
            .any(|child| child == final_goal),
        "Calling find_significant_paths with final_goal which is not a child of target"
    );

    let mut final_step = PathStep::Guaranteed(final_goal);
    for child in graph.inner.neighbors(target) {
        if child != final_goal && is_reachable(child, final_goal, &graph) {
            final_step = PathStep::Selection {
                from: target,
                to: final_goal,
            };
            break;
        }
    }

    // Call the actual function and return a path with block names instead
    // of node indices
    inner(start, target, final_goal, graph, doms, &mut HashMap::new())
        .into_iter()
        .map(|mut path| {
            path.push(final_step.clone());
            path.into_iter()
                .map(|node| node.map(|index| graph.name_from_index(&index).clone()))
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

#[cfg(test)]
mod fsp_tests {
    use super::*;
    use llvm_ir::{terminator, types::Types, BasicBlock, Terminator};

    use crate::graphgen::basic_block_dominance;
    use crate::testutil::f32var;

    // use pretty_assertions::assert_eq;

    #[test]
    fn significant_paths_are_found_correctly() {
        /*
            1
           / \
          2   \
         / \   |
        3   4  5
         \ /   |
          6   /
           \ /
            7
        */
        let types = Types::blank_for_testing();

        let basic_blocks = vec![
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 2.into(),
                    false_dest: 5.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(11, &types),
                    true_dest: 3.into(),
                    false_dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 6.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 4.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 6.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 5.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 7.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 6.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 7.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 7.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];

        let expected_6: Vec<Vec<PathStep<Name>>> = vec![vec![
            PathStep::Guaranteed(1.into()),
            PathStep::Selection {
                from: 1.into(),
                to: 2.into(),
            },
            PathStep::Guaranteed(6.into()),
            PathStep::Guaranteed(7.into()),
        ]];
        let expected_5: Vec<Vec<PathStep<Name>>> = vec![vec![
            PathStep::Guaranteed(1.into()),
            PathStep::Selection {
                from: 1.into(),
                to: 5.into(),
            },
            PathStep::Guaranteed(7.into()),
        ]];

        let graph = BlockGraph::new(&basic_blocks);
        let doms = basic_block_dominance(&graph, &basic_blocks[0].name);

        let result_6 = smart_find_significant_paths(
            graph.index_from_name(&1.into()),
            graph.index_from_name(&6.into()),
            graph.index_from_name(&7.into()),
            &graph,
            &doms,
        );
        let result_5 = smart_find_significant_paths(
            graph.index_from_name(&1.into()),
            graph.index_from_name(&5.into()),
            graph.index_from_name(&7.into()),
            &graph,
            &doms,
        );

        assert_eq!(result_6, expected_6);
        assert_eq!(result_5, expected_5);
    }
    #[test]
    fn simple_significant_paths_are_found() {
        /*
          0
         / \
        1   2
         \ /
          3
        */
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 1.into(),
                    false_dest: 2.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];

        let expected_2 = vec![vec![
            PathStep::Guaranteed(0.into()),
            PathStep::Selection {
                from: 0.into(),
                to: 2.into(),
            },
            PathStep::Guaranteed(3.into()),
        ]];

        let expected_1 = vec![vec![
            PathStep::Guaranteed(0.into()),
            PathStep::Selection {
                from: 0.into(),
                to: 1.into(),
            },
            PathStep::Guaranteed(3.into()),
        ]];

        let graph = BlockGraph::new(&basic_blocks);
        let doms = basic_block_dominance(&graph, &basic_blocks[0].name);

        let result_2 = smart_find_significant_paths(
            graph.index_from_name(&0.into()),
            graph.index_from_name(&2.into()),
            graph.index_from_name(&3.into()),
            &graph,
            &doms,
        );
        let result_1 = smart_find_significant_paths(
            graph.index_from_name(&0.into()),
            graph.index_from_name(&1.into()),
            graph.index_from_name(&3.into()),
            &graph,
            &doms,
        );

        assert_eq!(result_1, expected_1);
        assert_eq!(result_2, expected_2);
    }

    #[test]
    fn branching_onto_other_path_produces_correct_paths() {
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 1.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    true_dest: 2.into(),
                    false_dest: 3.into(),
                    condition: f32var(10, &types),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    true_dest: 3.into(),
                    false_dest: 4.into(),
                    condition: f32var(11, &types),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 4.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];
        /*
        1
        |\
        | 2
        |/|
        3 |
        |/
        4
        */

        let expected_3 = vec![
            vec![
                PathStep::Guaranteed(1.into()),
                PathStep::Selection {
                    from: 1.into(),
                    to: 2.into(),
                },
                PathStep::Selection {
                    from: 2.into(),
                    to: 3.into(),
                },
                PathStep::Guaranteed(4.into()),
            ],
            vec![
                PathStep::Guaranteed(1.into()),
                PathStep::Selection {
                    from: 1.into(),
                    to: 3.into(),
                },
                PathStep::Guaranteed(4.into()),
            ],
        ];

        let expected_2 = vec![vec![
            PathStep::Guaranteed(1.into()),
            PathStep::Selection {
                from: 1.into(),
                to: 2.into(),
            },
            PathStep::Selection {
                from: 2.into(),
                to: 4.into(),
            },
        ]];

        let graph = BlockGraph::new(&basic_blocks);
        let doms = basic_block_dominance(&graph, &basic_blocks[0].name);

        let result_2 = smart_find_significant_paths(
            graph.index_from_name(&1.into()),
            graph.index_from_name(&2.into()),
            graph.index_from_name(&4.into()),
            &graph,
            &doms,
        );
        let result_3 = smart_find_significant_paths(
            graph.index_from_name(&1.into()),
            graph.index_from_name(&3.into()),
            graph.index_from_name(&4.into()),
            &graph,
            &doms,
        );

        assert_eq!(result_2, expected_2);
        assert_eq!(result_3, expected_3);
    }

    #[test]
    fn fsp_does_not_find_pointless_selects() {
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    true_dest: 1.into(),
                    false_dest: 2.into(),
                    condition: f32var(10, &types),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    true_dest: 3.into(),
                    false_dest: 5.into(),
                    condition: f32var(10, &types),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    true_dest: 3.into(),
                    false_dest: 4.into(),
                    condition: f32var(11, &types),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 4.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
            BasicBlock {
                name: 5.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];
        /*
              0
             / \
            1   2
           /|   |\
          |  \ /  |
          |   3  /
          :   | /
          :   4
          5
        */

        let expected_3 = vec![
            vec![
                PathStep::Guaranteed(0.into()),
                PathStep::Selection {
                    from: 0.into(),
                    to: 2.into(),
                },
                PathStep::Selection {
                    from: 2.into(),
                    to: 3.into(),
                },
                PathStep::Guaranteed(4.into()),
            ],
            vec![
                PathStep::Guaranteed(0.into()),
                PathStep::Selection {
                    from: 0.into(),
                    to: 1.into(),
                },
                PathStep::Guaranteed(3.into()),
                PathStep::Guaranteed(4.into()),
            ],
        ];
        let expected_2 = vec![vec![
            PathStep::Guaranteed(0.into()),
            PathStep::Selection {
                from: 0.into(),
                to: 2.into(),
            },
            PathStep::Selection {
                from: 2.into(),
                to: 4.into(),
            },
        ]];

        let graph = BlockGraph::new(&basic_blocks);
        let doms = basic_block_dominance(&graph, &basic_blocks[0].name);

        let result_3 = smart_find_significant_paths(
            graph.index_from_name(&0.into()),
            graph.index_from_name(&3.into()),
            graph.index_from_name(&4.into()),
            &graph,
            &doms,
        );
        let result_2 = smart_find_significant_paths(
            graph.index_from_name(&0.into()),
            graph.index_from_name(&2.into()),
            graph.index_from_name(&4.into()),
            &graph,
            &doms,
        );

        assert_eq!(result_2, expected_2);
        assert_eq!(result_3, expected_3);
    }

    #[test]
    fn interp_if_structure_significant_paths_are_found() {
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 2.into(),
                    false_dest: 1.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(11, &types),
                    true_dest: 2.into(),
                    false_dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 4.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];
        /*
        0   (10) [true -> 2]
        |\
        1 | (11) [true -> 2]
        |\|
        | 2 () [()]
        3 | () [()]
        |/
        4   () [()]
        */

        let expected_3 = vec![vec![
            PathStep::Guaranteed(0.into()),
            PathStep::Selection {
                from: 0.into(),
                to: 1.into(),
            },
            PathStep::Selection {
                to: 3.into(),
                from: 1.into(),
            },
            PathStep::Guaranteed(4.into()),
        ]];
        let expected_2 = vec![
            vec![
                PathStep::Guaranteed(0.into()),
                PathStep::Selection {
                    from: 0.into(),
                    to: 1.into(),
                },
                PathStep::Selection {
                    from: 1.into(),
                    to: 2.into(),
                },
                PathStep::Guaranteed(4.into()),
            ],
            vec![
                PathStep::Guaranteed(0.into()),
                PathStep::Selection {
                    from: 0.into(),
                    to: 2.into(),
                },
                PathStep::Guaranteed(4.into()),
            ],
        ];

        let graph = BlockGraph::new(&basic_blocks);
        let doms = basic_block_dominance(&graph, &basic_blocks[0].name);

        let result_3 = smart_find_significant_paths(
            graph.index_from_name(&0.into()),
            graph.index_from_name(&3.into()),
            graph.index_from_name(&4.into()),
            &graph,
            &doms,
        );
        let result_2 = smart_find_significant_paths(
            graph.index_from_name(&0.into()),
            graph.index_from_name(&2.into()),
            graph.index_from_name(&4.into()),
            &graph,
            &doms,
        );

        assert_eq!(result_2, expected_2);
        assert_eq!(result_3, expected_3);
    }
}
