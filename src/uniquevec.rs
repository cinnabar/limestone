use serde_derive::{Deserialize, Serialize};

/// A Vec<T> with a custom PartialEq operation which checks element equality but not
/// location. This is an O(n^2) process, so usage should be avoided if PartialEq will
/// be a common operation.
///
/// The point of the struct is as a `Set` where `Eq`, `Hash` or `Ord` can not be implemented.
///
/// If duplicate elements are present, the same amount of those
/// elements is required
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UniqueVec<T>
where
    T: PartialEq,
{
    pub inner: Vec<T>,
}

impl<T> UniqueVec<T>
where
    T: PartialEq,
{
    pub fn new() -> Self {
        Self { inner: Vec::new() }
    }
}

impl<T> Into<UniqueVec<T>> for Vec<T>
where
    T: PartialEq,
{
    fn into(self) -> UniqueVec<T> {
        UniqueVec { inner: self }
    }
}

impl<T> std::ops::Deref for UniqueVec<T>
where
    T: PartialEq,
{
    type Target = Vec<T>;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}
impl<T> std::ops::DerefMut for UniqueVec<T>
where
    T: PartialEq,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<T> PartialEq for UniqueVec<T>
where
    T: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        // Trivial check for length
        if self.inner.len() != other.inner.len() {
            return false;
        }

        // Keep track of which elements have found a match in the other array
        let mut match_map = vec![];
        match_map.resize_with(self.inner.len(), || false);

        for s in &self.inner {
            let mut found_match = false;
            for (oi, o) in other.inner.iter().enumerate() {
                if s == o && !match_map[oi] {
                    match_map[oi] = true;
                    found_match = true;
                    break;
                }
            }
            if !found_match {
                return false;
            }
        }

        // We went through each element in `self` and found none that had no match.
        // Since the arrays are the same length, that means every element in other
        // also has a match
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn non_duplicates_works() {
        let x = UniqueVec {
            inner: vec![1, 2, 3],
        };
        let y = UniqueVec {
            inner: vec![3, 2, 1],
        };

        assert_eq!(x, y);
    }

    #[test]
    fn empty_comparison_works() {
        let x = UniqueVec { inner: vec![] };
        let y = UniqueVec { inner: vec![1] };

        assert_eq!(x, x);
        assert_ne!(x, y);
    }
}
