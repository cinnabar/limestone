#!/bin/bash

set -e

mkdir -p .coverage

cargo tarpaulin -o html --output-dir .coverage --target-dir target/tarpaulin \
    | if which rehl; then
        rehl -b '\d+.\d+% coverage'
    else
        cat -
    fi

    echo "Coverage report is in file://$(pwd)/.coverage/tarpaulin-report.html"
