use std::collections::HashMap;

use llvm_ir::module::GlobalVariable;
use llvm_ir::{constant::Float, types::FPType, Constant, Type};

use crate::operand::Identifier;

#[derive(Debug, Clone, PartialEq)]
pub enum Global {
    Str(String),
    DoubleArray(Vec<f64>),
}

/**
  Manages global variables associated with the llvm module in the global_vars
  field
*/
#[derive(Debug, Clone, PartialEq)]
pub struct GlobalTable {
    pub content: HashMap<Identifier, Global>,
}

impl GlobalTable {
    pub fn get_value(&self, name: &Identifier) -> Option<&Global> {
        self.content.get(name)
    }
}

/// Collects a string from the global variable under the assumption that it is
/// in fact a string.
fn collect_string(var: &GlobalVariable) -> Option<String> {
    // We still need to check if there is an initialiser here
    let elements = if let Some(Constant::Array { elements, .. }) = var.initializer.as_deref() {
        elements
    } else {
        return None;
    };

    // Type checks out, and a constant value is set. Start getting the actual values.
    // Since we checked the type we can stat unwrapping things which, at the type level may
    // be wrong

    let string = elements
        .iter()
        .map(|element| match element.as_ref() {
            Constant::Int { value, bits: 8 } => *value as u8,
            other => panic!("unexpected constant value: {:?}", other),
        })
        .collect::<Vec<_>>();

    // Since rust strings are not null terminated, we'll get rid of
    // the last byte
    Some(String::from_utf8_lossy(&string[0..string.len() - 1]).into())
}
/// Collects a double array from the global variable under the assumption that it is
/// in fact a double array.
fn collect_doubles(var: &GlobalVariable) -> Option<Vec<f64>> {
    // We still need to check if there is an initialiser here
    let elements = if let Some(Constant::Array { elements, .. }) = var.initializer.as_deref() {
        elements
    } else {
        return None;
    };

    // Type checks out, and a constant value is set. Start getting the actual values.
    // Since we checked the type we can stat unwrapping things which, at the type level may
    // be wrong

    let result = elements
        .iter()
        .map(|element| match element.as_ref() {
            Constant::Float(Float::Double(val)) => val,
            other => panic!("unexpected constant value: {:?}", other),
        })
        .cloned()
        .collect::<Vec<_>>();

    Some(result)
}

impl From<&Vec<GlobalVariable>> for GlobalTable {
    fn from(other: &Vec<GlobalVariable>) -> Self {
        let mut result = GlobalTable {
            content: HashMap::new(),
        };

        for var in other {
            // Check the pointer type, breaking out of the loop as soon as we detect
            // that it is not a string
            let pointee_type = if let Type::PointerType { pointee_type, .. } = &var.ty.as_ref() {
                pointee_type
            } else {
                continue;
            };

            let (_num_elements, element_type) = if let Type::ArrayType {
                element_type,
                num_elements,
            } = pointee_type.as_ref()
            {
                (num_elements, element_type)
            } else {
                continue;
            };

            let val = match element_type.as_ref() {
                Type::IntegerType { bits: 8 } => collect_string(var).map(Global::Str),
                Type::FPType(FPType::Double) => collect_doubles(var).map(Global::DoubleArray),
                _ => None,
            };

            if let Some(val) = val {
                result.content.insert(var.name.clone().into(), val);
            }
        }
        result
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use llvm_ir::{
        module::{DLLStorageClass, Linkage, ThreadLocalMode, UnnamedAddr, Visibility},
        types::Types,
    };
    use llvm_ir::{ConstantRef, Name};

    #[test]
    fn loading_strings_works() {
        let types = Types::blank_for_testing();
        let input = vec![
            GlobalVariable {
                name: Name::Name(Box::new(".str".to_string())),
                linkage: Linkage::Private,
                visibility: Visibility::Default,
                is_constant: true,
                ty: types.pointer_to(types.array_of(types.int(8), 6)),
                addr_space: 0,
                dll_storage_class: DLLStorageClass::Default,
                thread_local_mode: ThreadLocalMode::NotThreadLocal,
                unnamed_addr: Some(UnnamedAddr::Global),
                initializer: Some(ConstantRef::new(Constant::Array {
                    element_type: types.int(8),
                    elements: vec![
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 120,
                        }),
                        ConstantRef::new(Constant::Int { bits: 8, value: 95 }),
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 108,
                        }),
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 117,
                        }),
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 116,
                        }),
                        ConstantRef::new(Constant::Int { bits: 8, value: 0 }),
                    ],
                })),
                section: None,
                comdat: None,
                alignment: 1,
                debugloc: None,
            },
            GlobalVariable {
                name: Name::Name(Box::new(".str.1".to_string())),
                linkage: Linkage::Private,
                visibility: Visibility::Default,
                is_constant: true,
                ty: types.pointer_to(types.array_of(types.int(8), 6)),
                addr_space: 0,
                dll_storage_class: DLLStorageClass::Default,
                thread_local_mode: ThreadLocalMode::NotThreadLocal,
                unnamed_addr: Some(UnnamedAddr::Global),
                initializer: Some(ConstantRef::new(Constant::Array {
                    element_type: types.int(8),
                    elements: vec![
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 120,
                        }),
                        ConstantRef::new(Constant::Int { bits: 8, value: 95 }),
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 108,
                        }),
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 117,
                        }),
                        ConstantRef::new(Constant::Int {
                            bits: 8,
                            value: 117,
                        }),
                        ConstantRef::new(Constant::Int { bits: 8, value: 0 }),
                    ],
                })),
                section: None,
                comdat: None,
                alignment: 1,
                debugloc: None,
            },
        ];

        let expected = GlobalTable {
            content: vec![
                (".str".into(), Global::Str("x_lut".to_string())),
                (".str.1".into(), Global::Str("x_luu".to_string())),
            ]
            .into_iter()
            .collect(),
        };

        assert_eq!(expected, GlobalTable::from(&input));
    }

    #[test]
    fn loading_doubles_works() {
        let types = Types::blank_for_testing();
        let input = vec![GlobalVariable {
            name: Name::Name(Box::new(".lut".to_string())),
            linkage: Linkage::Private,
            visibility: Visibility::Default,
            is_constant: true,
            ty: types.pointer_to(types.array_of(types.fp(FPType::Double), 6)),
            addr_space: 0,
            dll_storage_class: DLLStorageClass::Default,
            thread_local_mode: ThreadLocalMode::NotThreadLocal,
            unnamed_addr: Some(UnnamedAddr::Global),
            initializer: Some(ConstantRef::new(Constant::Array {
                element_type: types.fp(FPType::Double),
                elements: vec![
                    ConstantRef::new(Constant::Float(Float::Double(5.))),
                    ConstantRef::new(Constant::Float(Float::Double(8.))),
                    ConstantRef::new(Constant::Float(Float::Double(3.))),
                ],
            })),
            section: None,
            comdat: None,
            alignment: 1,
            debugloc: None,
        }];

        let expected = GlobalTable {
            content: vec![(".lut".into(), Global::DoubleArray(vec![5., 8., 3.]))]
                .into_iter()
                .collect(),
        };

        assert_eq!(expected, GlobalTable::from(&input));
    }
}
