pub mod input_bounds;
pub mod interp;
pub mod lut;
pub mod structs;
mod util;

use crate::globals::GlobalTable;
use crate::graph::{Graph, GraphDiff};
use crate::graph_node::Kind;

pub use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Error)]
pub enum Error {
    #[error("Failed to lower Load instruction. Individual lowering errors:\n\t{0}\n\t{1}")]
    LoadLoweringError(lut::Error, structs::Error),
    #[error("Failed to lower input bound. Did you forget declare_bounds?")]
    InputBoundError(#[source] input_bounds::Error),
    #[error("Failed to lower struct store")]
    StructStoreError(#[from] structs::Error),
    #[error("failed to lower interpolation function")]
    InterpLoweringError(#[from] interp::Error),
}

pub type Result<T> = std::result::Result<T, Error>;

/// Attempts every possible lowering of the load instruction. Returns the result
/// of the first one succeeding, and an error oterwise
pub fn lower_loads(graph: Graph, globals: &GlobalTable) -> Result<Graph> {
    let mut new_graph = Graph::empty();
    for (name, node) in &graph.nodes {
        if let Kind::Load(ref operand) = node.kind {
            let lut = lut::try_lower_lut((name, operand), &graph, globals);
            let structs = structs::lower_struct_load((name, operand), &graph);

            let new_nodes = match (lut, structs) {
                (Ok(nodes), _) => nodes,
                (_, Ok(node)) => vec![node],
                (Err(lut), Err(structs)) => Err(Error::LoadLoweringError(lut, structs))?,
            };
            for node in new_nodes {
                new_graph.add_node(node);
            }
        } else {
            new_graph.add_node(node.clone());
        }
    }
    Ok(new_graph)
}

pub fn do_lowering(
    mut graph: Graph,
    steps: &[&dyn Fn(Graph) -> Result<Graph>],
) -> Result<(Graph, Vec<Vec<GraphDiff>>)> {
    let mut diffs = vec![];
    for step in steps {
        let pre = graph.clone();
        graph = step(graph)?;
        diffs.push(graph.diff(&pre))
    }
    Ok((graph, diffs))
}

/// Tests of specific instruction lowerings

#[cfg(test)]
mod load_lowering_tests {
    use super::*;

    use std::collections::HashMap;

    use crate::consts::Const;
    use crate::globals::Global;
    use crate::graph::Graph;
    use crate::graph_node::{node, Kind};
    use crate::operand::{ident, Operand};

    #[test]
    fn lut_lowering_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "ptr",
                    Kind::GetElementPtr {
                        address: Operand::Const(Const::ArrayReference("lut_vals".into())),
                        indices: vec![Operand::Const(Const::IntConst(0)), ident("idx".into())],
                    },
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph::from_tuples(
            vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "ptr",
                    Kind::GetElementPtr {
                        address: Operand::Const(Const::ArrayReference("lut_vals".into())),
                        indices: vec![Operand::Const(Const::IntConst(0)), ident("idx".into())],
                    },
                ),
                node(
                    "result",
                    Kind::Lookup {
                        name: "lut_vals".into(),
                        data: vec![0., 1., 3., 5.],
                        idx: ident("idx".into()),
                    },
                ),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        );

        let strings = GlobalTable {
            content: vec![("lut_vals".into(), Global::DoubleArray(vec![0., 1., 3., 5.]))]
                .into_iter()
                .collect(),
        };

        assert_eq!(Ok(expected), lower_loads(graph, &strings));
    }
}
