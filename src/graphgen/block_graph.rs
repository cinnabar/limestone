use std::collections::{HashMap, HashSet};

use llvm_ir::{BasicBlock, Name, Terminator};
use petgraph::{
    algo::dominators::Dominators,
    dot::{Config, Dot},
    graph::{Graph as PetGraph, NodeIndex},
};

/// Graph of basic blocks, used for other algorithms
#[derive(Debug)]
pub struct BlockGraph {
    pub inner: PetGraph<(), (), petgraph::Directed, u32>,
    pub rev: PetGraph<(), (), petgraph::Directed, u32>,
    /// Defines the mapping between petgraph nodes and block names
    pub index_map: HashMap<Name, NodeIndex>,
    pub name_map: HashMap<NodeIndex, Name>,
    pub name_block_map: HashMap<Name, BasicBlock>,

    pub root: NodeIndex,
}

impl BlockGraph {
    pub fn new(blocks: &[BasicBlock]) -> Self {
        Self::new_without_edges(blocks, HashSet::new())
    }

    pub fn new_without_edges(blocks: &[BasicBlock], to_trim: HashSet<(Name, Name)>) -> Self {
        // Mapping of name to (usize, PetGraph index)
        let mut index_map = HashMap::new();
        let mut name_map = HashMap::new();
        let mut graph = PetGraph::new();
        // Build the mapping between petgraph node id and basic block name
        for block in blocks {
            let index = graph.add_node(());
            index_map.insert(block.name.clone(), index);
            name_map.insert(index, block.name.clone());
        }

        // Add the edges
        for block in blocks {
            let index = index_map[&block.name];

            let mut add_edge = |to: &Name| {
                if !to_trim.contains(&(block.name.clone(), to.clone())) {
                    graph.add_edge(index, index_map[&to], ());
                }
            };

            match &block.term {
                Terminator::Br(br) => {
                    add_edge(&br.dest);
                }
                Terminator::CondBr(cond) => {
                    add_edge(&cond.true_dest);
                    add_edge(&cond.false_dest);
                }
                Terminator::Switch(switch) => {
                    for (_, dest) in &switch.dests {
                        add_edge(dest);
                    }
                    add_edge(&switch.default_dest)
                }
                Terminator::Unreachable(_) => {}
                Terminator::Ret(_) => {}
                other => panic!("Unhandled terminator: {:?}", other),
            }
        }

        let name_block_map = blocks
            .iter()
            .map(|block| (block.name.clone(), block.clone()))
            .collect::<HashMap<_, _>>();

        let root = index_map[&blocks[0].name];

        let mut rev = graph.clone();
        rev.reverse();
        Self {
            inner: graph,
            rev,
            name_map,
            index_map,
            name_block_map,
            root,
        }
    }

    pub fn index_from_name(&self, name: &Name) -> NodeIndex {
        self.index_map[name]
    }
    pub fn name_from_index(&self, index: &NodeIndex) -> &Name {
        &self.name_map[index]
    }
    pub fn block_from_index(&self, index: &NodeIndex) -> &BasicBlock {
        &self.name_block_map[&self.name_map[index]]
    }

    pub fn dot_graph(&self) -> String {
        let maped = self.inner.map(
            |node_idx, _| format!("{:?}", self.name_map[&node_idx]),
            |_, _| (),
        );
        format!("{:?}", Dot::with_config(&maped, &[Config::EdgeNoLabel]))
    }
}

/**
  Wrapper around the petgraph Dominators struct which requires identifiers
  to be copy
*/
pub type Dom = Dominators<NodeIndex>;

pub fn is_reachable(from: NodeIndex, to: NodeIndex, graph: &BlockGraph) -> bool {
    petgraph::algo::has_path_connecting(&graph.inner, from, to, None)
}
