use std::collections::HashMap;

use super::pathfinding::PathStep;

#[derive(PartialEq, Debug)]
pub struct PartialPath<S, T> {
    pub matched: Vec<PathStep<S>>,
    pub unmatched: Vec<PathStep<S>>,
    pub payload: T,
}

pub struct PathCollection<S, T>
where
    Vec<PathStep<S>>: Eq + std::hash::Hash,
    PathStep<S>: Clone,
    T: Clone,
{
    inner: HashMap<Vec<PathStep<S>>, T>,
}

impl<S, T> PathCollection<S, T>
where
    Vec<PathStep<S>>: Eq + std::hash::Hash,
    PathStep<S>: Clone,
    T: Clone,
{
    pub fn new() -> Self {
        Self {
            inner: HashMap::new(),
        }
    }

    pub fn push(&mut self, key: Vec<PathStep<S>>, value: T) {
        if let Some(_) = self.inner.insert(key, value) {
            panic!("Overwrote previous value")
        }
    }

    /// Tries to the longest stored path which is a prefix of the input path.
    ///
    /// The rest of the path, along with the T corresponding to the prefix is returned.
    pub fn longest_prefix(&self, mut needle: Vec<PathStep<S>>) -> Option<PartialPath<S, T>> {
        // Check if `input` is in inner, if not, pop the last element and move on.
        if needle.is_empty() {
            None
        } else if let Some(payload) = self.inner.get(&needle) {
            Some(PartialPath {
                matched: needle,
                unmatched: vec![],
                payload: payload.clone(),
            })
        } else {
            // This was not a match. Pop the element, recurse and add the element
            // to the remainder if a result is returned deeper in the call stack

            // NOTE: Safe unwrap because we know needle is not empty from the first condition
            let here = needle.pop().unwrap();

            self.longest_prefix(needle).map(|mut result| {
                result.unmatched.push(here);
                result
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn full_match_is_found() {
        let mut uut = PathCollection::new();

        let key = vec![
            PathStep::Guaranteed(0),
            PathStep::Selection { from: 0, to: 1 },
        ];

        uut.push(key.clone(), ());

        let expected = PartialPath {
            matched: vec![
                PathStep::Guaranteed(0),
                PathStep::Selection { from: 0, to: 1 },
            ],
            unmatched: vec![],
            payload: (),
        };
        assert_eq!(uut.longest_prefix(key), Some(expected));
    }

    #[test]
    fn partial_match_is_found() {
        let mut uut = PathCollection::new();

        let key = vec![PathStep::Guaranteed(0)];

        uut.push(key.clone(), ());

        let needle = vec![
            PathStep::Guaranteed(0),
            PathStep::Guaranteed(1),
            PathStep::Guaranteed(2),
        ];

        let expected = PartialPath {
            matched: vec![PathStep::Guaranteed(0)],
            unmatched: vec![PathStep::Guaranteed(1), PathStep::Guaranteed(2)],
            payload: (),
        };

        assert_eq!(uut.longest_prefix(needle), Some(expected));
    }

    #[test]
    fn no_match_returns_none() {
        let mut uut = PathCollection::new();

        let key = vec![
            PathStep::Guaranteed(0),
            PathStep::Selection { from: 0, to: 1 },
        ];

        uut.push(key.clone(), ());

        let needle = vec![
            PathStep::Guaranteed(0),
            PathStep::Selection { from: 1, to: 2 },
        ];

        assert_eq!(uut.longest_prefix(needle), None);
    }
}
