extern "C" {
    // Math function
    int limestone_trunc(double);

    double limestone_max(double x, double y);
    double limestone_min(double x, double y);

    double limestone_sqrt(double);
    double limestone_abs(double);

    // Marker functions

    // Creates a named lookup table
    double* limestone_declare_lut(const double* data);

    // Specifies the bounds on the variable `input` as [min, max]
    void declare_bounds(double input, double min, double max);


    double interp1equid(const double*, const double*, double);
    bool interp1equid_valid(const double*, const double*, double);

    double interp2equid(const double*, const double*, const double*, double, double);
    bool interp2equid_valid(const double*, const double*, const double*, double, double);

    // Does not perform any computations on the passed value. However, the
    // effects of this function is unknown to llvm at compile time, and
    // therefore, it can prevent llvm optimising things we can not handle
    void __invisible_nop(double);

    double opaque_barrier(double input);
}

