// opt-opts -mem2reg

// Using extern to prevent name mangling
extern "C" {
    float test(float x, float y) {
        float result;
        if (x > y) {
            result = x;
        }
        else {
            result = y;
        }
        return result;
    }
}
