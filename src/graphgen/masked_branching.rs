use std::collections::HashMap;

use petgraph::{graph::NodeIndex, visit::Topo};

use crate::{
    consts::Const,
    debuginfo::SpecificInfo,
    error::Result,
    graph_node::{Kind, Node},
    id_tracker::IdTracker,
    operand::{Identifier, Operand},
};

use super::BlockGraph;

pub struct MaskTracker {
    pub edge_masks: HashMap<(NodeIndex, NodeIndex), Operand>,
    pub entry_masks: HashMap<NodeIndex, Operand>,
}

impl MaskTracker {
    pub fn new() -> Self {
        Self {
            edge_masks: HashMap::new(),
            entry_masks: HashMap::new(),
        }
    }

    /// Adds the edge and entry masks for the specified block
    fn add_masks(
        &mut self,
        block_idx: NodeIndex,
        block_graph: &BlockGraph,
        id_tracker: &mut IdTracker,
    ) -> Result<Vec<Node>> {
        let node_ident: Identifier = block_graph.name_from_index(&block_idx).into();

        let mut new_nodes = vec![];

        let entry_edges = block_graph.rev.neighbors(block_idx).collect::<Vec<_>>();

        let entry_mask = match entry_edges.len() {
            0 => Operand::Const(Const::BoolConst(true)),
            1 => self.edge_masks[&(entry_edges[0], block_idx)].clone(),
            _ => {
                // Generate new nodes which are the disjunction on the incomming edges
                let mut prev_operand = self.edge_masks[&(entry_edges[0], block_idx)].clone();

                for prev in &entry_edges[1..] {
                    let new_id =
                        id_tracker.next_specific(SpecificInfo::EntryMask(node_ident.clone()));
                    let edge_mask = self.edge_masks[&(*prev, block_idx)].clone();
                    let new_node = Node::new(new_id, Kind::Or(prev_operand, edge_mask));
                    new_nodes.push(new_node);
                    prev_operand = Operand::Ident(new_id.into())
                }

                prev_operand
            }
        };

        self.entry_masks.insert(block_idx, entry_mask.clone());

        // Look up the block to get the terminator info
        let block = block_graph.block_from_index(&block_idx);
        // Add edge masks

        match &block.term {
            // The edge mask will be the same as the node mask
            llvm_ir::Terminator::Br(br) => {
                let target = block_graph.index_from_name(&br.dest);
                self.edge_masks.insert((block_idx, target), entry_mask);
            }
            llvm_ir::Terminator::CondBr(br) => {
                let true_operand = {
                    let new_id = id_tracker.next_specific(SpecificInfo::EdgeMask(
                        node_ident.clone(),
                        br.true_dest.clone().into(),
                    ));

                    Node::new(
                        new_id,
                        Kind::And(entry_mask.clone(), Operand::from_llvm(&br.condition)?),
                    )
                };

                // Add an inversion of the condition
                let inverted_condition = {
                    let new_id = id_tracker.next_specific(SpecificInfo::EdgeMask(
                        node_ident.clone(),
                        br.false_dest.clone().into(),
                    ));

                    Node::new(new_id, Kind::Not(Operand::from_llvm(&br.condition)?))
                };

                let false_operand = {
                    let new_id = id_tracker.next_specific(SpecificInfo::EdgeMask(
                        node_ident.clone(),
                        br.false_dest.clone().into(),
                    ));

                    Node::new(
                        new_id,
                        Kind::And(entry_mask, Operand::Ident(inverted_condition.name.clone())),
                    )
                };

                new_nodes.push(true_operand.clone());
                new_nodes.push(inverted_condition);
                new_nodes.push(false_operand.clone());

                let true_target = block_graph.index_from_name(&br.true_dest);
                let false_target = block_graph.index_from_name(&br.false_dest);

                self.edge_masks
                    .insert((block_idx, true_target), Operand::Ident(true_operand.name));
                self.edge_masks.insert(
                    (block_idx, false_target),
                    Operand::Ident(false_operand.name),
                );
            }
            // This creates no edge
            llvm_ir::Terminator::Ret(_) => {}
            llvm_ir::Terminator::Switch(_) => {
                todo!("Support switch statements in masked branching")
            }
            llvm_ir::Terminator::IndirectBr(_) => {
                unimplemented! {}
            }
            llvm_ir::Terminator::Invoke(_) => {
                unimplemented! {}
            }
            llvm_ir::Terminator::Resume(_) => {
                unimplemented! {}
            }
            llvm_ir::Terminator::Unreachable(_) => {}
            llvm_ir::Terminator::CleanupRet(_) => {
                unimplemented! {}
            }
            llvm_ir::Terminator::CatchRet(_) => {
                unimplemented! {}
            }
            llvm_ir::Terminator::CatchSwitch(_) => {
                unimplemented! {}
            }
            llvm_ir::Terminator::CallBr(_) => {
                unimplemented! {}
            }
        }

        Ok(new_nodes)
    }

    pub fn generate_masks(
        &mut self,
        block_graph: &BlockGraph,
        id_tracker: &mut IdTracker,
    ) -> Result<Vec<Node>> {
        let mut topo = Topo::new(&block_graph.inner);

        let mut result = vec![];
        while let Some(node) = topo.next(&block_graph.inner) {
            result.append(&mut self.add_masks(node, &block_graph, id_tracker)?);
        }

        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use llvm_ir::{terminator, types::Types, BasicBlock, Terminator};

    use crate::{operand::ident, testutil::f32var};

    use super::*;

    #[test]
    fn adding_masks_works_for_diamond_if_statements() {
        /*
          1
         / \
        2   3
         \ /
          4
        */

        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 2.into(),
                    false_dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 4.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 4.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];

        let graph = BlockGraph::new(&basic_blocks);

        let mut id_tracker = IdTracker::new(100);

        let expected_nodes = vec![
            // Condition for 1->2
            Node::new(
                100,
                Kind::And(Operand::Const(Const::BoolConst(true)), ident(10.into())),
            ),
            // Not for condition
            Node::new(101, Kind::Not(ident(10.into()))),
            Node::new(
                102,
                Kind::And(Operand::Const(Const::BoolConst(true)), ident(101.into())),
            ),
            // joining node for node 4
            Node::new(103, Kind::Or(ident(102.into()), ident(100.into()))),
        ];

        let idx1 = graph.index_from_name(&1.into());
        let idx2 = graph.index_from_name(&2.into());
        let idx3 = graph.index_from_name(&3.into());
        let idx4 = graph.index_from_name(&4.into());

        let expected_entry_masks = vec![
            (idx1, Operand::Const(Const::BoolConst(true))),
            (idx2, ident(100.into())),
            (idx3, ident(102.into())),
            (idx4, ident(103.into())),
        ]
        .into_iter()
        .collect::<HashMap<_, _>>();

        let expected_edge_masks = vec![
            ((idx1, idx2), ident(100.into())),
            ((idx1, idx3), ident(102.into())),
            ((idx2, idx4), ident(100.into())),
            ((idx3, idx4), ident(102.into())),
        ]
        .into_iter()
        .collect::<HashMap<_, _>>();

        let mut masks = MaskTracker::new();
        let new_nodes = masks.generate_masks(&graph, &mut id_tracker);

        assert_eq!(masks.edge_masks, expected_edge_masks);
        assert_eq!(masks.entry_masks, expected_entry_masks);

        assert_eq!(new_nodes, Ok(expected_nodes));
    }
}
