use std::convert::TryFrom;

use log::warn;
use thiserror::Error as ThisError;

use llvm_ir::{constant::Float::Double, ConstantRef};
use llvm_ir::{Constant, Type};

use serde_derive::Serialize;

use crate::operand::Identifier;

#[derive(ThisError, Debug, Clone, PartialEq)]
pub enum Error {
    #[error("Unhandled global reference: {0:?}")]
    UnhandledGlobalReferenceType(llvm_ir::Type),
    #[error("8 bit ints are interpreted as boolean, but this one had a value of {0}")]
    NonBoolean8BitInt(u64),
    /// Attempted to store a constant with more than the specified number
    /// of bits, overflowing the internal maximum constant size
    #[error("Integer constant out of bounds: u32")]
    IntConstOverflow(u32),
    #[error("Null pointers are unsupported")]
    FoundNullPointer,
    #[error("Unhandled constant type: {0:?}")]
    UnhandledConst(Constant),
    #[error("When handling GetElementPtr")]
    ElementPtrError(#[from] Box<ElementPtrError>),
}

#[derive(ThisError, Debug, Clone, PartialEq)]
pub enum ElementPtrError {
    #[error("Failed to convert index {0} to const")]
    IndexConversionError(usize, #[source] Error),
    #[error("Index {0} was expected to be an integer but was {1:?}")]
    NonIntegerIndex(usize, Const),
    #[error("Element ptr address must be an array reference. Found {0:?}")]
    NonArrayReference(Const),
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum Const {
    BoolConst(bool),
    IntConst(u64),
    DoubleConst(f64),
    ArrayReference(Identifier),
    GetElementPtr { addr: Identifier, indices: Vec<u64> },
}

impl Const {
    pub fn expect_int(&self) -> Option<u64> {
        match self {
            Const::IntConst(x) => Some(*x),
            _ => None,
        }
    }

    pub fn descriptive_string(&self) -> String {
        match self {
            Const::BoolConst(val) => format!("c bool {}", val),
            Const::IntConst(val) => format!("c int {}", val),
            Const::DoubleConst(val) => format!("c frac {}", val),
            Const::ArrayReference(val) => format!("c array `{:?}`", val),
            Const::GetElementPtr { addr, indices } => {
                format!("c getelementptr {:?}[{:?}]", addr, indices)
            }
        }
    }
}

impl TryFrom<&Constant> for Const {
    type Error = Error;

    fn try_from(value: &Constant) -> Result<Self, Self::Error> {
        match value {
            Constant::Int { bits, value } => {
                if *bits == 1 {
                    match *value {
                        0 => Ok(Const::BoolConst(false)),
                        1 => Ok(Const::BoolConst(true)),
                        _ => Err(Error::NonBoolean8BitInt(*value)),
                    }
                } else if *bits == 8 {
                    // 8 bit integers comming out of llvm are treated as booleans.
                    // If we decide to implement 8 bit int support, this will cause issues
                    match *value {
                        0 => Ok(Const::BoolConst(false)),
                        1 => Ok(Const::BoolConst(true)),
                        _ => Err(Error::NonBoolean8BitInt(*value)),
                    }
                } else if *bits <= 64 {
                    Ok(Const::IntConst(*value))
                } else {
                    Err(Error::IntConstOverflow(*bits))
                }
            }
            Constant::Float(Double(val)) => Ok(Const::DoubleConst(*val)),
            Constant::GlobalReference { name, ty } => match ty.as_ref() {
                Type::ArrayType { .. } => Ok(Const::ArrayReference(name.into())),
                other => Err(Error::UnhandledGlobalReferenceType(other.clone())),
            },
            Constant::GetElementPtr(inner) => {
                // Recurse to the subcomponents and aquire their values
                let addr = Const::try_from(inner.address.clone())?;
                let indices = inner
                    .indices
                    .iter()
                    // Convert to constants
                    .map(Const::try_from)
                    // Make sure they are all integers and log errors if not, or if
                    // there were problems converting
                    .enumerate()
                    .map(|(i, r)| match r {
                        Ok(Const::IntConst(val)) => Ok(val),
                        Ok(other) => Err(ElementPtrError::NonIntegerIndex(i, other)),
                        Err(e) => Err(ElementPtrError::IndexConversionError(i, e)),
                    })
                    .collect::<Result<Vec<_>, _>>()
                    .map_err(|e| Error::ElementPtrError(Box::new(e)))?;

                // Get the target value
                match addr {
                    Const::ArrayReference(addr) => Ok(Const::GetElementPtr { addr, indices }),
                    other => Err(Error::ElementPtrError(Box::new(
                        ElementPtrError::NonArrayReference(other),
                    ))),
                }
            }
            Constant::Null(_) => Err(Error::FoundNullPointer),
            Constant::Undef(t) => match t.as_ref() {
                Type::FPType(llvm_ir::types::FPType::Double) => {
                    warn!("Found an undefined double. Assuming 0");
                    Ok(Const::DoubleConst(0.))
                }
                _ => Err(Error::UnhandledConst(value.clone())),
            },
            c => Err(Error::UnhandledConst(c.clone())),
        }
    }
}
impl TryFrom<Constant> for Const {
    type Error = Error;

    fn try_from(value: Constant) -> Result<Self, Self::Error> {
        Const::try_from(&value)
    }
}

impl TryFrom<&ConstantRef> for Const {
    type Error = Error;
    fn try_from(value: &ConstantRef) -> Result<Self, Error> {
        Const::try_from(value.as_ref())
    }
}
impl TryFrom<ConstantRef> for Const {
    type Error = Error;
    fn try_from(value: ConstantRef) -> Result<Self, Error> {
        Const::try_from(value.as_ref())
    }
}

#[cfg(test)]
mod const_conversion_tests {
    use super::*;

    use llvm_ir::{
        constant::GetElementPtr,
        types::{FPType, Types},
    };

    #[test]
    fn null_constants_throw_an_error() {
        let types = Types::blank_for_testing();
        let input = Constant::Null(types.pointer_to(types.fp(FPType::Double)));

        let expected = Err(Error::FoundNullPointer);

        let result = Const::try_from(&input);

        assert_eq!(result, expected);
    }

    #[test]
    fn getelementptr_conversion_works() {
        let types = Types::blank_for_testing();
        let input = ConstantRef::new(Constant::GetElementPtr(GetElementPtr {
            address: ConstantRef::new(Constant::GlobalReference {
                name: "target".into(),
                ty: types.array_of(types.fp(FPType::Double), 1),
            }),
            indices: vec![
                ConstantRef::new(Constant::Int { bits: 64, value: 0 }),
                ConstantRef::new(Constant::Int { bits: 64, value: 0 }),
            ],
            in_bounds: true,
        }));

        let expected = Const::GetElementPtr {
            addr: "target".into(),
            indices: vec![0, 0],
        };

        assert_eq!(Const::try_from(&input), Ok(expected));
    }
}
