use llvm_sys::core::LLVMGetEnumAttributeKindForName;

use std::ffi::CString;

pub fn attribute_kind_from_name(name: &str) -> u32 {
    let c_name = CString::new(name).unwrap();
    let c_len = name.len();

    let result = unsafe { LLVMGetEnumAttributeKindForName(c_name.as_ptr(), c_len) };

    if result == 0 {
        panic!("{}: No such attribute", name)
    }
    result
}
