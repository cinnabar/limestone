use llvm_ir::DebugLoc;
use llvm_ir::Name;
use llvm_ir::{instruction::Call, Constant, FPPredicate, Instruction};
use llvm_ir::{operand::Operand as LlvmOperand, IntPredicate};

use thiserror::Error;

use crate::operand::{Identifier, Operand};
use crate::typecheck::Type;

#[derive(Error, Debug, Clone, PartialEq)]
pub enum Error {
    #[error("Unhandled instruction: {0:?}")]
    UnhandledInstruction(Instruction),
    #[error("Unhandled fp predicate: {0:?}")]
    UnhandledFPPredicate(FPPredicate),
    #[error("Unhandled int predicate: {0:?}")]
    UnhandledIntPredicate(IntPredicate),
    #[error("Unhandled operand: {0:?}")]
    UnnamedOperand(LlvmOperand),
    #[error("Inline assembly is unsupported")]
    InlineAssembly,
    #[error("Function was empty")]
    EmptyFunction,
    #[error("Instruction result is not stored. (`dest` is unset)")]
    NoDestination,
    #[error("Call to unsupported function {0:?}")]
    UnsupportedFunctionCall(Call),
    #[error("Call to unnamed function {0:?}")]
    CallToUnnamedFunction(Name),
    #[error("Attempt to call unknown function {0}")]
    CallToUnknownFunction(String),
    #[error("Error in argument {0}")]
    ArgumentError(usize, #[source] Box<Error>),
    #[error("Argument must be an identifier")]
    ArgumentMustBeIdentifier,
    #[error("Error converting constant")]
    ConstConversionError(#[from] crate::consts::Error),
    #[error("Interpolation data must be a global variable. Was {0:?}")]
    NonGlobalInterpData(Identifier),

    // Errors specific to some instructions
    /// Alloca was run on an unsupported type
    #[error("Attempted to allocate a value of unsupported type: {0:?}")]
    UnsupportedAllocation(llvm_ir::Type),

    #[error("Exactly one index required in InsertValue instruction. Got {0:?}")]
    InvalidInsertValueIndices(Vec<u32>),
    #[error("InsertValue aggregate must be an identifier. Got {0:?}")]
    InvalidInsertValueAggregate(llvm_ir::Operand),

    #[error("Attempt to store at address that not a local operand")]
    NonAliasingStore,
    #[error("There are multiple store instructions to the same address")]
    MultipleStoresToSameAddress,

    /// The function contained no return instructions
    #[error("Function has no return terminator")]
    NoReturn,
    /// The function contained more than one return instruction
    #[error("Function has multiple return terminators")]
    MultipleReturns,
    /// The function contained a return instruction, but it did not
    /// return anything
    #[error("The function does not return a value")]
    NoReturnValue,
    /// A return value was present, but the value was not a named value
    #[error("Must return named value, found: {0:?}")]
    NonIdentReturnValue(Operand),
    #[error("Attempting to return non-struct by reference")]
    SretOfNonStruct,

    // Errors specific to some functions
    /// A lookup table was declared with a name that is non-constant
    #[error("Attempted to declare a lookup table for a non-const pointer. {0:?}")]
    NonConstLutPointer(LlvmOperand),
    #[error("Attempted to declare a lookup table for non-const data. {0:?}")]
    NonConstLutValue(Constant),

    #[error("A bound was specified for a non-local operand")]
    InputBoundSpecifiedForNonLocalOperand,
    #[error("Non-constant min value specified")]
    NonConstantInputMin,
    #[error("Non-constant max value specified")]
    NonConstantInputMax,

    // Type check errors
    #[error("Select node has ambiguous types. Branches must be identical but were {0:?}")]
    AmbiguousSelectType(Vec<Type>),
    #[error("Type of if branches do not match. OnTrue: {0:?}, OnFalse: {1:?}")]
    AmbiguousIfType(Type, Type),

    // Recursive errors
    #[error("Error handling function '{0}' with destination {1:?}")]
    FunctionError(String, Option<Name>, #[source] Box<Error>),
    #[error("{0}")]
    WithLocation(DebugLocation, #[source] Box<Error>),
    #[error("Error when checking the type of {0:?}")]
    TypeCheckError(Operand, #[source] Box<Error>),
    #[error("When handling instruction\n\t{0:?}")]
    InstructionHandlingError(Instruction, #[source] Box<Error>),

    #[error("An internal error occurred:")]
    Internal(#[from] InternalError),
}

pub trait WithLocation {
    fn with_location(self, location: &Option<DebugLoc>) -> Self;
}

impl<T> WithLocation for Result<T> {
    fn with_location(self, location: &Option<DebugLoc>) -> Self {
        self.map_err(|e| {
            Error::WithLocation(
                DebugLocation {
                    inner: location.clone(),
                },
                Box::new(e),
            )
        })
    }
}

// Errors that should never occur in correctly written user code. This might be things
// which the type checker should chekc, but are here for diagnostic purposes
#[derive(Error, Debug, Clone, PartialEq)]
pub enum InternalError {
    #[error("The input to the lut declaration has an unexpected type: {0:?}")]
    LutInputTypeError(Constant),
    #[error("Parameter out of bounds. Tried accessing parameter {0} out of {1}")]
    ParameterOutOfBounds(usize, usize),
    #[error("sret attribute on non-pointer")]
    SretOnNonPointer,
    #[error("Found no node for operand {0:?}")]
    NoSuchOperand(Operand),

    #[error("select statement with no branches")]
    EmptySelectStatement,

    #[error("Unlowered array reference {0:?}")]
    UnloweredArrayReference(Identifier),
}

#[derive(Debug, Clone, PartialEq)]
pub struct DebugLocation {
    pub inner: Option<DebugLoc>,
}

impl std::fmt::Display for DebugLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.inner {
            Some(DebugLoc {
                line,
                filename,
                directory,
                ..
            }) => write!(
                f,
                "At line: {} of {}/{}",
                line,
                directory.as_ref().map(|x| x.as_str()).unwrap_or("."),
                filename
            ),
            None => write!(f, "At unknown location"),
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;

/**
  Error produced when loading a module
*/
#[derive(Error, Debug, Clone, PartialEq)]
pub enum LoadError {
    /// The module contained more than one function
    #[error("The module contains more than one function")]
    MultipleFunctions,
    /// The module did not contain any functions
    #[error("The module contains no functions")]
    NoFunctions,

    /// Failiure to convert the function into a graph
    #[error("Failed to convert function to graph")]
    ConversionFailure(#[from] Error),
    /// Error while lowering graph
    #[error("Failed to lower instructions")]
    Lowering(#[source] crate::lowering::Error),
    /// Error when finding function return value
    #[error("Failed to find function return value")]
    ReturnValue(#[source] Error),
}
