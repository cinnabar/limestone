use std::borrow::Cow;
use std::collections::HashMap;
use std::io::Cursor;

use dot::{render, Edges, GraphWalk, Id, LabelText, Labeller, Nodes, Style};

use crate::graph::{Graph, GraphDiff};
use crate::graph_node::Node;
use crate::operand::{Identifier, Operand};

const COLORS: &'static [&'static str] = &[
    "aquamarine",
    "brown1",
    "bisque",
    "darkgreen",
    "darkslategray",
    "gold",
    "hotpink",
    "indigo",
    "limegreen",
];

struct DotGraph {
    labels: Vec<String>,
    edges: Vec<(usize, usize)>,
    color: HashMap<usize, String>,
    style: HashMap<usize, Style>,
    edge_color: HashMap<(usize, usize), String>,
}

impl DotGraph {
    pub fn new() -> Self {
        Self {
            labels: vec![],
            edges: vec![],
            color: HashMap::new(),
            style: HashMap::new(),
            edge_color: HashMap::new(),
        }
    }

    /// Add a new node to the graph. Return the id of the newly created node
    pub fn add_node(&mut self, label: String) -> usize {
        self.labels.push(label);
        self.labels.len() - 1
    }

    pub fn add_edge(&mut self, from: usize, to: usize) {
        self.edges.push((from, to))
    }

    pub fn set_color(&mut self, node: usize, color: &str) {
        self.color.insert(node, color.to_string());
    }

    pub fn set_style(&mut self, node: usize, style: Style) {
        self.style.insert(node, style);
    }

    pub fn set_edge_color(&mut self, edge: (usize, usize), color: &str) {
        self.edge_color.insert(edge, color.to_string());
    }
}

impl GraphWalk<'_, usize, (usize, usize)> for DotGraph {
    fn nodes(&self) -> Nodes<usize> {
        Cow::Owned((0..self.labels.len()).collect())
    }
    fn edges(&self) -> Edges<(usize, usize)> {
        Cow::Owned(self.edges.clone())
    }
    fn source(&self, edge: &(usize, usize)) -> usize {
        edge.0
    }
    fn target(&self, edge: &(usize, usize)) -> usize {
        edge.1
    }
}

impl Labeller<'_, usize, (usize, usize)> for DotGraph {
    fn graph_id(&self) -> Id {
        Id::new("g1").unwrap()
    }
    fn node_id(&self, node: &usize) -> Id {
        Id::new(format!("n{}", node)).unwrap()
    }
    fn node_label(&self, node: &usize) -> LabelText {
        LabelText::LabelStr(Cow::Owned(self.labels[*node].clone()))
    }
    fn node_color(&self, node: &usize) -> Option<LabelText> {
        self.color
            .get(node)
            .map(|c| LabelText::LabelStr(Cow::Owned(c.clone())))
    }
    fn edge_color(&self, edge: &(usize, usize)) -> Option<LabelText> {
        self.edge_color
            .get(edge)
            .map(|c| LabelText::LabelStr(Cow::Owned(c.clone())))
    }
    fn node_style(&self, node: &usize) -> Style {
        self.style.get(node).cloned().unwrap_or(Style::None)
    }
}

struct IdTracker {
    inner: HashMap<Identifier, Vec<usize>>,
}

impl IdTracker {
    pub fn new() -> Self {
        Self {
            inner: HashMap::new(),
        }
    }

    pub fn insert(&mut self, ident: Identifier, id: usize) {
        self.inner.entry(ident).or_insert(vec![]).push(id);
    }

    pub fn last_id(&self, identifier: &Identifier) -> usize {
        self.inner
            .get(identifier)
            .map(|v| *v.first().unwrap())
            .unwrap()
    }

    pub fn has_id(&self, identifier: &Identifier) -> bool {
        self.inner.contains_key(identifier)
    }
}

pub fn visualise_node_graph(graph: &Graph) -> String {
    visualise_node_graph_diff(graph, &[])
}

fn add_edges(node: &Node, indices: &IdTracker, dotgraph: &mut DotGraph) {
    for operand in node.kind.operands() {
        match operand {
            Operand::Ident(ident) => {
                dotgraph.add_edge(indices.last_id(&node.name), indices.last_id(&ident));
            }
            Operand::Const(value) => {
                let new_index = dotgraph.add_node(value.descriptive_string());
                dotgraph.add_edge(indices.last_id(&node.name), new_index);
            }
        }
    }
}

pub fn visualise_node_graph_diff(graph: &Graph, diff: &[Vec<GraphDiff>]) -> String {
    // usize is used just to get a Display impl for the dotgraph. It is technically the
    // edge weight.
    let mut dotgraph = DotGraph::new();
    let mut indices = IdTracker::new();

    // Insert all the nodes, getting the IDs
    for (name, Node { kind, .. }) in &graph.nodes {
        let label: String = name.into();
        indices.insert(
            name.clone(),
            dotgraph.add_node(format!("{}\n{}", kind.description(), label)),
        );
    }

    // Add edges
    for (_name, node) in &graph.nodes {
        add_edges(node, &indices, &mut dotgraph)
    }

    // Add diff nodes
    for (color_id, diff_list) in diff.iter().enumerate() {
        let color = COLORS[color_id % COLORS.len()];

        for diff in diff_list {
            match diff {
                GraphDiff::OnlyLhs(lhs) => {
                    // This node is present in the new graph but not the old. Color it as
                    // added this run

                    // If it is part of the final graph, we can just adjust the color, if
                    // not it has been deleted again and we need to re-add it to the
                    let id = add_node(&mut dotgraph, lhs, &mut indices, false);
                    dotgraph.set_style(id, Style::Filled);
                    dotgraph.set_color(id, color);
                }
                GraphDiff::OnlyRhs(rhs) => {
                    // This node was removed in this iteration
                    let id = add_node(&mut dotgraph, rhs, &mut indices, true);
                    dotgraph.set_color(id, color);
                    dotgraph.set_style(id, Style::Dashed);
                }
                GraphDiff::Replaced(lhs, rhs) => {
                    // A node was replaced, color the new node as added and the old
                    // as removed
                    let lhs_id = add_node(&mut dotgraph, lhs, &mut indices, false);
                    dotgraph.set_style(lhs_id, Style::Filled);
                    dotgraph.set_color(lhs_id, color);

                    let rhs_id = add_node(&mut dotgraph, rhs, &mut indices, true);
                    dotgraph.set_style(rhs_id, Style::Dashed);
                    dotgraph.set_color(rhs_id, color);

                    // Add a link between the nodes showing that the node replaced
                    // the previous one
                    dotgraph.add_edge(lhs_id, rhs_id);
                    dotgraph.set_edge_color((lhs_id, rhs_id), color)
                }
            }
        }
    }

    // Add edges for the diff nodes
    for (_color_id, diff_list) in diff.iter().enumerate() {
        for diff in diff_list {
            match diff {
                GraphDiff::OnlyLhs(lhs) => {
                    add_edges(lhs, &indices, &mut dotgraph);
                }
                GraphDiff::OnlyRhs(rhs) => {
                    add_edges(rhs, &indices, &mut dotgraph);
                }
                GraphDiff::Replaced(lhs, rhs) => {
                    add_edges(lhs, &indices, &mut dotgraph);
                    add_edges(rhs, &indices, &mut dotgraph);
                }
            }
        }
    }

    let mut result = Cursor::new(Vec::new());
    render(&dotgraph, &mut result).unwrap();
    String::from_utf8(result.into_inner()).unwrap()
}

fn add_node(
    graph: &mut DotGraph,
    node: &Node,
    indices: &mut IdTracker,
    add_duplicate: bool,
) -> usize {
    if indices.has_id(&node.name) && !add_duplicate {
        indices.last_id(&node.name)
    } else {
        let label: String = node.name.clone().into();
        let new_index = graph.add_node(format!("{}\n{}", node.kind.description(), label));
        indices.insert(node.name.clone(), new_index);
        new_index
    }
}
