use std::collections::HashMap;

use thiserror::Error;

use crate::graph::Graph;
use crate::graph_node::{Kind, Node};
use crate::name_util::StringifyableName;
use crate::operand::Identifier;

#[derive(Clone, PartialEq, Debug, Error)]
pub enum Error {
    #[error("The identifier {0:?} has no associated bound")]
    NoAssociatedBounds(Identifier),
    #[error("Not a bound")]
    NotABound,
}

/**
  Converts `load(getelementpointer(<lut pointer>))` into look up instructions
*/
pub fn lower_inputs(graph: Graph) -> Result<Graph, super::Error> {
    let mut new_graph = Graph {
        nodes: HashMap::new(),
    };

    for (name, value) in &graph.nodes {
        match &value.kind {
            Kind::UnboundedInput => {
                let expected_name = Identifier::Str(format!("__bounds__{}", name.stringify()));
                let b = if let Some(bound_node) = graph.nodes.get(&expected_name) {
                    bound_node
                } else {
                    Err(super::Error::InputBoundError(Error::NoAssociatedBounds(
                        name.clone(),
                    )))?
                };

                let (min, max) = if let Kind::InputBound(target, min, max) = &b.kind {
                    assert_eq!(target, name);
                    (min, max)
                } else {
                    Err(super::Error::InputBoundError(Error::NotABound))?
                };

                new_graph.add_node(Node::new(name.clone(), Kind::Input(*min, *max)));
            }
            _ => {
                new_graph.nodes.insert(name.clone(), value.clone());
            }
        }
    }
    Ok(new_graph)
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashMap;

    use crate::graph_node::node;
    use crate::operand::ident;

    #[test]
    fn lut_lowering_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node("__bounds__x", Kind::InputBound("x".into(), 1., 5.)),
                node("x", Kind::UnboundedInput),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = Graph::from_tuples(
            vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node("__bounds__x", Kind::InputBound("x".into(), 1., 5.)),
                node("x", Kind::Input(1., 5.)),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        );

        assert_eq!(Ok(expected), lower_inputs(graph));
    }
}
