/**
  Functions used for lowering nodes, i.e. converting one or more nodes
  into new nodes that are better suited for further operations
*/
use thiserror::Error;

use crate::consts::Const;
use crate::graph::Graph;
use crate::graph_node::{Kind, Node};
use crate::operand::{Identifier, Operand};
use crate::{globals::GlobalTable, name_util::StringifyableName};

use super::util::{get_get_element_ptr, get_global_double_array};

#[derive(Debug, Clone, PartialEq, Error)]
pub enum Error {
    #[error("Expected load addr to be an identifier. Got {0:#?}")]
    NonIdentAddr(Operand),
    #[error("Can not index array with {0:?}")]
    UnsupportedIndex(Operand),
    #[error("Error looking up GetElementPtr")]
    GetElementPtrError(#[source] super::util::GetElementPtrError),
    #[error("Global lookup failed")]
    GlobalLookupError(#[from] super::util::GlobalLookupError),
}

/**
  Converts `load(getelementpointer(<lut pointer>))` into look up instructions
*/
pub fn try_lower_lut(
    (name, load_operand): (&Identifier, &Operand),
    graph: &Graph,
    globals: &GlobalTable,
) -> Result<Vec<Node>, Error> {
    match load_operand {
        Operand::Ident(addr) => {
            let elementptr =
                get_get_element_ptr(&graph, addr).map_err(Error::GetElementPtrError)?;

            // Extract the address identifier, returning an error
            // if no such identifier exists
            let name_var = match elementptr.address {
                Operand::Const(Const::ArrayReference(ident)) => ident.clone(),
                other => Err(Error::UnsupportedIndex(other.clone()))?,
            };
            let idx = elementptr.indices.iter().skip(1).next().unwrap().clone();

            let lut_values = get_global_double_array(&name_var, globals)?;

            Ok(vec![Node {
                name: name.clone(),
                kind: Kind::Lookup {
                    name: name_var.stringify(),
                    data: lut_values.clone(),
                    idx,
                },
            }])
        }
        other => Err(Error::NonIdentAddr(other.clone())),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::HashMap;

    use crate::globals::Global;
    use crate::graph_node::node;
    use crate::operand::ident;

    #[test]
    fn lut_lowering_without_lut_markers_works() {
        let graph = Graph {
            nodes: vec![
                // Ensure we don't mangle other nodes
                node("dummy", Kind::Nop(ident("a".into()))),
                node(
                    "ptr",
                    Kind::GetElementPtr {
                        address: Operand::Const(Const::ArrayReference("lut_vals".into())),
                        indices: vec![Operand::Const(Const::IntConst(0)), ident("idx".into())],
                    },
                ),
                node("result", Kind::Load(ident("ptr".into()))),
            ]
            .into_iter()
            .collect::<HashMap<_, _>>(),
        };

        let expected = vec![
            // Ensure we don't mangle other nodes
            Node::new(
                "result",
                Kind::Lookup {
                    name: "lut_vals".into(),
                    data: vec![0., 1., 3., 5.],
                    idx: ident("idx".into()),
                },
            ),
        ];

        let strings = GlobalTable {
            content: vec![("lut_vals".into(), Global::DoubleArray(vec![0., 1., 3., 5.]))]
                .into_iter()
                .collect(),
        };

        assert_eq!(
            Ok(expected),
            try_lower_lut((&"result".into(), &ident("ptr".into())), &graph, &strings)
        );
    }
}
