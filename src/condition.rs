use crate::graph_node::{Kind, Node};
use crate::id_tracker::IdTracker;
use crate::operand::{ident, Operand};
use crate::uniquevec::UniqueVec;

#[derive(Debug, Clone, PartialEq)]
pub enum Condition {
    Op(Operand),
    True(Box<Condition>),
    False(Box<Condition>),
}

// Constructors
impl Condition {
    /// Convenience function for constructing a condition from an operand that
    /// should be true
    pub fn op_true(op: Operand) -> Self {
        Condition::True(Box::new(Condition::Op(op)))
    }
    /// Convenience function for constructing a condition from an operand that
    /// should be true
    pub fn op_false(op: Operand) -> Self {
        Condition::False(Box::new(Condition::Op(op)))
    }

    /// Shorthand for creating a False instance from a non boxed element
    pub fn cond_false(cond: Condition) -> Self {
        Condition::False(Box::new(cond))
    }

    /// Shorthand for creating a True instance from a non boxed element
    pub fn cond_true(cond: Condition) -> Self {
        Condition::True(Box::new(cond))
    }
}

/// A condition that has been simplified to only contain `op` or `!op`
#[derive(Debug, Clone, PartialEq)]
pub enum SCondition {
    True(Operand),
    False(Operand),
}

impl Condition {
    pub fn simplify(&self) -> SCondition {
        match self {
            Condition::Op(inner) => SCondition::True(inner.clone()),
            Condition::True(inner) => inner.simplify(),
            Condition::False(inner) => {
                let simplified = inner.simplify();

                // Check if this is a double not
                match simplified {
                    SCondition::False(inner) => SCondition::True(inner),
                    SCondition::True(inner) => SCondition::False(inner),
                }
            }
        }
    }
}

// TODO: Make this prettier by adding a descriptor for raw conditions
pub fn condition_descriptor(conditions: &[Condition]) -> String {
    let mut result = String::new();
    for condition in conditions {
        result += "_";
        match condition {
            Condition::Op(op) => result += &format!("{:?}", op),
            Condition::True(op) => result += &format!("{:?}", op),
            Condition::False(op) => result += &format!("!{:?}", op),
        }
    }
    result
}

fn condition_to_node(condition: &Condition, id_tracker: &mut IdTracker) -> (Operand, Option<Node>) {
    match condition.simplify() {
        SCondition::True(operand) => (operand, None),
        SCondition::False(operand) => {
            let new_id = match &operand {
                Operand::Ident(id) => id_tracker.next_linking(&id, &format!("Inverse of")),
                _ => id_tracker.next_no_purpose(),
            };
            let new_node = Node::new(new_id, Kind::Not(operand));
            (ident(new_id.into()), Some(new_node))
        }
    }
}

/**
  Generate nodes for not(abc...) & not(cde...) & ..., the disjunctive normal form of the conditions

  Returns the node that represents the condition, along with every other new node
  that was created
*/
pub fn create_inverse_dnf(
    conditions: &UniqueVec<UniqueVec<Condition>>,
    id_tracker: &mut IdTracker,
) -> (Operand, Vec<Node>) {
    let mut new_nodes = vec![];
    let mut clause_nodes = vec![];

    for clause in &conditions.inner {
        let mut prev_op = {
            let (prev_op, new_node) = condition_to_node(clause.first().unwrap(), id_tracker);
            new_node.map(|node| new_nodes.push(node));
            prev_op
        };

        // Skip because we already added the node above
        for literal in clause.iter().skip(1) {
            // Potentially create a new node for the literal
            let literal_op = {
                let (literal_op, literal_node) = condition_to_node(literal, id_tracker);
                literal_node.map(|node| new_nodes.push(node));
                literal_op
            };

            // Create an and node to merge this node with the previous one
            let new_id = id_tracker.next_multi_link(&[&prev_op, &literal_op], "Inner and DNF");
            let new_node = Node::new(new_id, Kind::And(prev_op, literal_op));
            new_nodes.push(new_node);
            prev_op = ident(new_id.into());
        }

        // Invert this clause
        let inv_id = id_tracker.next_linking_operand(&prev_op, "Inverse of DNF clause");
        let inverse = Node::new(inv_id, Kind::Not(prev_op));
        new_nodes.push(inverse);
        clause_nodes.push(Operand::Ident(inv_id.into()))
    }

    let mut clause_nodes = clause_nodes.into_iter();
    let mut prev_op = clause_nodes
        .next()
        .expect("got a condition with no clauses");

    for op in clause_nodes {
        // Create an and node to merge this node with the previous one
        let new_id = id_tracker.next_multi_link(&[&prev_op, &op], "And for inverse DNF");
        let new_node = Node::new(new_id, Kind::And(prev_op, op));
        new_nodes.push(new_node);
        prev_op = ident(new_id.into());
    }
    (prev_op, new_nodes)
}

#[cfg(test)]
mod simplification_tests {
    use super::*;

    use crate::operand::ident;

    #[test]
    fn true_nodes_are_no_ops() {
        let input = Condition::cond_true(Condition::Op(ident(0.into())));

        assert_eq!(input.simplify(), SCondition::True(ident(0.into())));
    }

    #[test]
    fn repeated_nots_are_simplified_away() {
        let input = Condition::cond_false(Condition::cond_false(Condition::Op(ident(0.into()))));

        assert_eq!(input.simplify(), SCondition::True(ident(0.into())));
    }

    #[test]
    fn non_repeated_nots_are_not_simplified() {
        let input = Condition::cond_false(Condition::Op(ident(0.into())));

        assert_eq!(input.simplify(), SCondition::False(ident(0.into())));
    }
}

#[cfg(test)]
mod node_generation_tests {
    use super::*;

    use pretty_assertions::assert_eq;

    #[test]
    fn node_generation_works() {
        let conditions: UniqueVec<UniqueVec<_>> = vec![
            vec![
                Condition::op_true(ident(0.into())),
                Condition::op_true(ident(1.into())),
            ]
            .into(),
            vec![
                Condition::op_true(ident(2.into())),
                Condition::op_false(ident(3.into())),
            ]
            .into(),
        ]
        .into();

        let expected = (
            ident(15.into()),
            vec![
                Node::new(10, Kind::And(ident(0.into()), ident(1.into()))),
                Node::new(11, Kind::Not(ident(10.into()))),
                Node::new(12, Kind::Not(ident(3.into()))),
                Node::new(13, Kind::And(ident(2.into()), ident(12.into()))),
                Node::new(14, Kind::Not(ident(13.into()))),
                Node::new(15, Kind::And(ident(11.into()), ident(14.into()))),
            ],
        );

        let mut next_id = IdTracker::new(10);
        assert_eq!(create_inverse_dnf(&conditions, &mut next_id), expected);
    }
}
