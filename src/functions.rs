use crate::consts::Const;
use crate::error::{Error, InternalError, Result};
use crate::graph_node::{GlobalIdent, Kind, Node, UnloweredInterp1, UnloweredInterp2};
use crate::name_util::StringifyableName;
use crate::operand::{Identifier, Operand};

use either::Either;
use llvm_ir::constant::Float;
use llvm_ir::instruction;
use llvm_ir::Constant;
use llvm_ir::Name;
use llvm_ir::Operand as LlvmOperand;

pub fn call(ins: &instruction::Call) -> Result<Option<Node>> {
    match &ins.function {
        Either::Right(operand) => match operand {
            LlvmOperand::ConstantOperand(const_ref) => {
                match const_ref.as_ref() {
                    Constant::GlobalReference { name, .. } => {
                        let name_str = if let Name::Name(s) = name {
                            s
                        } else {
                            Err(Error::CallToUnnamedFunction(name.clone()))?
                        };

                        match name_str.as_str() {
                            "declare_bounds" => handle_declare_bounds(ins).map(Some),
                            "limestone_sqrt" => handle_sqrt(ins).map(Some),
                            "limestone_trunc" => handle_trunc(ins).map(Some),
                            "limestone_abs" => handle_abs(ins).map(Some),
                            "limestone_max" => handle_max(ins).map(Some),
                            "limestone_min" => handle_min(ins).map(Some),
                            "interp1equid" => handle_interp1(ins).map(Some),
                            "interp1equid_valid" => handle_interp1_valid(ins).map(Some),
                            "interp2equid" => handle_interp2(ins).map(Some),
                            "interp2equid_valid" => handle_interp2_valid(ins).map(Some),
                            "opaque_barrier" => handle_opaque_barrier(ins).map(Some),
                            // Nop functions we should ignore
                            "__invisible_nop" => Ok(None),
                            // Llvm debug functions which we should ignore
                            "llvm.dbg.value" => Ok(None),
                            "llvm.dbg.declare" => Ok(None),
                            _ => Err(Error::CallToUnknownFunction(name_str.to_string())),
                        }
                        .map_err(|e| {
                            Error::FunctionError(
                                name_str.to_string(),
                                ins.dest.clone(),
                                Box::new(e),
                            )
                        })
                    }
                    _ => Err(Error::UnsupportedFunctionCall(ins.clone())),
                }
            }
            _ => Err(Error::UnsupportedFunctionCall(ins.clone())),
        },
        Either::Left(_) => Err(Error::InlineAssembly),
    }
}

/// Creates a node from a Kind and an instruction with the node name
/// being the instruction destination parameter
fn kind_to_destination(ins: &instruction::Call, kind: Kind) -> Result<Node> {
    if let Some(dest) = &ins.dest {
        Ok(Node::new(dest, kind))
    } else {
        Err(Error::NoDestination)
    }
}

fn handle_declare_bounds(ins: &instruction::Call) -> Result<Node> {
    // Get the argument, assuming it is constant
    let (target, min_, max_) = if let [(LlvmOperand::LocalOperand { name, .. }, _), (min, _), (max, _)] =
        ins.arguments.as_slice()
    {
        (name, min, max)
    } else {
        Err(Error::InputBoundSpecifiedForNonLocalOperand)?
    };

    let min = if let Some(Constant::Float(Float::Double(val))) = min_.as_constant() {
        val
    } else {
        Err(Error::NonConstantInputMin)?
    };
    let max = if let Some(Constant::Float(Float::Double(val))) = max_.as_constant() {
        val
    } else {
        Err(Error::NonConstantInputMax)?
    };

    // This is kind of ugly, but we have nowhere to put metadata. So we'll
    // create a new node with a name based on the target name.
    let target_ident: Identifier = target.into();
    let node_name = format!("__bounds__{}", target_ident.stringify());
    Ok(Node::new(
        Identifier::Str(node_name),
        Kind::InputBound(target_ident, *min, *max),
    ))
}

macro_rules! unop_instruction_handler {
    ($name:ident, $kind:ident) => {
        fn $name(ins: &instruction::Call) -> Result<Node> {
            let (operand, _) = &ins.arguments[0];
            kind_to_destination(ins, Kind::$kind(Operand::from_llvm(operand)?))
        }
    };
}

fn get_argument(ins: &instruction::Call, index: usize) -> Result<Operand> {
    let (arg, _) =
        ins.arguments
            .get(index)
            .ok_or(Error::Internal(InternalError::ParameterOutOfBounds(
                index,
                ins.arguments.len(),
            )))?;

    Operand::from_llvm(&arg).map_err(|e| Error::ArgumentError(index, Box::new(e)))
}

unop_instruction_handler!(handle_sqrt, Sqrt);
unop_instruction_handler!(handle_trunc, Truncate);
unop_instruction_handler!(handle_abs, Abs);
unop_instruction_handler!(handle_opaque_barrier, OpaqueBarrier);

macro_rules! binop_instruction_handler {
    ($name:ident, $kind:ident) => {
        fn $name(ins: &instruction::Call) -> Result<Node> {
            let lhs = get_argument(ins, 0)?;
            let rhs = get_argument(ins, 1)?;
            kind_to_destination(ins, Kind::$kind(lhs, rhs))
        }
    };
}

binop_instruction_handler!(handle_min, Min);
binop_instruction_handler!(handle_max, Max);

fn get_interp_array_name(ins: &instruction::Call, index: usize) -> Result<GlobalIdent> {
    let operand = get_argument(ins, index)?;
    match operand {
        Operand::Ident(ident) => Err(Error::NonGlobalInterpData(ident)),
        Operand::Const(Const::ArrayReference(id)) => Ok(GlobalIdent(id)),
        Operand::Const(Const::GetElementPtr { addr, indices }) => {
            if indices.as_slice() != [0, 0] {
                Err(Error::ArgumentMustBeIdentifier)
            } else {
                Ok(GlobalIdent(addr))
            }
        }
        _ => Err(Error::ArgumentMustBeIdentifier),
    }
    .map_err(|e| Error::ArgumentError(index, Box::new(e)))
}

fn handle_interp1_args(ins: &instruction::Call) -> Result<UnloweredInterp1> {
    let x = get_interp_array_name(ins, 0)?;
    let y = get_interp_array_name(ins, 1)?;
    let x_point = get_argument(ins, 2)?;

    Ok(UnloweredInterp1 { x, y, x_point })
}
fn handle_interp2_args(ins: &instruction::Call) -> Result<UnloweredInterp2> {
    let x = get_interp_array_name(ins, 0)?;
    let y = get_interp_array_name(ins, 1)?;
    let z = get_interp_array_name(ins, 2)?;
    let x_point = get_argument(ins, 3)?;
    let y_point = get_argument(ins, 4)?;

    Ok(UnloweredInterp2 {
        x,
        y,
        z,
        x_point,
        y_point,
    })
}

fn handle_interp1(ins: &instruction::Call) -> Result<Node> {
    let args = handle_interp1_args(ins)?;
    kind_to_destination(ins, Kind::UnloweredInterp1(args))
}
// NOTE: This function does not have an explicit test as its behaviour is simlar to handle_interp1
fn handle_interp1_valid(ins: &instruction::Call) -> Result<Node> {
    let args = handle_interp1_args(ins)?;
    kind_to_destination(ins, Kind::UnloweredInterp1Valid(args))
}
fn handle_interp2(ins: &instruction::Call) -> Result<Node> {
    let args = handle_interp2_args(ins)?;
    kind_to_destination(ins, Kind::UnloweredInterp2(args))
}
// NOTE: This function does not have an explicit test as its behaviour is simlar to handle_interp1
fn handle_interp2_valid(ins: &instruction::Call) -> Result<Node> {
    let args = handle_interp2_args(ins)?;
    kind_to_destination(ins, Kind::UnloweredInterp2Valid(args))
}

#[cfg(test)]
mod misc_function_tests {
    use super::*;

    use crate::graph_node::{Kind, Node};
    use crate::operand::ident;

    use llvm_ir::constant::Constant;
    use llvm_ir::function::CallingConvention;
    use llvm_ir::instruction::Call;
    use llvm_ir::Operand as LlvmOperand;
    use llvm_ir::{
        types::{FPType, Types},
        ConstantRef,
    };

    #[test]
    fn declare_bounds_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "declare_bounds".into(),
                    ty: types.func_type(
                        types.fp(FPType::Double),
                        vec![
                            types.fp(FPType::Double),
                            types.fp(FPType::Double),
                            types.fp(FPType::Double),
                        ],
                        false,
                    ),
                },
            ))),
            arguments: vec![
                (
                    LlvmOperand::LocalOperand {
                        name: "conv".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::Float(
                        llvm_ir::constant::Float::Double(0.0),
                    ))),
                    vec![],
                ),
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::Float(
                        llvm_ir::constant::Float::Double(10.0),
                    ))),
                    vec![],
                ),
            ],
            return_attributes: vec![],
            dest: None,
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("__bounds__conv", Kind::InputBound("conv".into(), 0., 10.));

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn opaque_barrier_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "opaque_barrier".into(),
                    ty: types.func_type(
                        types.fp(FPType::Double),
                        vec![types.fp(FPType::Double)],
                        false,
                    ),
                },
            ))),
            arguments: vec![(
                LlvmOperand::LocalOperand {
                    name: "input".into(),
                    ty: types.fp(FPType::Double),
                },
                vec![],
            )],
            return_attributes: vec![],
            dest: Some("output".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("output", Kind::OpaqueBarrier(ident("input".into())));

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn truncate_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "limestone_trunc".into(),
                    ty: types.func_type(types.int(32), vec![types.fp(FPType::Double)], false),
                },
            ))),
            arguments: vec![(
                LlvmOperand::LocalOperand {
                    name: "div".into(),
                    ty: types.fp(FPType::Double),
                },
                vec![],
            )],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("call", Kind::Truncate(ident("div".into())));

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn sqrt_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "limestone_sqrt".into(),
                    ty: types.func_type(
                        types.fp(FPType::Double),
                        vec![types.fp(FPType::Double)],
                        false,
                    ),
                },
            ))),
            arguments: vec![(
                LlvmOperand::LocalOperand {
                    name: "value".into(),
                    ty: types.fp(FPType::Double),
                },
                vec![],
            )],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("call", Kind::Sqrt(ident("value".into())));

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn abs_works() {
        // Using inside functions is kind of ugly, but we'll be importing a *lot*
        // of stuff to declare this monstrosity
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "limestone_abs".into(),
                    ty: types.func_type(
                        types.fp(FPType::Double),
                        vec![types.fp(FPType::Double)],
                        false,
                    ),
                },
            ))),
            arguments: vec![(
                LlvmOperand::LocalOperand {
                    name: "value".into(),
                    ty: types.fp(FPType::Double),
                },
                vec![],
            )],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("call", Kind::Abs(ident("value".into())));

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn max_works() {
        let types = Types::blank_for_testing();

        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "limestone_max".into(),
                    ty: types.func_type(
                        types.fp(FPType::Double),
                        vec![types.fp(FPType::Double)],
                        false,
                    ),
                },
            ))),
            arguments: vec![
                (
                    LlvmOperand::LocalOperand {
                        name: "lhs".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
                (
                    LlvmOperand::LocalOperand {
                        name: "rhs".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
            ],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("call", Kind::Max(ident("lhs".into()), ident("rhs".into())));

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn min_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "limestone_min".into(),
                    ty: types.func_type(
                        types.fp(FPType::Double),
                        vec![types.fp(FPType::Double)],
                        false,
                    ),
                },
            ))),
            arguments: vec![
                (
                    LlvmOperand::LocalOperand {
                        name: "lhs".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
                (
                    LlvmOperand::LocalOperand {
                        name: "rhs".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
            ],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new("call", Kind::Min(ident("lhs".into()), ident("rhs".into())));

        assert_eq!(result, Ok(Some(expected)));
    }
}

#[cfg(test)]
mod interp_tests {
    use super::*;

    use llvm_ir::function::CallingConvention;
    use llvm_ir::instruction::Call;
    use llvm_ir::{
        constant::Constant,
        types::{FPType, Types},
        ConstantRef,
    };

    use crate::graph_node::{Kind, Node};

    use llvm_ir::constant::GetElementPtr;
    use llvm_ir::Operand as LlvmOperand;

    #[test]
    fn interp1equid_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "interp1equid".into(),
                    ty: types.func_type(
                        types.pointer_to(types.fp(FPType::Double)),
                        vec![types.pointer_to(types.int(8))],
                        false,
                    ),
                },
            ))),
            arguments: vec![
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::GetElementPtr(
                        GetElementPtr {
                            address: ConstantRef::new(Constant::GlobalReference {
                                name: "x".into(),
                                ty: types.array_of(types.fp(FPType::Double), 1),
                            }),
                            indices: vec![
                                ConstantRef::new(Constant::Int { bits: 64, value: 0 }),
                                ConstantRef::new(Constant::Int { bits: 64, value: 0 }),
                            ],
                            in_bounds: true,
                        },
                    ))),
                    vec![],
                ),
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::GlobalReference {
                        name: "y".into(),
                        ty: types.array_of(types.fp(FPType::Double), 1),
                    })),
                    vec![],
                ),
                (
                    LlvmOperand::LocalOperand {
                        name: "x_point".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
            ],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new(
            "call",
            Kind::UnloweredInterp1(UnloweredInterp1 {
                x: GlobalIdent("x".into()),
                y: GlobalIdent("y".into()),
                x_point: Operand::Ident("x_point".into()),
            }),
        );

        assert_eq!(result, Ok(Some(expected)));
    }

    #[test]
    fn interp2equid_works() {
        let types = Types::blank_for_testing();
        let input = Call {
            function: Either::Right(LlvmOperand::ConstantOperand(ConstantRef::new(
                Constant::GlobalReference {
                    name: "interp2equid".into(),
                    ty: types.func_type(
                        types.pointer_to(types.fp(FPType::Double)),
                        vec![types.pointer_to(types.int(8))],
                        false,
                    ),
                },
            ))),
            arguments: vec![
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::GetElementPtr(
                        GetElementPtr {
                            address: ConstantRef::new(Constant::GlobalReference {
                                name: "x".into(),
                                ty: types.array_of(types.fp(FPType::Double), 1),
                            }),
                            indices: vec![
                                ConstantRef::new(Constant::Int { bits: 64, value: 0 }),
                                ConstantRef::new(Constant::Int { bits: 64, value: 0 }),
                            ],
                            in_bounds: true,
                        },
                    ))),
                    vec![],
                ),
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::GlobalReference {
                        name: "y".into(),
                        ty: types.array_of(types.fp(FPType::Double), 1),
                    })),
                    vec![],
                ),
                (
                    LlvmOperand::ConstantOperand(ConstantRef::new(Constant::GlobalReference {
                        name: "z".into(),
                        ty: types.array_of(types.fp(FPType::Double), 1),
                    })),
                    vec![],
                ),
                (
                    LlvmOperand::LocalOperand {
                        name: "x_point".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
                (
                    LlvmOperand::LocalOperand {
                        name: "y_point".into(),
                        ty: types.fp(FPType::Double),
                    },
                    vec![],
                ),
            ],
            return_attributes: vec![],
            dest: Some("call".into()),
            function_attributes: vec![],
            is_tail_call: false,
            calling_convention: CallingConvention::C,
            debugloc: None,
        };

        let result = call(&input);

        let expected = Node::new(
            "call",
            Kind::UnloweredInterp2(UnloweredInterp2 {
                x: GlobalIdent("x".into()),
                y: GlobalIdent("y".into()),
                z: GlobalIdent("z".into()),
                x_point: Operand::Ident("x_point".into()),
                y_point: Operand::Ident("y_point".into()),
            }),
        );

        assert_eq!(result, Ok(Some(expected)));
    }
}
