use crate::debuginfo::{DebugInfo, IdentDebugInfo, SpecificInfo};
use crate::operand::{Identifier, Operand};
use std::collections::HashMap;

pub struct IdTracker {
    next_id: usize,

    debug_info: HashMap<usize, Vec<IdentDebugInfo>>,
}

impl IdTracker {
    pub fn new(start: usize) -> Self {
        Self {
            next_id: start,
            debug_info: HashMap::new(),
        }
    }

    pub fn next_no_purpose(&mut self) -> usize {
        let result = self.next_id;
        self.next_id += 1;
        result
    }

    pub fn next_linking(&mut self, target: &Identifier, motivation: &str) -> usize {
        let id = self.next_no_purpose();
        self.debug_info
            .insert(id, vec![IdentDebugInfo::link(target, motivation)]);
        id
    }
    pub fn next_linking_operand(&mut self, target: &Operand, motivation: &str) -> usize {
        match target {
            Operand::Ident(id) => self.next_linking(&id, motivation),
            Operand::Const(_) => self.next_no_purpose(),
        }
    }
    pub fn next_multi_link(&mut self, targets: &[&Operand], motivation: &str) -> usize {
        let motivations = targets.iter().filter_map(|op| match op {
            Operand::Ident(id) => Some(IdentDebugInfo::link(&id, motivation)),
            Operand::Const(_) => None,
        });

        let id = self.next_no_purpose();
        self.debug_info.insert(id, motivations.collect());
        id
    }

    pub fn next_specific(&mut self, motivation: SpecificInfo) -> usize {
        let id = self.next_no_purpose();
        self.debug_info
            .insert(id, vec![IdentDebugInfo::Specific(motivation)]);
        id
    }

    pub fn peek(&self) -> usize {
        self.next_id
    }

    pub fn push_motivations(&self, debug: &mut DebugInfo) {
        for (id, info) in &self.debug_info {
            debug.add_info(Identifier::Id(*id), info.clone());
        }
    }
}
