use std::default::Default;

use llvm_ir::{
    function::{Parameter, ParameterAttribute},
    instruction::Phi,
    BasicBlock, Function, HasDebugLoc, Instruction, Name, Terminator, Type,
};
use petgraph::{algo::dominators::simple_fast, graph::NodeIndex};

use crate::error::{Error, InternalError, Result, WithLocation};
use crate::graph::Graph;
use crate::graph_node::{Kind, Node};
use crate::id_tracker::IdTracker;
use crate::operand::{ident, Identifier, Operand};
use crate::uniquevec::UniqueVec;
use crate::util::{invert_operand, resolve_operand};
use crate::{
    debuginfo::{DebugInfo, IdentDebugInfo, SpecificInfo},
    graphgen::pathfinding::{naive_find_significant_paths, smart_find_significant_paths},
};

mod block_graph;
mod masked_branching;
mod path_collection;
mod pathfinding;

pub use block_graph::{BlockGraph, Dom};
use pathfinding::PathStep;

use self::{
    masked_branching::MaskTracker,
    path_collection::{PartialPath, PathCollection},
};

pub enum PhiStrategy {
    /// True if undef branches should be filtered
    PredicationMasks(PhiMaskOptions),
    SignificantPaths(PhiConversionOptions),
}

/// Returns the resulting graph along with the operand corresponding to the returned value
pub fn function_to_graph(
    function: &Function,
    phi_strategy: PhiStrategy,
    id_tracker: &mut IdTracker,
    debug: &mut DebugInfo,
) -> Result<(Graph, Operand)> {
    let return_method = find_return_method(&function.basic_blocks, &function.parameters)?;

    let block_graph = BlockGraph::new(&function.basic_blocks);

    let mut result = Graph::default();

    let mut masks = MaskTracker::new();
    if let PhiStrategy::PredicationMasks(_) = phi_strategy {
        let new_nodes = masks.generate_masks(&block_graph, id_tracker)?;
        for node in new_nodes {
            result.add_node(node);
        }
    }

    if let ReturnMethod::Struct(node) = &return_method {
        result.nodes.insert(node.name.clone(), node.clone());
    }

    // Add input nodes
    for param in &function.parameters {
        if !is_sret(&param) {
            result.nodes.insert(
                (&param.name).into(),
                Node::new(&param.name, Kind::UnboundedInput),
            );
        }
    }

    // Add nodes from the blocks
    for block in &function.basic_blocks {
        let params = AddBasicBlockParams {
            block,
            basic_blocks: &function.basic_blocks,
            graph: result,
            masks: &masks,
            phi_strategy: &phi_strategy,
            id_tracker,
            debug,
        };

        result = add_basic_block_to_graph(params)?;
    }

    Ok((result, return_method.returned_operand()))
}

/// Computes the dominance relation between basic blocks in a function
fn basic_block_dominance(graph: &BlockGraph, root: &Name) -> Dom {
    simple_fast(&graph.inner, graph.index_from_name(&root))
}

struct AddBasicBlockParams<'a> {
    block: &'a BasicBlock,
    basic_blocks: &'a [BasicBlock],
    graph: Graph,
    masks: &'a MaskTracker,
    phi_strategy: &'a PhiStrategy,
    id_tracker: &'a mut IdTracker,
    debug: &'a mut DebugInfo,
}

/// Converts a basic block into an expression graph.
fn add_basic_block_to_graph(
    AddBasicBlockParams {
        block,
        basic_blocks,
        mut graph,
        masks,
        phi_strategy,
        id_tracker,
        debug,
    }: AddBasicBlockParams,
) -> Result<Graph> {
    for ins in &block.instrs {
        macro_rules! handle_instruction {
            ($handler:ident, $payload:path) => {
                let result = crate::instructions::$handler($payload)
                    .map_err(|e| {
                        Error::InstructionHandlingError($payload.clone().into(), Box::new(e))
                    })
                    .with_location(ins.get_debug_loc())?;
                debug.insert(&result.name, &ins.get_debug_loc());
                graph.nodes.insert(result.name.clone(), result);
            };
        }
        macro_rules! handle_option_instruction {
            ($handler:ident, $payload:path) => {
                let result = crate::instructions::$handler($payload)
                    .map_err(|e| {
                        Error::InstructionHandlingError($payload.clone().into(), Box::new(e))
                    })
                    .with_location(ins.get_debug_loc())?;
                if let Some(result) = result {
                    debug.insert(&result.name, &ins.get_debug_loc());
                    graph.nodes.insert(result.name.clone(), result);
                }
            };
        }
        match ins {
            Instruction::Add(p) => {
                handle_instruction!(add, p);
            }
            Instruction::Sub(p) => {
                handle_instruction!(sub, p);
            }
            Instruction::FAdd(p) => {
                handle_instruction!(fadd, p);
            }
            Instruction::FSub(p) => {
                handle_instruction!(fsub, p);
            }
            Instruction::FDiv(p) => {
                handle_instruction!(fdiv, p);
            }
            Instruction::FMul(p) => {
                handle_instruction!(fmul, p);
            }
            Instruction::FNeg(p) => {
                handle_instruction!(fneg, p);
            }
            Instruction::FCmp(p) => {
                handle_instruction!(fcmp, p);
            }
            Instruction::ICmp(p) => {
                handle_instruction!(icmp, p);
            }
            Instruction::SExt(p) => {
                handle_instruction!(sext, p);
            }
            Instruction::ZExt(p) => {
                handle_instruction!(zext, p);
            }
            Instruction::Or(p) => {
                handle_instruction!(or, p);
            }
            Instruction::And(p) => {
                handle_instruction!(and, p);
            }
            Instruction::UIToFP(p) => {
                handle_instruction!(uitofp, p);
            }
            Instruction::SIToFP(p) => {
                handle_instruction!(sitofp, p);
            }
            Instruction::GetElementPtr(p) => {
                handle_instruction!(get_element_ptr, p);
            }
            Instruction::Alloca(p) => {
                handle_instruction!(alloca, p);
            }
            Instruction::Call(p) => {
                handle_option_instruction!(call, p);
            }
            Instruction::Load(p) => {
                handle_instruction!(load, p);
            }
            Instruction::Store(_) => {
                todo!("re-add support for store instructions");
                // let condition =
                //     masks.entry_masks[&block_graph.index_from_name(&block.name)].clone();

                // let result = crate::instructions::store(p, Some(condition), id_tracker)?;
                // if graph.nodes.contains_key(&result.name) {
                //     Err(Error::MultipleStoresToSameAddress)?
                // } else {
                //     graph.add_node(result);
                // }
            }
            Instruction::InsertValue(p) => {
                handle_instruction!(insert_value, p);
            }
            Instruction::BitCast(p) => {
                handle_instruction!(bitcast, p);
            }
            Instruction::Select(p) => {
                handle_instruction!(select, p);
            }
            Instruction::Phi(phi) => match phi_strategy {
                PhiStrategy::PredicationMasks(options) => {
                    let block_graph = BlockGraph::new(basic_blocks);
                    let new_nodes = convert_phi_node_masked(
                        phi,
                        block_graph.index_from_name(&block.name),
                        &block_graph,
                        masks,
                        options,
                    )?;
                    for node in new_nodes {
                        graph.nodes.insert(node.name.clone(), node);
                    }
                }
                PhiStrategy::SignificantPaths(options) => {
                    let new_nodes = convert_phi_node(
                        phi,
                        &block.name,
                        basic_blocks,
                        options,
                        id_tracker,
                        debug,
                    )
                    .map_err(|e| {
                        Error::InstructionHandlingError(phi.clone().into(), Box::new(e))
                    })?;
                    for node in new_nodes {
                        graph.nodes.insert(node.name.clone(), node);
                    }
                }
            },
            other => Err(Error::UnhandledInstruction(other.clone()))?,
        };
    }
    Ok(graph)
}

// Return handling

// Returns can be handled in two ways by llvm. One is to return a local register and the
// other is to pass a reference to a struct in which to store values. It seems to switch
// between the two depending on the size of the return value.

// The first case is easy, the `ret` instruction will point to an identifier to be returned. We
// just return that (with some modifications to handle returning structs)

// The second case is a bit more tricky. We then need to emit a `StructAllocation` node into
// the node graph. This will eventually get lowered to a `StructMembers` node which we can
// return.

#[derive(Debug, PartialEq, Clone)]
pub enum ReturnMethod {
    /// A normal return value through the `ret` instruction
    Normal(Operand),
    /// The returned value is a struct passed by reference in the first argument.
    /// The contained node is a struct allocation node for this return value.
    ///
    /// At some point, this will get lowered to `StructMembers`, meaning that the
    /// exact node will no longer be present in the graph. But the return value
    /// is a node with the same name.
    Struct(Node),
}

impl ReturnMethod {
    pub fn returned_operand(&self) -> Operand {
        match self {
            ReturnMethod::Normal(op) => op.clone(),
            ReturnMethod::Struct(node) => ident(node.name.clone()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ReturnValue {
    Single(Identifier),
    Struct {
        struct_node: Identifier,
        values: Vec<Identifier>,
    },
}

impl ReturnValue {
    pub fn get_root_identifier(&self) -> Identifier {
        match self {
            ReturnValue::Single(ident) => ident.clone(),
            ReturnValue::Struct { struct_node, .. } => struct_node.clone(),
        }
    }
}

pub fn find_return_method(blocks: &[BasicBlock], parameters: &[Parameter]) -> Result<ReturnMethod> {
    let mut result = Err(Error::NoReturn);
    for block in blocks {
        match &block.term {
            Terminator::Ret(ret) => {
                if result.is_ok() {
                    Err(Error::MultipleReturns)?
                }
                if let Some(op) = ret.return_operand.as_ref() {
                    result = Ok(ReturnMethod::Normal(Operand::from_llvm(op)?))
                } else {
                    result = Err(Error::NoReturnValue)
                }
            }
            _ => {}
        }
    }

    if let Err(Error::NoReturnValue) = result {
        // Check if there is a StructReturn in the first parameter
        let first_param = parameters.first().ok_or(Error::NoReturnValue)?;

        if is_sret(&first_param) {
            let pointee_type = match &first_param.ty.as_ref() {
                Type::PointerType { pointee_type, .. } => pointee_type,
                _ => Err(Error::Internal(InternalError::SretOnNonPointer))?,
            };
            // Create a struct allocation of the specified node
            match pointee_type.as_ref() {
                Type::NamedStructType { name, .. } => Ok(ReturnMethod::Struct(Node::new(
                    first_param.name.clone(),
                    Kind::StructAllocation(name.to_string()),
                ))),
                _ => Err(Error::SretOfNonStruct),
            }
        } else {
            result
        }
    } else {
        result
    }
}

fn is_sret(parameter: &Parameter) -> bool {
    for attr in &parameter.attributes {
        if matches!(attr, ParameterAttribute::SRet) {
            return true;
        }
    }
    false
}

/// Finds the return node in the specified basic blocks. The return value can either
/// be a single node idetifier, or a struct definition node in which case all members
/// of the struct will be returned individually.
pub fn return_operands(return_operand: &Operand, graph: &Graph) -> Result<ReturnValue> {
    match &return_operand {
        Operand::Ident(ident) => {
            let resolved_operand = resolve_operand(&Operand::Ident(ident.clone()), graph);

            // If the operand resolves to a non-identifier, something has gone wrong
            let target_ident: &Identifier = resolved_operand
                .expect_ident()
                .ok_or(Error::NonIdentReturnValue(resolved_operand.clone()))?;

            // Look up the node and check if it is a struct
            let target_node = &graph.nodes[target_ident];

            match &target_node.kind {
                Kind::StructWithMembers(_, operands) => {
                    let converted = operands
                        .into_iter()
                        .map(|op| match op {
                            Operand::Ident(x) => Ok(x.clone()),
                            _ => Err(Error::NonIdentReturnValue(op.clone())),
                        })
                        .collect::<Result<Vec<_>>>()?;
                    Ok(ReturnValue::Struct {
                        struct_node: target_ident.clone(),
                        values: converted,
                    })
                }
                _ => Ok(ReturnValue::Single(ident.clone())),
            }
        }
        _ => Err(Error::NonIdentReturnValue(return_operand.clone())),
    }
}

fn transition_condition(
    from: &BasicBlock,
    to: &BasicBlock,
    id_tracker: &mut IdTracker,
) -> Option<(Operand, Vec<Node>)> {
    match &from.term {
        Terminator::Br(br) => {
            // No choice is made, so we better hope this ends in the right place :)
            debug_assert_eq!(br.dest, to.name);
            None
        }
        Terminator::CondBr(br) => {
            let operand = Operand::from_llvm(&br.condition).unwrap();
            if br.true_dest == to.name {
                Some((operand, vec![]))
            } else if br.false_dest == to.name {
                let (new_op, new_node) = invert_operand(
                    operand,
                    id_tracker,
                    &format!("condition for jump {} -> {}", from.name, to.name),
                );
                Some((new_op, vec![new_node]))
            } else {
                unreachable!("Incorrect path")
            }
        }
        _ => unimplemented!(),
    }
}

#[derive(PartialEq)]
pub struct PhiConversionOptions {
    pub cache: bool,
    pub filter_undef: bool,
    pub naive_paths: bool,
}

/**
    Converts a phi node found in origin to a selection node.

    The `paths` variable contains a list of all the possible paths between
    the origin and its dominator.

    In some cases, `not x` nodes need to be created, any such nodes
    are also returned.
*/
fn convert_phi_node(
    node: &Phi,
    origin: &Name,
    basic_blocks: &[BasicBlock],
    options: &PhiConversionOptions,
    id_tracker: &mut IdTracker,
    debug: &mut DebugInfo,
) -> Result<Vec<Node>> {
    let graph = BlockGraph::new(basic_blocks);

    let origin_id = graph.index_from_name(origin);

    let graph = if options.filter_undef {
        let parents = graph.rev.neighbors(origin_id);
        let edges_to_trim = parents
            .map(|parent| {
                let parent_name = graph.name_from_index(&parent);
                let resulting_value = node
                    .incoming_values
                    .iter()
                    .filter_map(|(operand, name)| {
                        if name == parent_name {
                            Some(operand)
                        } else {
                            None
                        }
                    })
                    .next()
                    .expect("Phi node did not have an entry for this path");

                Ok((parent, Operand::from_llvm_filter_undef(resulting_value)?))
            })
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .filter_map(|(parent, operand)| {
                if operand.is_none() {
                    Some((graph.name_from_index(&parent).clone(), origin.clone()))
                } else {
                    None
                }
            })
            .collect();

        BlockGraph::new_without_edges(basic_blocks, edges_to_trim)
    } else {
        graph
    };

    let doms = basic_block_dominance(&graph, &basic_blocks[0].name);

    let origin_id = graph.index_from_name(origin);

    // Generate the available paths
    let dom = doms
        .immediate_dominator(origin_id)
        .expect("Node did not have an immediate dominator");
    let mut new_nodes = vec![];
    let mut selections = UniqueVec::new();

    let mut path_collection = PathCollection::new();

    // Compute the choices needed to end up in the phi node along the relevant paths
    for parent in graph.rev.neighbors(origin_id) {
        let parent_name = graph.name_from_index(&parent);
        let paths = if options.naive_paths {
            naive_find_significant_paths(dom, parent, origin_id, &graph, &doms)
        } else {
            smart_find_significant_paths(dom, parent, origin_id, &graph, &doms)
        };

        for path in paths {
            let conditions = if options.cache {
                let PartialPath {
                    mut matched,
                    unmatched,
                    payload: mut operand,
                } = if let Some(prev) = path_collection.longest_prefix(path.clone()) {
                    prev
                } else {
                    PartialPath {
                        matched: vec![],
                        unmatched: path.clone(),
                        payload: Operand::Const(crate::consts::Const::BoolConst(true)),
                    }
                };

                // Add the conditions for the remainder to the prev_operand

                for rem in unmatched {
                    match &rem {
                        PathStep::Guaranteed(_) => {}
                        PathStep::Selection { from, to } => {
                            let from_block = &graph.name_block_map[&from];
                            let to_block = &graph.name_block_map[&to];
                            if let Some((here, extra)) =
                                transition_condition(from_block, to_block, id_tracker)
                            {
                                let new_id = id_tracker.next_specific(SpecificInfo::PhiCondition(
                                    graph.name_from_index(&origin_id).into(),
                                    parent_name.into(),
                                ));
                                let new_node = Node::new(new_id, Kind::And(operand.clone(), here));
                                operand = Operand::Ident(new_node.name.clone());

                                new_nodes.push(new_node);
                                for node in extra {
                                    new_nodes.push(node)
                                }
                            }
                        }
                    }

                    matched.push(rem);
                    path_collection.push(matched.clone(), operand.clone())
                }

                vec![operand].into()
            } else {
                // The choices required to continue down the current path
                let mut choices = UniqueVec::new();
                for step in &path {
                    match step {
                        PathStep::Guaranteed(_) => {}
                        PathStep::Selection { from, to } => {
                            let f = graph.name_block_map[from].clone();
                            let t = graph.name_block_map[to].clone();
                            if let Some((c, mut nodes)) = transition_condition(&f, &t, id_tracker) {
                                for new_node in &nodes {
                                    debug.add_info(
                                        new_node.name.clone(),
                                        vec![IdentDebugInfo::Specific(SpecificInfo::PhiCondition(
                                            graph.name_from_index(&origin_id).into(),
                                            parent_name.into(),
                                        ))],
                                    )
                                }
                                new_nodes.append(&mut nodes);
                                choices.push(c)
                            }
                        }
                    }
                }

                choices
            };

            let resulting_value = node
                .incoming_values
                .iter()
                .filter_map(|(operand, name)| {
                    if name == parent_name {
                        Some(operand)
                    } else {
                        None
                    }
                })
                .next()
                .expect("Phi node did not have an entry for this path");

            if let Some(value) = Operand::from_llvm_filter_undef(resulting_value)? {
                selections.push((conditions, value));
            } else if !options.filter_undef {
                selections.push((conditions, Operand::from_llvm(resulting_value)?));
            }
        }
    }

    new_nodes.push(Node::new(&node.dest, Kind::Select(selections)));
    Ok(new_nodes)
}

#[derive(PartialEq)]
pub struct PhiMaskOptions {
    pub filter_undef: bool,
}

/**
  Converts phi nodes using pre-generated edge masks
*/
fn convert_phi_node_masked(
    node: &Phi,
    origin: NodeIndex,
    graph: &BlockGraph,
    masks: &MaskTracker,
    options: &PhiMaskOptions,
) -> Result<Vec<Node>> {
    let mut new_nodes = vec![];

    let mut selections = UniqueVec::new();

    for (value, parent) in &node.incoming_values {
        let choices =
            vec![masks.edge_masks[&(graph.index_from_name(&parent), origin)].clone()].into();
        if let Some(value) = Operand::from_llvm_filter_undef(value)? {
            selections.push((choices, value))
        } else if !options.filter_undef {
            selections.push((choices, Operand::from_llvm(&value)?))
        }
    }

    new_nodes.push(Node::new(&node.dest, Kind::Select(selections)));

    Ok(new_nodes)
}

#[cfg(test)]
mod tests {
    use super::*;

    use llvm_ir::ConstantRef;
    use llvm_ir::{
        terminator, types::FPType, types::Types, Module, Operand as LlvmOperand, Terminator,
    };

    use crate::testutil::f32var;
    use crate::{consts::Const, graph_node::node};

    use pretty_assertions::assert_eq;

    #[test]
    fn ret_values_are_found() {
        let types = Types::blank_for_testing();
        let basic_blocks = vec![BasicBlock {
            name: 1.into(),
            instrs: vec![],
            term: Terminator::Ret(terminator::Ret {
                return_operand: Some(LlvmOperand::LocalOperand {
                    name: "retval".into(),
                    ty: types.fp(FPType::Double),
                }),
                debugloc: None,
            }),
        }];

        let mut graph = Graph::empty();
        graph.add_node(Node::new(
            "retval",
            // This just has to be anything that isn't a nop to avoid issues when
            // looking for struct definitions
            Kind::Not(ident("a".into())),
        ));
        let method = find_return_method(&basic_blocks, &[]).unwrap();
        let result = return_operands(&method.returned_operand(), &graph);

        assert_eq!(result, Ok(ReturnValue::Single("retval".into())));
    }

    #[test]
    fn return_by_reference_is_handled_correctly() {
        let types = Types::blank_for_testing();
        let parameters = [
            Parameter {
                name: "agg.result".into(),
                ty: types.pointer_to(types.named_struct("struct.output")),
                attributes: vec![ParameterAttribute::SRet],
            },
            Parameter {
                name: "a".into(),
                ty: types.fp(FPType::Single),
                attributes: vec![],
            },
        ];
        let basic_blocks = vec![BasicBlock {
            name: 1.into(),
            instrs: vec![],
            term: Terminator::Ret(terminator::Ret {
                return_operand: None,
                debugloc: None,
            }),
        }];

        let result = find_return_method(&basic_blocks, &parameters);

        let expected = ReturnMethod::Struct(Node::new(
            "agg.result",
            Kind::StructAllocation("struct.output".to_string()),
        ));
        assert_eq!(result, Ok(expected));
    }

    #[test]
    fn struct_ret_values_are_found() {
        let types = Types::blank_for_testing();
        let basic_blocks = vec![BasicBlock {
            name: 1.into(),
            instrs: vec![],
            term: Terminator::Ret(terminator::Ret {
                return_operand: Some(LlvmOperand::LocalOperand {
                    name: "retval".into(),
                    ty: types.fp(FPType::Double),
                }),
                debugloc: None,
            }),
        }];

        let mut graph = Graph::empty();
        graph.add_node(Node::new(
            "retval",
            Kind::StructWithMembers(
                "something".to_string(),
                vec![ident("a".into()), ident("b".into())],
            ),
        ));

        let return_method = find_return_method(&basic_blocks, &[]).unwrap();

        let return_value = return_operands(&return_method.returned_operand(), &graph);

        assert_eq!(
            return_value,
            Ok(ReturnValue::Struct {
                struct_node: "retval".into(),
                values: vec!["a".into(), "b".into()]
            })
        );
    }

    #[test]
    fn ret_values_are_not_found_when_none_exist() {
        let basic_blocks = vec![BasicBlock {
            name: 1.into(),
            instrs: vec![],
            term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
        }];

        let result = find_return_method(&basic_blocks, &[]);

        assert_eq!(result, Err(Error::NoReturn));
    }

    #[test]
    fn error_is_thrown_when_no_ret_value() {
        let basic_blocks = vec![BasicBlock {
            name: 1.into(),
            instrs: vec![],
            term: Terminator::Ret(terminator::Ret {
                return_operand: None,
                debugloc: None,
            }),
        }];

        let result = find_return_method(&basic_blocks, &[]);

        assert_eq!(result, Err(Error::NoReturnValue));
    }

    #[test]
    fn error_is_thrown_when_multiple_rets() {
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::Ret(terminator::Ret {
                    return_operand: Some(LlvmOperand::LocalOperand {
                        name: "retval".into(),
                        ty: types.fp(FPType::Double),
                    }),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::Ret(terminator::Ret {
                    return_operand: Some(LlvmOperand::LocalOperand {
                        name: "retval2".into(),
                        ty: types.fp(FPType::Double),
                    }),
                    debugloc: None,
                }),
            },
        ];

        let result = find_return_method(&basic_blocks, &[]);

        assert_eq!(result, Err(Error::MultipleReturns));
    }

    #[test]
    fn singular_path_to_phi_conversion_works() {
        /*
          0
         / \
        1   2
         \ /
          3
        */
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 1.into(),
                    false_dest: 2.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];

        let mut id_tracker = IdTracker::new(100);
        let result = convert_phi_node(
            &Phi {
                incoming_values: vec![
                    (f32var(12, &types), 1.into()),
                    (f32var(13, &types), 2.into()),
                ],
                dest: 14.into(),
                to_type: types.fp(FPType::Single),
                debugloc: None,
            },
            &3.into(),
            &basic_blocks,
            &PhiConversionOptions {
                cache: true,
                filter_undef: false,
                naive_paths: false,
            },
            &mut id_tracker,
            &mut DebugInfo::new(),
        );

        let expected = vec![
            Node {
                name: 101.into(),
                kind: Kind::And(Operand::Const(Const::BoolConst(true)), ident(100.into())),
            },
            Node {
                name: 100.into(),
                kind: Kind::Not(ident(10.into())),
            },
            Node {
                name: 102.into(),
                kind: Kind::And(Operand::Const(Const::BoolConst(true)), ident(10.into())),
            },
            Node {
                name: 14.into(),
                kind: Kind::Select(
                    vec![
                        (vec![ident(101.into())].into(), ident(13.into())),
                        (vec![ident(102.into())].into(), ident(12.into())),
                    ]
                    .into(),
                ),
            },
        ];

        assert_eq!(result, Ok(expected));
        assert_eq!(id_tracker.peek(), 103);
    }

    #[test]
    fn phi_conversion_with_undef_value_filter_works() {
        /*
          0
         / \
        1   2
         \ /
          3
        */
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 1.into(),
                    false_dest: 2.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];

        let mut id_tracker = IdTracker::new(100);
        let result = convert_phi_node(
            &Phi {
                incoming_values: vec![
                    (f32var(12, &types), 1.into()),
                    (
                        LlvmOperand::ConstantOperand(ConstantRef::new(llvm_ir::Constant::Undef(
                            types.fp(FPType::Single),
                        ))),
                        2.into(),
                    ),
                ],
                dest: 14.into(),
                to_type: types.fp(FPType::Single),
                debugloc: None,
            },
            &3.into(),
            &basic_blocks,
            &PhiConversionOptions {
                cache: true,
                filter_undef: true,
                naive_paths: false,
            },
            &mut id_tracker,
            &mut DebugInfo::new(),
        );

        let expected = vec![Node {
            name: 14.into(),
            kind: Kind::Select(
                vec![(
                    vec![Operand::Const(Const::BoolConst(true))].into(),
                    ident(12.into()),
                )]
                .into(),
            ),
        }];

        assert_eq!(result, Ok(expected));
        assert_eq!(id_tracker.peek(), 100);
    }

    #[test]
    fn phi_conversion_with_undef_value_filter_assumes_0() {
        /*
          0
         / \
        1   2
         \ /
          3
        */
        let types = Types::blank_for_testing();
        let basic_blocks = vec![
            BasicBlock {
                name: 0.into(),
                instrs: vec![],
                term: Terminator::CondBr(terminator::CondBr {
                    // NOTE: We don't care about the type here, so f32 is fine
                    condition: f32var(10, &types),
                    true_dest: 1.into(),
                    false_dest: 2.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 1.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 2.into(),
                instrs: vec![],
                term: Terminator::Br(terminator::Br {
                    dest: 3.into(),
                    debugloc: None,
                }),
            },
            BasicBlock {
                name: 3.into(),
                instrs: vec![],
                term: Terminator::Unreachable(terminator::Unreachable { debugloc: None }),
            },
        ];

        let mut id_tracker = IdTracker::new(100);
        let result = convert_phi_node(
            &Phi {
                incoming_values: vec![
                    (f32var(12, &types), 1.into()),
                    (
                        LlvmOperand::ConstantOperand(ConstantRef::new(llvm_ir::Constant::Undef(
                            types.fp(FPType::Double),
                        ))),
                        2.into(),
                    ),
                ],
                dest: 14.into(),
                to_type: types.fp(FPType::Single),
                debugloc: None,
            },
            &3.into(),
            &basic_blocks,
            &PhiConversionOptions {
                cache: true,
                filter_undef: false,
                naive_paths: false,
            },
            &mut id_tracker,
            &mut DebugInfo::new(),
        );

        let expected = vec![
            Node {
                name: 101.into(),
                kind: Kind::And(Operand::Const(Const::BoolConst(true)), ident(100.into())),
            },
            Node {
                name: 100.into(),
                kind: Kind::Not(ident(10.into())),
            },
            Node {
                name: 102.into(),
                kind: Kind::And(Operand::Const(Const::BoolConst(true)), ident(10.into())),
            },
            Node {
                name: 14.into(),
                kind: Kind::Select(
                    vec![
                        (vec![ident(102.into())].into(), ident(12.into())),
                        (
                            vec![ident(101.into())].into(),
                            Operand::Const(Const::DoubleConst(0.)),
                        ),
                    ]
                    .into(),
                ),
            },
        ];

        assert_eq!(result, Ok(expected));
        assert_eq!(id_tracker.peek(), 103);
    }

    #[test]
    #[ignore]
    fn branching_produces_correct_expression_tree() {
        let module = Module::from_bc_path("testcode/unittests/basic_branching.bc")
            .expect("Failed to load bitcode");

        let function = module
            .get_func_by_name("test")
            .expect("No function in module");

        let mut id_tracker = IdTracker::new(100);
        let (graph, _) = function_to_graph(
            function,
            PhiStrategy::SignificantPaths(PhiConversionOptions {
                cache: true,
                filter_undef: false,
                naive_paths: false,
            }),
            &mut id_tracker,
            &mut DebugInfo::new(),
        )
        .expect("Failed to build graph");

        let expected = Graph::from_tuples(vec![
            node("x", Kind::UnboundedInput),
            node("y", Kind::UnboundedInput),
            node(
                "cmp",
                Kind::GreaterThan(ident("x".into()), ident("y".into())),
            ),
            node(100, Kind::Not(ident("cmp".into()))),
            node(
                "result.0",
                Kind::Select(
                    vec![
                        (vec![ident(100.into())].into(), ident("y".into())),
                        (vec![ident("cmp".into())].into(), ident("x".into())),
                    ]
                    .into(),
                ),
            ),
        ]);

        assert_eq!(graph, expected);
    }
}
