use llvm_ir::{types::FPType, types::Types, Name, Operand};

pub fn f32var(name: impl Into<Name>, types: &Types) -> Operand {
    Operand::LocalOperand {
        name: name.into(),
        ty: types.fp(FPType::Single),
    }
}

pub fn i32var(name: impl Into<Name>, types: &Types) -> Operand {
    Operand::LocalOperand {
        name: name.into(),
        ty: types.int(32),
    }
}

#[allow(unused)]
pub fn f32ptr(name: impl Into<Name>, types: &Types) -> Operand {
    todo!("This is not a f32 pointer, just an f32");
    Operand::LocalOperand {
        name: name.into(),
        ty: types.fp(FPType::Single),
    }
}
