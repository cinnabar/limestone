use thiserror::Error;

use crate::globals::{Global, GlobalTable};
use crate::graph::Graph;
use crate::graph_node::Kind;
use crate::operand::{Identifier, Operand};

#[derive(Debug, Clone, PartialEq, Error)]
pub enum GetElementPtrError {
    #[error("Expected {0:?} to be a GetElementPtr node")]
    NoGetElementPtr(Identifier),
}

pub struct GetElementPtrData<'a> {
    pub address: &'a Operand,
    pub indices: &'a Vec<Operand>,
}

pub fn get_get_element_ptr<'a>(
    graph: &'a Graph,
    name: &Identifier,
) -> Result<GetElementPtrData<'a>, GetElementPtrError> {
    let candidate = &graph.nodes[&name];
    if let Kind::GetElementPtr { address, indices } = &candidate.kind {
        Ok(GetElementPtrData { address, indices })
    } else {
        Err(GetElementPtrError::NoGetElementPtr(name.clone()))
    }
}

#[derive(Debug, Clone, PartialEq, Error)]
pub enum GlobalLookupError {
    #[error("No global string named '{0:?}' found")]
    NoSuchGlobal(Identifier),
    /// The specified global exists but is not a double array
    #[error("No array of doubles named '{0:?}' found")]
    NotDoubleArray(Identifier),
}

pub fn get_global_double_array(
    name: &Identifier,
    globals: &GlobalTable,
) -> Result<Vec<f64>, GlobalLookupError> {
    let lut_values = globals
        .get_value(&name)
        .ok_or(GlobalLookupError::NoSuchGlobal(name.clone()))?;

    if let Global::DoubleArray(data) = lut_values {
        Ok(data.clone())
    } else {
        Err(GlobalLookupError::NotDoubleArray(name.clone()))?
    }
}
