// opt-opts -mem2reg

extern "C" {
    int limestone_trunc(double);
}

double interp1equid(double nx, double xmin, double dx, double xp) {
    double ii = (xp-xmin) - dx;
    double yp = ii + dx * xp;
    return yp * nx;
}
