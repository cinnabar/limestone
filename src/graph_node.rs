use serde_derive::Serialize;

use crate::operand::{Identifier, Operand};
use crate::uniquevec::UniqueVec;

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct GlobalIdent(pub Identifier);

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct UnloweredInterp1 {
    pub x: GlobalIdent,
    pub y: GlobalIdent,
    pub x_point: Operand,
}
impl UnloweredInterp1 {
    fn operands(self) -> Vec<Operand> {
        vec![self.x_point]
    }
}
#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct UnloweredInterp2 {
    pub x: GlobalIdent,
    pub y: GlobalIdent,
    pub z: GlobalIdent,
    pub x_point: Operand,
    pub y_point: Operand,
}
impl UnloweredInterp2 {
    fn operands(self) -> Vec<Operand> {
        vec![self.x_point.into(), self.y_point.into()]
    }
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum Kind {
    /// Used for functions that do nothing of importance. For example, sign
    /// extend which is irrelevant as word lengths are selected later
    Nop(Operand),

    Input(f64, f64),

    // Comparisons
    GreaterThan(Operand, Operand),
    GreaterThanOrEq(Operand, Operand),
    LessThan(Operand, Operand),
    LessThanOrEq(Operand, Operand),
    Equal(Operand, Operand),
    NotEqual(Operand, Operand),

    // Arithmetic
    Add(Operand, Operand),
    Sub(Operand, Operand),
    Div(Operand, Operand),
    Mul(Operand, Operand),

    // Numeric stuff
    USub(Operand),
    Truncate(Operand),
    Sqrt(Operand),
    Abs(Operand),
    Max(Operand, Operand),
    Min(Operand, Operand),

    // Boolean
    Not(Operand),
    And(Operand, Operand),
    Or(Operand, Operand),
    /// (x, y) Select value y if all conditions in x are satisfied
    Select(UniqueVec<(UniqueVec<Operand>, Operand)>),
    /// If .1 is true, select 2, elese 3
    IfElse(Operand, Operand, Operand),

    /// Look up `idx` in the named lookup table containing the specified data
    Lookup {
        name: String,
        data: Vec<f64>,
        idx: Operand,
    },

    /// Full interpolation functions
    Interp1 {
        x: Vec<f64>,
        y: Vec<f64>,
        x_point: Operand,
    },
    Interp1Valid {
        x: Vec<f64>,
        y: Vec<f64>,
        x_point: Operand,
    },
    Interp2 {
        x: Vec<f64>,
        y: Vec<f64>,
        z: Vec<f64>,
        x_point: Operand,
        y_point: Operand,
    },
    Interp2Valid {
        x: Vec<f64>,
        y: Vec<f64>,
        z: Vec<f64>,
        x_point: Operand,
        y_point: Operand,
    },

    /// Allocation of a struct with the specified name
    StructAllocation(String),

    StructWithMembers(String, Vec<Operand>),

    OpaqueBarrier(Operand),

    // The following kinds get lowered to other instructions before
    // being sent out of the program. Therefore, they don't have to
    // be serialized
    /// This instruction will not be handled individually, so we'll just keep
    /// it around and lower it later when handling its users
    #[serde(skip)]
    GetElementPtr {
        address: Operand,
        indices: Vec<Operand>,
    },
    /// The load instruction can mean a whole bunch of things. We'll hold
    /// off on interpreting it until all instructions have been loaded.
    /// The stored value is the address to load from
    #[serde(skip)]
    Load(Operand),
    /// Result of a store instruction. Will be used for aliasing in lowering steps.
    /// The `conditions` vector contains all conditions that must hold for this store
    /// to be performed.
    #[serde(skip)]
    Store {
        addr: Identifier,
        value: Operand,
        condition: Option<Operand>,
    },

    /// Insertvalue instructions, primarily used for building structs
    #[serde(skip)]
    InsertValue {
        /// The name of the previous insertion into this struct
        previous: Option<Identifier>,
        /// The value to insert
        value: Operand,
        /// The index of the element to insert
        index: u32,
    },

    /// Unlowered interpolation nodes
    #[serde(skip)]
    UnloweredInterp1(UnloweredInterp1),
    #[serde(skip)]
    UnloweredInterp1Valid(UnloweredInterp1),
    #[serde(skip)]
    UnloweredInterp2(UnloweredInterp2),
    #[serde(skip)]
    UnloweredInterp2Valid(UnloweredInterp2),

    /// Specifies the bounds on the specified `UnboundedInput`.
    InputBound(Identifier, f64, f64),

    /// An input value to the function which has yet to have a bound associated with it
    #[serde(skip)]
    UnboundedInput,
}

impl Kind {
    pub fn operands(&self) -> Vec<Operand> {
        match self.clone() {
            Kind::Nop(op) => vec![op],
            Kind::Input(_, _) => vec![],
            Kind::UnboundedInput => vec![],
            Kind::GreaterThan(lhs, rhs) => vec![lhs, rhs],
            Kind::GreaterThanOrEq(lhs, rhs) => vec![lhs, rhs],
            Kind::LessThan(lhs, rhs) => vec![lhs, rhs],
            Kind::LessThanOrEq(lhs, rhs) => vec![lhs, rhs],
            Kind::Equal(lhs, rhs) => vec![lhs, rhs],
            Kind::NotEqual(lhs, rhs) => vec![lhs, rhs],
            Kind::Add(lhs, rhs) => vec![lhs, rhs],
            Kind::Sub(lhs, rhs) => vec![lhs, rhs],
            Kind::Div(lhs, rhs) => vec![lhs, rhs],
            Kind::Mul(lhs, rhs) => vec![lhs, rhs],
            Kind::Min(lhs, rhs) => vec![lhs, rhs],
            Kind::Max(lhs, rhs) => vec![lhs, rhs],
            Kind::USub(op) => vec![op],
            Kind::Truncate(op) => vec![op],
            Kind::Sqrt(op) => vec![op],
            Kind::Abs(op) => vec![op],
            Kind::Not(op) => vec![op],
            Kind::And(lhs, rhs) => vec![lhs, rhs],
            Kind::Or(lhs, rhs) => vec![lhs, rhs],
            Kind::Select(values) => values
                .inner
                .into_iter()
                .map(|(conds, val)| conds.inner.into_iter().chain(vec![val].into_iter()))
                .flatten()
                .collect(),
            Kind::IfElse(cond, on_true, on_false) => vec![cond, on_true, on_false],
            Kind::Lookup { idx: op, .. } => vec![op],
            Kind::GetElementPtr {
                address: op,
                indices: ops,
            } => {
                let mut result = vec![op];
                result.append(&mut ops.into_iter().collect());
                result
            }
            Kind::Load(op) => vec![op],
            // TODO: Add conditions here
            Kind::Store {
                addr,
                value,
                condition,
            } => {
                let mut result = vec![Operand::Ident(addr), value];
                if let Some(cond) = condition {
                    result.push(cond);
                }
                result
            }
            Kind::InsertValue {
                previous,
                value,
                index: _,
            } => {
                if let Some(previous) = previous {
                    vec![Operand::Ident(previous), value]
                } else {
                    vec![value]
                }
            }
            Kind::OpaqueBarrier(op) => vec![op],
            Kind::InputBound(target, _, _) => vec![Operand::Ident(target)],
            Kind::StructAllocation(_) => vec![],
            Kind::StructWithMembers(_, members) => members,
            Kind::Interp1 { x_point, .. } => vec![x_point],
            Kind::Interp1Valid { x_point, .. } => vec![x_point],
            Kind::Interp2 {
                x_point, y_point, ..
            } => vec![x_point, y_point],
            Kind::Interp2Valid {
                x_point, y_point, ..
            } => vec![x_point, y_point],

            Kind::UnloweredInterp1(params) => params.operands(),
            Kind::UnloweredInterp2(params) => params.operands(),
            Kind::UnloweredInterp1Valid(params) => params.operands(),
            Kind::UnloweredInterp2Valid(params) => params.operands(),
        }
    }

    pub fn description(&self) -> &'static str {
        match self {
            Kind::Nop(_) => "Nop",
            Kind::Input(_, _) => "Input",
            Kind::GreaterThan(_, _) => "GreaterThan",
            Kind::GreaterThanOrEq(_, _) => "GreaterThanOrEq",
            Kind::LessThan(_, _) => "LessThan",
            Kind::LessThanOrEq(_, _) => "LessThanOrEq",
            Kind::Equal(_, _) => "Equal",
            Kind::NotEqual(_, _) => "NotEqual",
            Kind::Add(_, _) => "Add",
            Kind::Sub(_, _) => "Sub",
            Kind::Div(_, _) => "Div",
            Kind::Mul(_, _) => "Mul",
            Kind::USub(_) => "USub",
            Kind::Truncate(_) => "Truncate",
            Kind::Sqrt(_) => "Sqrt",
            Kind::Abs(_) => "Abs",
            Kind::Max(_, _) => "Max",
            Kind::Min(_, _) => "Min",
            Kind::Not(_) => "Not",
            Kind::And(_, _) => "And",
            Kind::Or(_, _) => "Or",
            Kind::Select(_) => "Select",
            Kind::IfElse(_, _, _) => "IfElse",
            Kind::Lookup { .. } => "Lookup",
            Kind::Interp1 { .. } => "Interp1",
            Kind::Interp1Valid { .. } => "Interp1Valid",
            Kind::Interp2 { .. } => "Interp2",
            Kind::Interp2Valid { .. } => "Interp2Valid",
            Kind::StructAllocation(_) => "StructAllocation",
            Kind::StructWithMembers(_, _) => "StructWithMembers",
            Kind::GetElementPtr { .. } => "GetElementPtr",
            Kind::Load(_) => "Load",
            Kind::Store { .. } => "Store",
            Kind::InsertValue { .. } => "InsertValue",
            Kind::UnloweredInterp1(_) => "UnloweredInterp1",
            Kind::UnloweredInterp1Valid(_) => "UnloweredInterp1Valid",
            Kind::UnloweredInterp2(_) => "UnloweredInterp2",
            Kind::UnloweredInterp2Valid(_) => "UnloweredInterp2Valid",
            Kind::InputBound(_, _, _) => "InputBound",
            Kind::UnboundedInput => "UnboundedInput",
            Kind::OpaqueBarrier(_) => "OpaqueBarrier",
        }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct Node {
    pub name: Identifier,
    pub kind: Kind,
}

impl Node {
    pub fn new(name: impl Into<Identifier>, kind: Kind) -> Self {
        Self {
            name: name.into(),
            kind,
        }
    }
}

pub fn node(name: impl Into<Identifier> + Clone, kind: Kind) -> (Identifier, Node) {
    (
        name.clone().into(),
        Node {
            name: name.clone().into(),
            kind,
        },
    )
}
