use petgraph::{
    graph::{Graph, IndexType, NodeIndex},
    EdgeType,
};

/// Returns true if there are paths between `from` and `to` which do not go via
/// `not_via`
///
/// Takes a ownership over graph in order to be able to remove an element from it.
pub fn has_paths_not_via<N, E, Ty, Ix>(
    from: NodeIndex<Ix>,
    to: NodeIndex<Ix>,
    not_via: NodeIndex<Ix>,
    mut graph: Graph<N, E, Ty, Ix>,
) -> bool
where
    Ty: EdgeType,
    Ix: IndexType,
{
    // Check if one of the nodes we're interested in is the one which will have
    // its index changed by the removal
    let last_index = if let Some(idx) = graph.node_indices().last() {
        idx
    } else {
        // Graph must be empty
        return false;
    };

    let from = if last_index == from { not_via } else { from };
    let to = if last_index == to { not_via } else { to };

    graph.remove_node(not_via);
    petgraph::algo::has_path_connecting(&graph, from, to, None)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn has_paths_not_via_tests() {
        /*
        0
        |
        1
        |\
        | 2
        |/|
        3 |
        |/
        4
        |
        5
        */

        let mut graph = Graph::<usize, usize>::new();
        let n0 = graph.add_node(0);
        let n1 = graph.add_node(1);
        let n2 = graph.add_node(2);
        let n3 = graph.add_node(3);
        let n4 = graph.add_node(4);
        let n5 = graph.add_node(5);
        graph.extend_with_edges(&[
            (n0, n1),
            (n1, n2),
            (n1, n3),
            (n2, n3),
            (n2, n4),
            (n3, n4),
            (n4, n5),
        ]);

        assert_eq!(has_paths_not_via(n0, n4, n3, graph.clone()), true);
        assert_eq!(has_paths_not_via(n0, n4, n1, graph.clone()), false);
        assert_eq!(has_paths_not_via(n0, n4, n2, graph.clone()), true);
    }

    #[test]
    fn has_paths_not_via_works_when_last_node_idx_changed() {
        {
            /*
            0
            |
            1
            |\
            | 2
            |/|
            3 |
            |/
            4
            */

            let mut graph = Graph::<usize, usize>::new();
            let n1 = graph.add_node(1);
            let n2 = graph.add_node(2);
            let n3 = graph.add_node(3);
            let n4 = graph.add_node(4);
            let n0 = graph.add_node(0);
            graph.extend_with_edges(&[(n0, n1), (n1, n2), (n1, n3), (n2, n3), (n2, n4), (n3, n4)]);

            assert_eq!(has_paths_not_via(n0, n4, n3, graph.clone()), true);
            assert_eq!(has_paths_not_via(n0, n4, n1, graph.clone()), false);
            assert_eq!(has_paths_not_via(n0, n4, n2, graph.clone()), true);
        }
    }
}
