// opt-opts -mem2reg
#include <cmath>

// Original function using C++ primitive
// datatypes
double example(double x, double y) {
    double a;
    if (x > 0) {
        a = x;
    }
    else {
        a = y;
    }
    return a;
}
