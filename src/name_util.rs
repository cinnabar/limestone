use llvm_ir::Name;

use crate::operand::Identifier;

/// LLVM IR can contain some strange name that will not be liked by the
/// tools reading cinnabar output. This makes those names somewhat safe
///
/// Returns None if the name is an id
fn escape_name(name: &str) -> String {
    name.replace(".", "__dot__")
}

fn num_to_string(num: usize) -> String {
    format!("__num_{}", num)
}

pub trait StringifyableName {
    fn stringify(&self) -> String;
}

impl StringifyableName for Name {
    fn stringify(&self) -> String {
        match self {
            Name::Name(inner) => escape_name(inner),
            Name::Number(id) => num_to_string(*id),
        }
    }
}

impl StringifyableName for Identifier {
    fn stringify(&self) -> String {
        match self {
            Identifier::Str(inner) => escape_name(inner),
            Identifier::Id(id) => num_to_string(*id),
        }
    }
}
