use std::collections::HashMap;

use serde_derive::Serialize;

use crate::consts::Const;
use crate::debuginfo::DebugInfo;
use crate::operand::{Identifier, Operand, OperandMap};

use crate::graph_node::{Kind, Node};

/// A struct that can be used to interract with a node that was just added
pub struct AddHandle {
    /// The identifier of the node that was added
    pub name: Identifier,
}
impl AddHandle {
    pub fn debug_link(self, debug: &mut DebugInfo, to: &Identifier, description: &str) -> Self {
        debug.add_link(&self.name, to, description);
        self
    }
}

#[derive(Default, Clone, Debug, PartialEq, Serialize)]
pub struct Graph {
    pub nodes: HashMap<Identifier, Node>,
}

impl Graph {
    pub fn empty() -> Self {
        Graph {
            nodes: HashMap::new(),
        }
    }

    pub fn from_nodes<'a, T, N>(other: T) -> Self
    where
        T: IntoIterator<Item = N>,
        N: std::borrow::Borrow<Node>,
    {
        Graph {
            nodes: other
                .into_iter()
                .map(|node| (node.borrow().name.clone(), node.borrow().clone()))
                .collect(),
        }
    }

    pub fn from_tuples<'a, T, N>(other: T) -> Self
    where
        T: IntoIterator<Item = N>,
        N: std::borrow::Borrow<(Identifier, Node)>,
    {
        Graph {
            nodes: other.into_iter().map(|n| n.borrow().clone()).collect(),
        }
    }

    /// Removes any nodes that are not reachable from the `ret_operand` node
    pub fn clean_unreachable(self, ret_identifier: &Identifier) -> Graph {
        let mut to_visit = vec![&self.nodes[ret_identifier]];
        let mut seen = vec![];
        while let Some(curr) = to_visit.pop() {
            for operand in curr.kind.operands() {
                match operand {
                    Operand::Ident(ref op) => {
                        let node = &self.nodes[op];
                        if !to_visit.contains(&node) && !seen.contains(&node) {
                            to_visit.push(node)
                        }
                    }
                    Operand::Const(_) => {}
                }
            }
            seen.push(curr);
        }

        Graph::from_nodes(seen)
    }

    pub fn add_node(&mut self, node: Node) -> AddHandle {
        let name = node.name.clone();
        self.nodes.insert(name.clone(), node);
        AddHandle { name }
    }
}

pub enum GraphDiff {
    /// The specified node is present in the lhs but not the rhs
    OnlyLhs(Node),
    /// Both graphs have a node with the same id, but one is not like the other
    Replaced(Node, Node),
    /// The specified node is present in the rhs but not the lhs
    OnlyRhs(Node),
}

impl Graph {
    pub fn diff(&self, other: &Self) -> Vec<GraphDiff> {
        let mut result = vec![];

        let mut seen_in_other = vec![];
        for (name, node) in &self.nodes {
            if let Some(other_node) = other.nodes.get(name) {
                // Compare the nodes
                seen_in_other.push(name.clone());

                if other_node.kind != node.kind {
                    result.push(GraphDiff::Replaced(node.clone(), other_node.clone()))
                }
            } else {
                result.push(GraphDiff::OnlyRhs(node.clone()))
            }
        }

        // Check if there are any nodes only present in lhs
        for (name, node) in &other.nodes {
            if !seen_in_other.contains(name) {
                result.push(GraphDiff::OnlyLhs(node.clone()))
            }
        }

        result
    }
}

// Depth first traversal impls
impl Graph {
    pub fn cached_traversal<T, E, Op, ConstOp>(
        &self,
        root: &Operand,
        op: &mut Op,
        const_op: &ConstOp,
    ) -> Result<OperandMap<T>, E>
    where
        Op: FnMut(&Identifier, &Kind, &OperandMap<T>) -> Result<T, E>,
        ConstOp: Fn(&Const, &OperandMap<T>) -> Result<T, E>,
    {
        let mut result = OperandMap::new();
        self.cached_traversal_helper(root, op, const_op, &mut result)?;
        Ok(result)
    }
    fn cached_traversal_helper<T, E>(
        &self,
        root: &Operand,
        op: &mut impl FnMut(&Identifier, &Kind, &OperandMap<T>) -> Result<T, E>,
        const_op: &impl Fn(&Const, &OperandMap<T>) -> Result<T, E>,
        cache: &mut OperandMap<T>,
    ) -> Result<(), E> {
        if let None = cache.get(root) {
            let result = match root {
                Operand::Ident(ident) => {
                    let node = &self.nodes[ident];
                    for operand in node.kind.operands() {
                        self.cached_traversal_helper(&operand, op, const_op, cache)?;
                    }
                    op(&ident, &node.kind, cache)?
                }
                Operand::Const(c) => const_op(c, cache)?,
            };
            cache.insert(root.clone(), result)
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::graph_node::node;
    use crate::operand::ident;

    #[test]
    fn graph_cleanup_works() {
        let graph = Graph::from_tuples(vec![
            node("a", Kind::UnboundedInput),
            node("b", Kind::UnboundedInput),
            node("c", Kind::UnboundedInput),
            node("add_a_b", Kind::Add(ident("a".into()), ident("b".into()))),
        ]);

        let clean = graph.clean_unreachable(&"add_a_b".into());

        let expected = Graph::from_tuples(vec![
            node("add_a_b", Kind::Add(ident("a".into()), ident("b".into()))),
            node("a", Kind::UnboundedInput),
            node("b", Kind::UnboundedInput),
        ]);
        assert_eq!(clean, expected);
    }

    #[test]
    fn select_node_operand_listing_works() {
        let select = Kind::Select(
            vec![
                (
                    vec![ident("x".into()), ident("y".into())].into(),
                    ident("a".into()),
                ),
                (
                    vec![ident("q".into()), ident("p".into())].into(),
                    ident("b".into()),
                ),
            ]
            .into(),
        );

        let expected = vec![
            ident("x".into()),
            ident("y".into()),
            ident("a".into()),
            ident("q".into()),
            ident("p".into()),
            ident("b".into()),
        ];

        assert_eq!(expected, select.operands());
    }

    #[test]
    fn get_element_ptr_operand_listing_works() {
        let node = Kind::GetElementPtr {
            address: ident("a".into()),
            indices: vec![ident("b".into()), ident("c".into())],
        };

        let expected = vec![ident("a".into()), ident("b".into()), ident("c".into())];

        assert_eq!(expected, node.operands());
    }

    #[test]
    fn cached_traversal_test() {
        let graph = Graph::from_tuples(vec![
            node("a", Kind::UnboundedInput),
            node(
                "b",
                Kind::Add(ident("a".into()), Operand::Const(Const::IntConst(0))),
            ),
            node("c", Kind::Nop(ident("b".into()))),
        ]);

        let mut expected = OperandMap::new();
        expected.insert(ident("a".into()), 0);
        expected.insert(ident("b".into()), 2);
        expected.insert(ident("c".into()), 1);
        expected.insert(Operand::Const(Const::IntConst(0)), 100);

        let result: Result<_, ()> = graph.cached_traversal(
            &ident("c".into()),
            &mut |_name, kind, _cache| Ok(kind.operands().len()),
            &|_c, _cache| Ok(100),
        );
        assert_eq!(result, Ok(expected));
    }
}
